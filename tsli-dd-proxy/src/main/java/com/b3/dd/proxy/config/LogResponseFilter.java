package com.b3.dd.proxy.config;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;


//@Component
public class LogResponseFilter extends ZuulFilter {

    private static Logger log = LogManager.getLogger(LogResponseFilter.class);
	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 9999;
	}

	public boolean shouldFilter() {
		return true;
	}

	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		
//		final List<String> routingDebug = (List<String>) RequestContext.getCurrentContext().get("routingDebug");
//		log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));
//	    log.debug(String.format("Post-Request  [%s] Method %s request to %s",request.getRequestedSessionId(), request.getMethod(), request.getRequestURL().toString()));
		try {
			return super.runFilter();
		}
		finally {
			// Simply force a close for every request...
			doCloseSourceInputStream();
		}
	}

	void doCloseSourceInputStream() {
		RequestContext context = RequestContext.getCurrentContext();
		InputStream inputStream = context.getResponseDataStream();
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				// This should actually never happen
				log.error("Failed to close wrapped stream", e);
			}
		}
	}

}