package com.b3.dd.proxy.config;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.constants.ZuulHeaders;
import com.netflix.zuul.context.RequestContext;

@Component
public class LogRequestFilter extends ZuulFilter {

    private static Logger log = LogManager.getLogger(LogRequestFilter.class);
    
    public String filterType() {
      return "pre";
    }

    @Override
    public int filterOrder() {
    	return FilterConstants.SEND_RESPONSE_FILTER_ORDER;
    }

    public boolean shouldFilter() {
//    	System.out.println(RequestContext.getCurrentContext().getRequest()
//    		         .getRequestURI());
    	  return false;
//    			  RequestContext.getCurrentContext().getRequest()
//    		         .getRequestURI().startsWith("/agency");
    }

    public Object run() {
      RequestContext ctx = RequestContext.getCurrentContext();
//      HttpServletRequest request = ctx.getRequest();
//      log.info(String.format("Before filter ['%s': '%s', '%s': '%s']",
//              ZuulHeaders.X_FORWARDED_PROTO.toLowerCase(),
//              ctx.getZuulRequestHeaders().get(ZuulHeaders.X_FORWARDED_PROTO.toLowerCase()),
//              "X-Forwarded-Port",
//              ctx.getZuulRequestHeaders().get("x-forwarded-port")));
      
      
//      log.info(String.format("Request HeaderNames [%s] ",ctx.getRequest().getHeaderNames()));
      final String originalXForwardedProto = "http";
      final String originalXForwardedPort = "81";

      if (!StringUtils.isEmpty(originalXForwardedProto)) {
          ctx.addZuulRequestHeader(ZuulHeaders.X_FORWARDED_PROTO.toLowerCase(), originalXForwardedProto);
      }

      if (!StringUtils.isEmpty(originalXForwardedPort)) {
          ctx.addZuulRequestHeader("x-forwarded-port", originalXForwardedPort);
      }

//      log.info(String.format("After filter ['%s': '%s', '%s': '%s']",
//              ZuulHeaders.X_FORWARDED_PROTO.toLowerCase(),
//              ctx.getZuulRequestHeaders().get(ZuulHeaders.X_FORWARDED_PROTO.toLowerCase()),
//              "X-Forwarded-Port",
//              ctx.getZuulRequestHeaders().get("x-forwarded-port")));
      return super.runFilter();
    }


  }