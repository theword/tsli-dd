package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PERSISTENCY_DETAIL", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewPerDetail.findAll", query = "SELECT v FROM P3ViewPerDetail v")
})

public class P3ViewPerDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 10)
    private String closYm;
	@Column(name = "PERSISTENCY_TYPE", length = 25)
    private String persistencyType;
	
	@Column(name = "CHANNEL", length = 10)
    private String channel;
	
	@Column(name = "GM_CODE", length = 15)
    private String gmCode;
	@Column(name = "GM_NAME", length = 300)
    private String gmName;
	@Column(name = "AVP_CODE", length = 15)
    private String avpCode;
	@Column(name = "AVP_NAME", length = 300)
    private String avpName;
	@Column(name = "AL_CODE", length = 15)
    private String alCode;
	@Column(name = "AL_NAME", length = 300)
    private String alName;
	@Column(name = "AGENT_CODE", length = 15)
    private String agentCode;
	@Column(name = "AGENT_NAME", length = 300)
	private String agentName;
	
	@Column(name = "POLICY_CODE", length = 15)
	private String policyCode;
	@Column(name = "PROD_CD", length = 100)
	private String prodCd;
	@Column(name = "POLICY_STATUS", length = 100)
	private String policyStatus;
	@Column(name = "CONT_YMD", length = 10)
	private String contYmd;
	
	@Column(name = "DUE_DATE", length = 10)
	private String dueDate;
	@Column(name = "YEAR_TERM", length = 11)
	private String yearTerm;
	
	@Column(name = "RYP")
    private Double ryp;
	@Column(name = "TARGET_PC")
    private Double targetPc;
	@Column(name = "COLLECT_PC")
    private Double collectPc;
	@Column(name = "PLUS_PC")
    private Double plusPc;
	
	@Column(name = "CUSTOMER_NAME", length = 250)
	private String customerName;
	@Column(name = "STATUS", length = 1)
	private String status;
	
	public P3ViewPerDetail() {	
	}

	public P3ViewPerDetail(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewPerDetail)) {
            return false;
        }
        P3ViewPerDetail other = (P3ViewPerDetail) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getPersistencyType() {
		return persistencyType;
	}

	public void setPersistencyType(String persistencyType) {
		this.persistencyType = persistencyType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAvpName() {
		return avpName;
	}

	public void setAvpName(String avpName) {
		this.avpName = avpName;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getProdCd() {
		return prodCd;
	}

	public void setProdCd(String prodCd) {
		this.prodCd = prodCd;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getContYmd() {
		return contYmd;
	}

	public void setContYmd(String contYmd) {
		this.contYmd = contYmd;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getYearTerm() {
		return yearTerm;
	}

	public void setYearTerm(String yearTerm) {
		this.yearTerm = yearTerm;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getTargetPc() {
		return targetPc;
	}

	public void setTargetPc(Double targetPc) {
		this.targetPc = targetPc;
	}

	public Double getCollectPc() {
		return collectPc;
	}

	public void setCollectPc(Double collectPc) {
		this.collectPc = collectPc;
	}

	public Double getPlusPc() {
		return plusPc;
	}

	public void setPlusPc(Double plusPc) {
		this.plusPc = plusPc;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
