package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewWholeGroupMtd;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewWholeGroupMtdRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewWholeGroupMtdController {
	@Autowired
	private P3ViewWholeGroupMtdRepository P3ViewWholeGroupMtdRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - I.IV : Under Producer
	@PostMapping("kpidetail-wholegroup-mtd")
	public ResponseEntity<List<P3ViewWholeGroupMtd>>
	getTeamDetailWholeMtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String level = userHelper.getUserPosition();
		List<P3ViewWholeGroupMtd> P3ViewWholeGroupMtd = null;
		
		switch (level) {
		case "AGENT":
			P3ViewWholeGroupMtd = P3ViewWholeGroupMtdRepository.findByAgent(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "AL":
			P3ViewWholeGroupMtd = P3ViewWholeGroupMtdRepository.findByAl(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition() 
			);
			break;
		case "AVP":
			P3ViewWholeGroupMtd = P3ViewWholeGroupMtdRepository.findByAvp(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "GM":
			P3ViewWholeGroupMtd = P3ViewWholeGroupMtdRepository.findByGm(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "ZONE":
			P3ViewWholeGroupMtd = P3ViewWholeGroupMtdRepository.findByGm(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewWholeGroupMtd);
	}
}
