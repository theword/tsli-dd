package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_LEVEL_COMP", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewPerformanceComp.findAll", query = "SELECT v FROM ViewPerformanceComp v")
})
public class ViewPerformanceComp implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 18)
    private String closYm;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "ZONE", length = 4)
    private String zone;
	@Column(name = "ZONE_NAME", length = 20)
    private String zoneName;
	@Column(name = "PC_APPROVE")
	private Double pcApprove;
	@Column(name = "PC_SUBMIT")
	private Double pcSubmit;
	@Column(name = "ACTIVE")
	private Double active;
	@Column(name = "NC")
	private Double nc;
	@Column(name = "RETENTION")
	private Double retention;
	@Column(name = "RYP")
	private Double ryp;
	@Column(name = "RETENTION_MTD_1")
	private Double retentionMtd1;
	@Column(name = "RETENTION_MTD_2")
	private Double retentionMtd2;
	@Column(name = "RETENTION_MTD_3")
	private Double retentionMtd3;
	@Column(name = "RETENTION_MTD_4")
	private Double retentionMtd4;
	@Column(name = "RETENTION_MTD_5")
	private Double retentionMtd5;
	@Column(name = "RETENTION_MTD_6")
	private Double retentionMtd6;
	
	@Column(name = "NO_CASE")
	private Double noCase;
	@Column(name = "CASE_SIZE")
	private Double caseSize;
	
	public ViewPerformanceComp() {
		
	}

	public ViewPerformanceComp(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public Double getPcApprove() {
		return pcApprove;
	}

	public void setPcApprove(Double pcApprove) {
		this.pcApprove = pcApprove;
	}

	public Double getPcSubmit() {
		return pcSubmit;
	}

	public void setPcSubmit(Double pcSubmit) {
		this.pcSubmit = pcSubmit;
	}

	public Double getActive() {
		return active;
	}

	public void setActive(Double active) {
		this.active = active;
	}

	public Double getNc() {
		return nc;
	}

	public void setNc(Double nc) {
		this.nc = nc;
	}

	public Double getRetention() {
		return retention;
	}

	public void setRetention(Double retention) {
		this.retention = retention;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getRetentionMtd1() {
		return retentionMtd1;
	}

	public void setRetentionMtd1(Double retentionMtd1) {
		this.retentionMtd1 = retentionMtd1;
	}

	public Double getRetentionMtd2() {
		return retentionMtd2;
	}

	public void setRetentionMtd2(Double retentionMtd2) {
		this.retentionMtd2 = retentionMtd2;
	}

	public Double getRetentionMtd3() {
		return retentionMtd3;
	}

	public void setRetentionMtd3(Double retentionMtd3) {
		this.retentionMtd3 = retentionMtd3;
	}

	public Double getRetentionMtd4() {
		return retentionMtd4;
	}

	public void setRetentionMtd4(Double retentionMtd4) {
		this.retentionMtd4 = retentionMtd4;
	}

	public Double getRetentionMtd5() {
		return retentionMtd5;
	}

	public void setRetentionMtd5(Double retentionMtd5) {
		this.retentionMtd5 = retentionMtd5;
	}

	public Double getRetentionMtd6() {
		return retentionMtd6;
	}

	public void setRetentionMtd6(Double retentionMtd6) {
		this.retentionMtd6 = retentionMtd6;
	}

	public Double getNoCase() {
		return noCase;
	}

	public void setNoCase(Double noCase) {
		this.noCase = noCase;
	}

	public Double getCaseSize() {
		return caseSize;
	}

	public void setCaseSize(Double caseSize) {
		this.caseSize = caseSize;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewPerformanceComp)) {
            return false;
        }
        ViewPerformanceComp other = (ViewPerformanceComp) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewPerformanceComp [id=" + id + "]";
	}

}
