package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewPolicyPc;
import com.b3.dd.api.model.PaggingModel;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewPolicyPcRepository;

@RestController
@RequestMapping("/${api.prefix}/policy/pc")
public class ViewPolicyPcController {
	private static final int PAGE_SIZE = 500;
	@Autowired
	private ViewPolicyPcRepository ViewPolicyPcRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.
	@PostMapping("/agent")
	public ResponseEntity<List<ViewPolicyPc>> 
	getPolicyPcAgent(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByAgentCode(
				request.getAgentCode(), request.getAgentLevel(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyPc);
	}
	
	// CASE I.I
	@PostMapping("/al")
	public ResponseEntity<List<ViewPolicyPc>> 
	getPolicyPcUnderAl(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByAlCode(
				request.getAlCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyPc);
	}
	
	// CASE I.II
	@PostMapping("/avp")
	public ResponseEntity<List<ViewPolicyPc>> 
	getPolicyPcUnderAvp(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByAvpCode(
				request.getAvpCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyPc);
	}
		
	// CASE I.III
	@PostMapping("/gm")
	public ResponseEntity<List<ViewPolicyPc>> 
	getPolicyPcUnderGm(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByGmCode(
				request.getGmCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyPc);
	}
	
	// CASE II.
	@PostMapping("/company")
	public ResponseEntity<PaggingModel<ViewPolicyPc>> 
	getPolicyPcCompany(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		
		Page<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByCompanyPagination(
				request.getFilter(), userHelper.getUserPosition(), pagging);
		
		return ResponseEntity.ok().body( new PaggingModel<ViewPolicyPc>(ViewPolicyPc.getContent(), ViewPolicyPc.getTotalPages(), ViewPolicyPc.getTotalElements()));
	}
		
	// CASE III.
	@PostMapping("/channel")
	public ResponseEntity<PaggingModel<ViewPolicyPc>> 
	getPolicyPcChannel(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		
		Page<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByChannelPagination(
				request.getChannelCode(), request.getFilter(), userHelper.getUserPosition(), pagging);
		
		return ResponseEntity.ok().body( new PaggingModel<ViewPolicyPc>(ViewPolicyPc.getContent(), ViewPolicyPc.getTotalPages(), ViewPolicyPc.getTotalElements()));
	}
	
	// CASE IV.
	@PostMapping("/zone")
	public ResponseEntity<List<ViewPolicyPc>> 
	getPolicyPcZone(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyPc> ViewPolicyPc = ViewPolicyPcRepository.findByZone(
				request.getZoneCode(), request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyPc);
	}
	
	// CASE VIII findBySearchPagination
	@PostMapping("/search-pagination")
	public ResponseEntity<PaggingModel<ViewPolicyPc>> 
	getSearchPaging(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		
		
		Page<ViewPolicyPc>  ViewPolicyRyp = ViewPolicyPcRepository.findBySearchPagination(
				request.getFilter(), request.getSearchKeyword(), request.getChannelCode(), userHelper.getUserPosition(), pagging);
		
//		request.getSortColName()
		
		
		return ResponseEntity.ok().body( new PaggingModel<ViewPolicyPc>(ViewPolicyRyp.getContent(), ViewPolicyRyp.getTotalPages(), ViewPolicyRyp.getTotalElements()));
	
	}
}
