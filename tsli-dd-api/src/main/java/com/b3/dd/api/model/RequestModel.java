package com.b3.dd.api.model;

public class RequestModel {

	private String closYm;
	private String channelCode;
	private String zoneCode;
	private String gmCode;
	private String avpCode;
	private String alCode;
	private String agentCode;
	private String agentLevel;
	private String filter;
	private Integer paginationPage;
	private String memoType;
	private String searchKeyword;
	private String sortColName;
	private String sortOrderType;
	
	private Integer pcSumYearTarget;
	private Integer activeSumYearTarget;
	private Integer ncSumYearTarget;
	private Integer rypSumYearTarget;
	private Integer retentionSumYearTarget;
	
	private Integer [] pcYearTarget;
	private Integer [] activeYearTarget;
	private Integer [] ncYearTarget;
	private Integer [] rypYearTarget;
	private Integer [] retentionYearTarget;
	
	private String filterType;
	
	private String fileName;
	private Long fileSize;
	private String fileBased64;
	private String fileType;
	private String promotionType;
	
	private String groupType;
	private String periodType;
	private String positionCode;
	
	private Integer promotionConfigId;
	
	private String username;
	
	public String getClosYm() {
		return closYm;
	}
	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}
	public String getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	public String getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getGmCode() {
		return gmCode;
	}
	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}
	public String getAvpCode() {
		return avpCode;
	}
	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}
	public String getAlCode() {
		return alCode;
	}
	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getAgentLevel() {
		return agentLevel;
	}
	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public Integer getPaginationPage() {
		return paginationPage;
	}
	public void setPaginationPage(Integer paginationPage) {
		this.paginationPage = paginationPage;
	}
	public String getMemoType() {
		return memoType;
	}
	public void setMemoType(String memoType) {
		this.memoType = memoType;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	public String getSortColName() {
		return sortColName;
	}
	public void setSortColName(String sortColName) {
		this.sortColName = sortColName;
	}
	public String getSortOrderType() {
		return sortOrderType;
	}
	public void setSortOrderType(String sortOrderType) {
		this.sortOrderType = sortOrderType;
	}
	public Integer getPcSumYearTarget() {
		return pcSumYearTarget;
	}
	public void setPcSumYearTarget(Integer pcSumYearTarget) {
		this.pcSumYearTarget = pcSumYearTarget;
	}
	public Integer getActiveSumYearTarget() {
		return activeSumYearTarget;
	}
	public void setActiveSumYearTarget(Integer activeSumYearTarget) {
		this.activeSumYearTarget = activeSumYearTarget;
	}
	public Integer getNcSumYearTarget() {
		return ncSumYearTarget;
	}
	public void setNcSumYearTarget(Integer ncSumYearTarget) {
		this.ncSumYearTarget = ncSumYearTarget;
	}
	public Integer[] getPcYearTarget() {
		return pcYearTarget;
	}
	public void setPcYearTarget(Integer[] pcYearTarget) {
		this.pcYearTarget = pcYearTarget;
	}
	public Integer[] getActiveYearTarget() {
		return activeYearTarget;
	}
	public void setActiveYearTarget(Integer[] activeYearTarget) {
		this.activeYearTarget = activeYearTarget;
	}
	public Integer[] getNcYearTarget() {
		return ncYearTarget;
	}
	public void setNcYearTarget(Integer[] ncYearTarget) {
		this.ncYearTarget = ncYearTarget;
	}
	public String getFilterType() {
		return filterType;
	}
	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileBased64() {
		return fileBased64;
	}
	public void setFileBased64(String fileBased64) {
		this.fileBased64 = fileBased64;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getPeriodType() {
		return periodType;
	}
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	public String getPositionCode() {
		return positionCode;
	}
	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}
	public Integer getPromotionConfigId() {
		return promotionConfigId;
	}
	public void setPromotionConfigId(Integer promotionConfigId) {
		this.promotionConfigId = promotionConfigId;
	}
	public Integer[] getRypYearTarget() {
		return rypYearTarget;
	}
	public void setRypYearTarget(Integer[] rypYearTarget) {
		this.rypYearTarget = rypYearTarget;
	}
	public Integer getRypSumYearTarget() {
		return rypSumYearTarget;
	}
	public void setRypSumYearTarget(Integer rypSumYearTarget) {
		this.rypSumYearTarget = rypSumYearTarget;
	}
	public Integer getRetentionSumYearTarget() {
		return retentionSumYearTarget;
	}
	public void setRetentionSumYearTarget(Integer retentionSumYearTarget) {
		this.retentionSumYearTarget = retentionSumYearTarget;
	}
	public Integer[] getRetentionYearTarget() {
		return retentionYearTarget;
	}
	public void setRetentionYearTarget(Integer[] retentionYearTarget) {
		this.retentionYearTarget = retentionYearTarget;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
