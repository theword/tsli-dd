package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewAllYtdComp;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewAllYtdCompRepository;

@RestController
@RequestMapping("/${api.prefix}/company")
public class ViewAllYtdCompController {

	@Autowired
	private ViewAllYtdCompRepository ViewAllYtdCompRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("/ytd")
	public ResponseEntity<List<ViewAllYtdComp>> 
	getChannelPerformance(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewAllYtdComp> ViewAllYtdComp = ViewAllYtdCompRepository.findByLevelAndCode(
				request.getAgentLevel(), request.getZoneCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewAllYtdComp);
	}
}
