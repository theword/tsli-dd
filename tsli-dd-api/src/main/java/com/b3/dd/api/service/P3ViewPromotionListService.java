package com.b3.dd.api.service;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.transform.Transformers;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.b3.dd.api.entity.dd.P3ViewPromotionList;
import com.b3.dd.api.utils.ObjectUtil;

@Service
public class P3ViewPromotionListService {

	@PersistenceContext
	private EntityManager em;


	@Transactional
	public List<Map<String, Object>> findByZone(
			String filter
			, String authLevel
			, String zone
			, String groupChannel){
		
		String sql =	
				"  SELECT DISTINCT ID,  CLOS_YM,  CONTEST_CODE,  CONTEST_DESC, TYPE, CREATE_DATE"
				+ " FROM VIEW_PROMOTION_LIST "
				+ " WHERE TYPE = :filter AND :authLevel = 'ZONE' AND ( group_channel= :group_channel  OR ZONE = :ZONE OR  (:ZONE = 'COMPANY' AND :group_channel = 'COMPANY') ) "
				+ " ORDER BY CLOS_YM, CONTEST_DESC, CREATE_DATE DESC";

		List<Map<String, Object>> rs = em.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("filter", filter)
				.setParameter("authLevel", authLevel)
				.setParameter("ZONE", zone)
				.setParameter("group_channel", groupChannel)
				.getResultList();

		return ObjectUtil.columnToCamelCase(rs);
	}



	@Transactional
	public List<Map<String, Object>> findByGmCode(
			String agentCode
			, String filter
			, String authCode
			,  String authLevel){
		
		String sql = 
				"SELECT DISTINCT ID, CLOS_YM,  CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
						+ "	FROM ("
						+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE "
						+ "    FROM VIEW_PROMOTION_LIST"
						+ "    WHERE GM_CODE = :agentCode AND TYPE = :filter"
						+ " 	AND ("
						+ "			GM_CODE = :authCode OR (:authLevel = 'ZONE')"
						+ "		)"
						+ ")"
						+ " ORDER BY CLOS_YM, CONTEST_DESC, CREATE_DATE DESC";

		List<Map<String, Object>> rs = em.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("filter", filter)
				.setParameter("authLevel", authLevel)
				.setParameter("authCode", authCode)
				.setParameter("agentCode", agentCode)
				.getResultList();
		return ObjectUtil.columnToCamelCase(rs);
	}



	public List<?> findByAvpCode(
		String agentCode
			,String filter
			,  String authCode
			,  String authLevel){

		String sql =
				"SELECT DISTINCT ID, CLOS_YM,  CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
						+ "	FROM ("
						+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE " 
						+ "    FROM VIEW_PROMOTION_LIST"
						+ "    WHERE AVP_CODE = :agentCode AND TYPE = :filter"
						+ "		AND ("
						+ "			("
						+ "				GM_CODE = :authCode"
						+ "				OR AVP_CODE = :authCode"
						+ "			)"
						+ "		OR"
						+ "			(:authLevel = 'ZONE')"
						+ " 	)"
						+ ")"
						+ " ORDER BY CLOS_YM, CONTEST_DESC, CREATE_DATE DESC";
		List<Map<String, Object>> rs = em.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("filter", filter)
				.setParameter("authLevel", authLevel)
				.setParameter("authCode", authCode)
				.setParameter("agentCode", agentCode)
				.getResultList();

		return ObjectUtil.columnToCamelCase(rs);

	}
	

	public List<?> findByAlCode(
			String agentCode
			, String filter
			, String authCode
			, String authLevel){
		
		
	String sql =  "SELECT DISTINCT ID, CLOS_YM, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
			+ "	FROM ("
			+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE" 
			+ "    FROM VIEW_PROMOTION_LIST"
			+ "    WHERE AL_CODE = :agentCode AND TYPE = :filter"
			+ "		AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ " )"
			+ ")"
			+ " ORDER BY CLOS_YM, CONTEST_DESC, CREATE_DATE DESC";
		List<Map<String, Object>> rs = em.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("filter", filter)
				.setParameter("authLevel", authLevel)
				.setParameter("authCode", authCode)
				.setParameter("agentCode", agentCode)
				.getResultList();

		return ObjectUtil.columnToCamelCase(rs);

	}
	
	
	public List<?> findByAgentCode(
			 String agentCode
			,String filter
			,String authCode
			, String authLevel){

		String sql = 
				"SELECT DISTINCT ID, CLOS_YM,   CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
				+ "	FROM ("
				+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE " 
				+ "    FROM VIEW_PROMOTION_LIST"
				+ "    WHERE AGENT_CODE = :agentCode AND TYPE = :filter"
				+ "		AND ("
				+ "			("
				+ "				GM_CODE = :authCode"
				+ "				OR AVP_CODE = :authCode"
				+ "				OR AL_CODE = :authCode"
				+ "				OR AGENT_CODE = :authCode"
				+ "			)"
				+ "		OR"
				+ "			(:authLevel = 'ZONE')"
				+ " )"
				+ ")"
				+ " ORDER BY CLOS_YM, CONTEST_DESC, CREATE_DATE DESC";
		
		List<Map<String, Object>> rs = em.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("filter", filter)
				.setParameter("authLevel", authLevel)
				.setParameter("authCode", authCode)
				.setParameter("agentCode", agentCode)
				.getResultList();

		return ObjectUtil.columnToCamelCase(rs);

	}
	

}
