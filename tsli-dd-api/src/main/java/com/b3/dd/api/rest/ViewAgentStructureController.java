package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewAgentStructure;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewAgentStructureRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class ViewAgentStructureController {
	
	@Autowired
	private ViewAgentStructureRepository ViewAgentStructureRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("/structure")
	public ResponseEntity<List<ViewAgentStructure>> 
		getAgentStructure(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewAgentStructure> viewAgentStructure = ViewAgentStructureRepository.findByAgentCode(
				request.getAgentCode(), principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(viewAgentStructure);
	}
}
