package com.b3.dd.api.config.security;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.b3.dd.api.constants.Constants.UserStatus;
import com.b3.dd.api.entity.dd.DdTAgentStructure;
import com.b3.dd.api.entity.dd.Users;
import com.b3.dd.api.entity.dd.UsersLogging.ActionType;
import com.b3.dd.api.model.AuthenticationResponseModel;
import com.b3.dd.api.repository.AgentStructureRepository;
import com.b3.dd.api.repository.UsersRepository;
import com.b3.dd.api.service.AuthenticationService;
import com.b3.dd.api.service.UsersLoggingService;

//@Service
//@Profile(value= {"dev", "vm"})
public class DevCustomUserDetailsService implements UserDetailsService {

	private static final Logger log = LogManager.getLogger(DevCustomUserDetailsService.class);
	
	@Value("${spring.profiles:dev}")
	private String activeProfile;
	private HttpServletRequest httpServletRequest;
	private AuthenticationService authenticationService;
	private AgentStructureRepository agentStructureRepository;
	private PasswordEncoder passwordEncoder;
	private UsersRepository  usersRepository;
	private UsersLoggingService usersLoggingService;
	
	public DevCustomUserDetailsService(
			AuthenticationService authenticationService,
			AgentStructureRepository agentStructureRepository,
			PasswordEncoder passwordEncoder,
			UsersRepository  usersRepository,
			HttpServletRequest httpServletRequest,
			UsersLoggingService usersLoggingService) {
		
		this.agentStructureRepository = agentStructureRepository;
		this.authenticationService = authenticationService;
		this.httpServletRequest = httpServletRequest;
		this.passwordEncoder = passwordEncoder;
		this.usersRepository = usersRepository;
		this.usersLoggingService = usersLoggingService;
		log.info("Initial: CustomUserDetailsService.class");
		
	}

	public CustomUserDetail loadUserByUsername(String username) throws UsernameNotFoundException {
		Map<String, Object> additionalInfo = new HashMap<String, Object>();
		Users user = usersRepository.findByUsername(username);
		System.out.println("usernameusernameusernameusernameusernameusername " + username);
		
		/**
		 * Action Logging
		 */
		usersLoggingService.saveLogging(httpServletRequest, user, null, ActionType.LOGIN);
		
		
		if (user == null) {
			log.error("Could not find the account " + username);
			throw new UsernameNotFoundException("Could not find the account " + username);
		}
		
		if( UserStatus.INACTIVE.equals(user.getStatus()) ) {
			throw new AccessDeniedException ("Account " + username +" is inactive");
		}

		Set<SimpleGrantedAuthority> authority = new HashSet<SimpleGrantedAuthority>();
		List<DdTAgentStructure> structure = agentStructureRepository.findByAgentCode(username);
		for (DdTAgentStructure level : structure) {
			authority.add(new SimpleGrantedAuthority( level.getAgentLevel()));
			log.info("Agent Level: " + level.getAgentLevel());
		}

		/*
		 * Check is super admin
		 */
		additionalInfo.put("isSuperUser", usersRepository.checkSuperUser(username).isPresent());
//		if(user.isStaff() ) {
//			authority.add(new SimpleGrantedAuthority( "STAFF" ));
//			authority.add(new SimpleGrantedAuthority( "ADMIN" ));
//		}

		return new CustomUserDetail(username, user.getPassword(), user, authority, additionalInfo);

	}

}
