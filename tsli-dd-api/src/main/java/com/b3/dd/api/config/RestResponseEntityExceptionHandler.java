package com.b3.dd.api.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.b3.dd.api.model.MessageHandleModel;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final Logger log = LogManager.getLogger(RestResponseEntityExceptionHandler.class);

	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {
		MessageHandleModel body = new MessageHandleModel(ex.getMessage());
		log.info("handleConflict");
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}

	@ExceptionHandler(value = { UnauthorizedUserException.class, InternalAuthenticationServiceException.class })
	protected ResponseEntity<Object> handleUnauthorized(Exception ex, WebRequest request) {
		log.info("HandleUnauthorized");
		log.info(ex.getMessage());
		MessageHandleModel body = new MessageHandleModel(ex.getMessage());
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}

	@ExceptionHandler(value = { AccessDeniedException.class })
	protected ResponseEntity<Object> handleForbidden(Exception ex, WebRequest request) {
		log.info("HandleForbidden");
		log.info(ex.getMessage());
		
		MessageHandleModel body = new MessageHandleModel(ex.getMessage());
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
	}

	@ExceptionHandler(value = { UsernameNotFoundException.class })
	protected ResponseEntity<Object> handleUsernameNotFound(Exception ex, WebRequest request) {
		log.info("HandleUsernameNotFound");
		log.info(ex.getMessage());
		
		MessageHandleModel body = new MessageHandleModel(ex.getMessage());
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}
	

	@ExceptionHandler(value = { InvalidGrantException.class})
	protected ResponseEntity<Object> handleInvalidGrant(Exception ex, WebRequest request) {
		log.info("HandleInvalidGrant");
		log.info("Bad credentials");
		
		MessageHandleModel body = new MessageHandleModel("Bad credentials");
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}
	

	@ExceptionHandler(value = { AuthenticationException.class})
	protected ResponseEntity<Object> handleAuthentication(Exception ex, WebRequest request) {
		log.info("HandleAuthentication");
		log.info(ex.getMessage());
		
		MessageHandleModel body = new MessageHandleModel(ex.getMessage());
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}
}