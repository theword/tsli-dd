package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewRetentionMtd;

@Repository
public interface ViewRetentionMtdRepository extends CrudRepository<ViewRetentionMtd, Integer> {

	// CONDITION I. RETENTION: COMPANY, all channel and zone
	@Query("SELECT v FROM ViewRetentionMtd v"
			+ " WHERE (:authLevel = 'ZONE')")
	public List<ViewRetentionMtd> findByCompany(@Param("authLevel") String authLevel);
		
	// CONDITION II. RETENTION: CHANNEL, only zone under this channel
	@Query("SELECT v FROM ViewRetentionMtd v WHERE (v.agentLevel = 'CHANNEL' AND v.groupChannel = :channelCode) OR v.groupChannel = :channelCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewRetentionMtd> findByChannel(
			@Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel);
			
	// CONDITION III. RETENTION: ZONE
	@Query("SELECT v FROM ViewRetentionMtd v WHERE v.agentLevel = 'ZONE' AND v.zone = :zoneCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewRetentionMtd> findByZone(
			@Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
	
	
		
}
