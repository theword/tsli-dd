package com.b3.dd.api.config.security;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	
	@Value("/${api.prefix}")
	private  String ROOT_PATTERN;
	private final TokenStore tokenStore;
	
	public ResourceServerConfiguration(final TokenStore tokenStore) {
		this.tokenStore = tokenStore;
	}

	@Override
	public void configure(final ResourceServerSecurityConfigurer resources) {
		resources
		.tokenStore(tokenStore);
	}

    @Override
    public void configure(HttpSecurity http) throws Exception {
    	// @formatter:off
        http
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.GET, ROOT_PATTERN).access("#oauth2.hasScope('read')")
        .antMatchers(HttpMethod.POST, ROOT_PATTERN).access("#oauth2.hasScope('write')")
        .antMatchers(HttpMethod.PATCH, ROOT_PATTERN).access("#oauth2.hasScope('write')")
        .antMatchers(HttpMethod.PUT, ROOT_PATTERN).access("#oauth2.hasScope('write')")
        .antMatchers(HttpMethod.DELETE, ROOT_PATTERN).access("#oauth2.hasScope('write')")
        .antMatchers( 
        		ROOT_PATTERN+"/users/signout"
//        		,ROOT_PATTERN+"/policy/nc/memo"
        		, ROOT_PATTERN+"/info/**"
        		, "/oauth/**"
        		,"/h2-console/**"
        		, ROOT_PATTERN+"/lock-engine/deviceValidation/mobile"
        		,"/websocket/**"
        		).permitAll()
        .anyRequest().authenticated();
		// @formatter:on
    }

 
    
    public class CustomOAuth2AccessDeniedHandler extends OAuth2AccessDeniedHandler{
    	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException authException)
    			throws IOException, ServletException {
    		
    		doHandle(request, response, authException);
    		
    	}
    }
}
