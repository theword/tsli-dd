package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PROMOTION_DETAIL", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewPromotionDetail.findAll", query = "SELECT v FROM P3ViewPromotionDetail v")
})
public class P3ViewPromotionDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 18)
    private String closYm;
	@Column(name = "GROUP_CHANNEL", length = 20)
    private String groupChannel;
	@Column(name = "ZONE", length = 20)
    private String zone;
	
	@Column(name = "GM_CODE", length = 20)
    private String gmCode;
	@Column(name = "GM_NAME", length = 300)
    private String gmName;
	@Column(name = "AVP_CODE", length = 20)
    private String avpCode;
	@Column(name = "AVP_NAME", length = 300)
    private String avpName;
	@Column(name = "AL_CODE", length = 20)
    private String alCode;
	@Column(name = "AL_NAME", length = 300)
    private String alName;
	@Column(name = "AGENT_CODE", length = 20)
    private String agentCode;
	@Column(name = "AGENT_POSITION", length = 300)
    private String agentPosition;
	@Column(name = "AGENT_NAME", length = 300)
    private String agentName;
	
	@Column(name = "CONTEST_CODE", length = 64)
    private String contestCode;
	@Column(name = "CONTEST_DESC")
    private String contestDesc;
	
	@Column(name = "DESCRIPTION")
    private String description;
	@Column(name = "DETAIL", length = 300)
    private String detail;
	
	@Column(name = "PROMOTION_CONFIG_ID", length = 32)
    private Double promotionConfigId;
	
	@Column(name = "HEADER")
    private String header;
	@Column(name = "TYPE", length = 1024)
    private String type;
	
	public P3ViewPromotionDetail() {	
	}

	public P3ViewPromotionDetail(Integer id) {
		super();
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewPromotionDetail)) {
            return false;
        }
        P3ViewPromotionDetail other = (P3ViewPromotionDetail) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAvpName() {
		return avpName;
	}

	public void setAvpName(String avpName) {
		this.avpName = avpName;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentPosition() {
		return agentPosition;
	}

	public void setAgentPosition(String agentPosition) {
		this.agentPosition = agentPosition;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getContestCode() {
		return contestCode;
	}

	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	public String getContestDesc() {
		return contestDesc;
	}

	public void setContestDesc(String contestDesc) {
		this.contestDesc = contestDesc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Double getPromotionConfigId() {
		return promotionConfigId;
	}

	public void setPromotionConfigId(Double promotionConfigId) {
		this.promotionConfigId = promotionConfigId;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
