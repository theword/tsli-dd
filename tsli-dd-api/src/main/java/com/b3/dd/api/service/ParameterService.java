package com.b3.dd.api.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

@Service
public class ParameterService {

	@PersistenceContext
	private EntityManager em;
	private static final String SQL = "SELECT TO_CHAR(PARAM_VALUE ) PARAM_VALUE  FROM CB_T_PARAMETER WHERE NVL(IS_DELETE, 'N' ) <> 'Y' AND PARAM_NAME = :paramName";

	public String getParameter(String paramName) {

		try {

			return (String) em.createNativeQuery(SQL)
					.setParameter("paramName", paramName)
					.getSingleResult();

		} catch (Exception e) {
			return null;
		}

	}
}
