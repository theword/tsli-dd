package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewCompensationConfig;

@Repository
public interface P3ViewCompensationConfigRepository extends CrudRepository<P3ViewCompensationConfig, Integer> {
	// CASE I.I - findConfig
	@Query(value=""
			+ "SELECT * FROM VIEW_COMPENSATION_CONFIG"
			+ " WHERE CLOS_YM = :closYm"
			+ "		AND GROUP_CHANNEL = :channelCode"
			+ "		AND AGENT_LEVEL = :agentLevel"
			+ "		AND COMPENSATION_DESC = :filter"
			+ "	ORDER BY PC, RATE ASC"
	, nativeQuery = true)
	public List<P3ViewCompensationConfig> findConfig(
			@Param("closYm") String closYm
			, @Param("channelCode") String channelCode
			, @Param("agentLevel") String agentLevel
			, @Param("filter") String filter);
}
