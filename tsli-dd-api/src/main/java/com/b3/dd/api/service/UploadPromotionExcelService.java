package com.b3.dd.api.service;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;

import com.b3.dd.api.constants.Constants;
import com.b3.dd.api.entity.dd.DdTPromotionConfig;
import com.b3.dd.api.entity.dd.DdTPromotionDetail;
import com.b3.dd.api.model.FileUploadModel;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@org.springframework.context.annotation.Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UploadPromotionExcelService {
	
	private static final Logger log = LogManager.getLogger(UploadPromotionExcelService.class);
	private static final String SPLITER = "||";
	private static final  String[] DATA_COLUMN = { "closYm", "contestCode", "contestDesc", "agentCode", "agentPosition" };
	private  final static  DecimalFormat DF = new DecimalFormat("#,##0.00");
	private FileUploadModel myFile;
	private String username;
	private String promotionConfigType;
	private DdTPromotionConfig config;
	
	private int FIRST_ROW = 0;
	private int SHEET_DATA = 0;
	private int START_CELL = 5;
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private EntityManager em;
	
	
	public UploadPromotionExcelService(FileUploadModel file, String username, String promotionConfigType) {
		this.myFile = file;
		this.username = username;
		this.promotionConfigType = promotionConfigType;
		this.config = this.readHeaderConfig();
		
	}

	@Transactional
	public void importFile() throws IOException {

		InputStream is = null;

		try {
			log.info("Start import excel file: " + this.myFile.getFilename() );
			log.info("On: "+ Calendar.getInstance().getTime() );
			
			is = new ByteArrayInputStream(decodeString(this.myFile.getData()));

			Workbook workbooks = WorkbookFactory.create(is);
			Sheet sheet = workbooks.getSheetAt(SHEET_DATA);
			log.info("Start read data row" );
			List<DdTPromotionDetail> dataRow = readRow(sheet);
			config.setHeader(readCellData(sheet.getRow(FIRST_ROW)));
			config.setDdTPromotionDetailList(dataRow);
			log.info("End read data row [" + dataRow.size() +"] record");
			workbooks.close();

			
		} catch (IOException e) {
			
			config.setStatus(Constants.UploadStatus.FAIL);
			config.setStatusDetail(e.getMessage());
			throw new IOException(e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				log.error(e);
				config.setStatus(Constants.UploadStatus.FAIL);
				config.setStatusDetail(e.getMessage());
				throw new IOException(e);
			}
		}
		log.info("Start insert data to database on [" + Calendar.getInstance().getTime()  +"]");
		em.persist(config);
		log.info("End process on [" + Calendar.getInstance().getTime()  +"]");
	}

	private byte[] decodeString( String urlBase64) {
		String[] data = urlBase64.split("base64,");
		return Base64.getDecoder().decode(data[1]);
	}
	
	private DdTPromotionConfig readHeaderConfig() {

		DdTPromotionConfig config = new DdTPromotionConfig();
		config.setCreateBy(username);
		config.setCreateDate(Calendar.getInstance().getTime());
		config.setFileName(this.myFile.getFilename());
		config.setIsDelete(Constants.DeleteStatus.FALSE);
		config.setStatus(Constants.UploadStatus.SUCCESS);
		config.setType(promotionConfigType);

		return config;
	}

	private List<DdTPromotionDetail> readRow(Sheet sheet) {

		List<DdTPromotionDetail> l = new ArrayList<DdTPromotionDetail>();

		for (int r = FIRST_ROW + 1; r <= sheet.getLastRowNum(); r++) {
			Row row = sheet.getRow(r);
			DdTPromotionDetail detail = readCellConfigData(row);
			detail.setDetail(readCellData(row));
			detail.setCreateDate(Calendar.getInstance().getTime());
			detail.setCreateBy(username);
			detail.setPromotionConfigId(this.config);
			l.add(detail);
		}

		return l;
	}

	private String readCellData(Row row) {
		int index = 0;
		List<String> value = new ArrayList<String>();
		for (Cell cell = row.getCell(START_CELL + index); cell != null; cell = row.getCell(START_CELL + ++index)) {
			value.add(toCellString(cell));
		}
		
		String out = "[" + String.join(SPLITER, value) +"]";
		return out;
	}

	private DdTPromotionDetail readCellConfigData(Row row) {

		Map<String, String> data = new HashMap<String, String>();
		for (int i = 0; i < DATA_COLUMN.length; i++) {
			String value = toCellString( row.getCell(i) );
			data.put(DATA_COLUMN[i], value );
		}
		return mapper.convertValue(data, DdTPromotionDetail.class);
	}

	private String toCellString(Cell cell) {

		if (cell == null)
			return "";
		Object o = null;
		CellType type = cell.getCellType();
		if (type == CellType.STRING) {
			o = cell.getRichStringCellValue();
		} else if (type == CellType.NUMERIC) {
			o = DF.format(cell.getNumericCellValue());
		} else if (type == CellType.BOOLEAN) {
			o = cell.getBooleanCellValue();
		} else if (type == CellType.BLANK) {
			o = "";
		} else {
			o = new String("");
		}
		return String.format("%s", o.toString());
	}
	
}
