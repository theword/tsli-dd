/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "DD_T_PROMOTION_DETAIL", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DdTPromotionDetail.findAll", query = "SELECT d FROM DdTPromotionDetail d")
    , @NamedQuery(name = "DdTPromotionDetail.findById", query = "SELECT d FROM DdTPromotionDetail d WHERE d.id = :id")
    , @NamedQuery(name = "DdTPromotionDetail.findByClosYm", query = "SELECT d FROM DdTPromotionDetail d WHERE d.closYm = :closYm")
    , @NamedQuery(name = "DdTPromotionDetail.findByContestCode", query = "SELECT d FROM DdTPromotionDetail d WHERE d.contestCode = :contestCode")
    , @NamedQuery(name = "DdTPromotionDetail.findByAgentCode", query = "SELECT d FROM DdTPromotionDetail d WHERE d.agentCode = :agentCode")
    , @NamedQuery(name = "DdTPromotionDetail.findByAgentPosition", query = "SELECT d FROM DdTPromotionDetail d WHERE d.agentPosition = :agentPosition")
    , @NamedQuery(name = "DdTPromotionDetail.findByCreateDate", query = "SELECT d FROM DdTPromotionDetail d WHERE d.createDate = :createDate")
    , @NamedQuery(name = "DdTPromotionDetail.findByCreateBy", query = "SELECT d FROM DdTPromotionDetail d WHERE d.createBy = :createBy")
    , @NamedQuery(name = "DdTPromotionDetail.findByUpdateDate", query = "SELECT d FROM DdTPromotionDetail d WHERE d.updateDate = :updateDate")
    , @NamedQuery(name = "DdTPromotionDetail.findByUpdateBy", query = "SELECT d FROM DdTPromotionDetail d WHERE d.updateBy = :updateBy")
    , @NamedQuery(name = "DdTPromotionDetail.findByIsDelete", query = "SELECT d FROM DdTPromotionDetail d WHERE d.isDelete = :isDelete")})
public class DdTPromotionDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, precision = 32, scale = 0, insertable = true, updatable = true)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "CLOS_YM", nullable = false, length = 18)
    private String closYm;
    @Column(name = "CONTEST_CODE", length = 64)
    private String contestCode;
    @Lob
    @Column(name = "CONTEST_DESC")
    private String contestDesc;
    @Basic(optional = false)
    @Column(name = "AGENT_CODE", nullable = false, length = 30)
    private String agentCode;
    @Column(name = "AGENT_POSITION", length = 30)
    private String agentPosition;
    @Basic(optional = false)
    @Lob
    @Column(name = "DETAIL", nullable = false)
    private String detail;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY", length = 255)
    private String createBy;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY", length = 255)
    private String updateBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @JoinColumn(name = "PROMOTION_CONFIG_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DdTPromotionConfig promotionConfigId;

    public DdTPromotionDetail() {
    }

    public DdTPromotionDetail(BigDecimal id) {
        this.id = id;
    }

    public DdTPromotionDetail(BigDecimal id, String closYm, String agentCode, String detail, Date createDate) {
        this.id = id;
        this.closYm = closYm;
        this.agentCode = agentCode;
        this.detail = detail;
        this.createDate = createDate;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getClosYm() {
        return closYm;
    }

    public void setClosYm(String closYm) {
        this.closYm = closYm;
    }

    public String getContestCode() {
        return contestCode;
    }

    public void setContestCode(String contestCode) {
        this.contestCode = contestCode;
    }

    public String getContestDesc() {
        return contestDesc;
    }

    public void setContestDesc(String contestDesc) {
        this.contestDesc = contestDesc;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentPosition() {
        return agentPosition;
    }

    public void setAgentPosition(String agentPosition) {
        this.agentPosition = agentPosition;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public DdTPromotionConfig getPromotionConfigId() {
        return promotionConfigId;
    }

    public void setPromotionConfigId(DdTPromotionConfig promotionConfigId) {
        this.promotionConfigId = promotionConfigId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTPromotionDetail)) {
            return false;
        }
        DdTPromotionDetail other = (DdTPromotionDetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.b3.dd.api.entity.DdTPromotionDetail[ id=" + id + " ]";
    }
    
}
