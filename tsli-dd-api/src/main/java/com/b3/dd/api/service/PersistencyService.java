package com.b3.dd.api.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.b3.dd.api.entity.dd.P3ViewPerSummary;

@Service
public class PersistencyService {

	private static final Logger log = LogManager.getLogger(PersistencyService.class);
			private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
	@PersistenceContext
	private EntityManager em;


	public List<P3ViewPerSummary> getBlockMonth( String filter, String agentCode, String agentLevel, List<P3ViewPerSummary> lists ){

		Object[] o = (Object[]) em.createNativeQuery(
				" SELECT PERSISTENCY_CLOS_YM, PERSISTENCY_START_YM, PERSISTENCY_END_YM FROM DD_T_PERSISTENCY_CONFIG WHERE PERSISTENCY_CLOS_YM IN ( " 
						+ "    SELECT MIN(CLOS_YM) FROM DD_T_CLOS_CALENDAR WHERE FLAG_END_CLOSING_YMD = 'F' "  
						+ ") AND PERSISTENCY_TYPE = :filter")
				.setParameter("filter", filter)
				.getSingleResult();


		if( o == null ) {
			return lists;
		}

		String start = (String) o[1], end = (String)o[2];

		try {
			/**
			 * Generate default all block 12 month in the Set
			 */
			Set<String> period = getSetOfPeriod(start, end);
			
			/**
			 * That the block is present then remove default the block
			 */
			for(P3ViewPerSummary list : lists) {
				if( period.contains( list.getClosYm() ) ) {
					period.remove(list.getClosYm());
				}
			}

			/**
			 * Dummy invisible month from datain
			 */
			for( String s : period ) {
				P3ViewPerSummary tmp = new P3ViewPerSummary();
				tmp.setAgentCode(agentCode);
				tmp.setAgentLevel(agentLevel);
				tmp.setClosYm(s);
				tmp.setCollectPc(0D);
				tmp.setPlusPc(0D);
				tmp.setTargetPc(0D);
				tmp.setPersistencyType(filter);
				lists.add(tmp);
			}
			// end

			
			/**
			 * Change CLOS_YM to issue date
			 * CLOS_YM-13 month when persistency 13 
			 * CLOS_YM-25 month where persistency 25
			 */
			Calendar cal = Calendar.getInstance();
			for(int i =0; i< lists.size(); i++ ) {
				P3ViewPerSummary s = lists.get(i);
				cal.setTime(sdf.parse(s.getClosYm()));
				cal.add(Calendar.MONTH, -1*Integer.parseInt(filter));
				s.setClosYm(sdf.format(cal.getTime()));
			}
			Collections.sort(lists, Collections.reverseOrder());
		        

		}catch ( ParseException e) {
			e.printStackTrace();
			log.error(e);
		}


		return lists;
	}

	private Set<String> getSetOfPeriod( String start, String end) throws ParseException {
		Set<String> row = new HashSet<String>();
		Calendar cstart = Calendar.getInstance(), cend = Calendar.getInstance();
		cstart.setTime( sdf.parse(start));
		cend.setTime(sdf.parse(end));
	
		

	
		
//		System.out.println("Start "+ start);
//		System.out.println("End "+ end);
//		
		while(cend.after(cstart) ) {
//			System.out.println( cend.getTime()  );
			row.add(sdf.format(cend.getTime()));
			cend.add(Calendar.MONTH, -1);
//			System.out.println( cend.getTime()  );
//			System.out.println("---------------------");
		}
		/*
		 * Last month
		 */
		row.add(start);
		
//		System.out.println(row);
		return row;
	}
}
