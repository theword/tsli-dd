package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewPromotionList;

/*
	// CASE I.I find by GM_CODE
	@Query(value=""
			+ "SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
			+ "	FROM ("
			+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE, ROW_NUMBER()" 
			+ "        OVER (PARTITION BY CONTEST_CODE ORDER BY CONTEST_CODE) as CONTEST_ID"
			+ "    FROM VIEW_PROMOTION_LIST"
			+ "    WHERE GM_CODE = :agentCode AND TYPE = :filter"
			+ " )"
			+ " WHERE CONTEST_ID = 1"
			+ "		AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			, nativeQuery = true)
	public List<P3ViewPromotionList> findByGmCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II find by AVP_CODE
	@Query(value=""
			+ "SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
			+ "	FROM ("
			+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE, ROW_NUMBER()" 
			+ "        OVER (PARTITION BY CONTEST_CODE ORDER BY CONTEST_CODE) as CONTEST_ID"
			+ "    FROM VIEW_PROMOTION_LIST"
			+ "    WHERE AVP_CODE = :agentCode AND TYPE = :filter"
			+ " )"
			+ " WHERE CONTEST_ID = 1"
			+ "		AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY CREATE_DATE DESC"
			, nativeQuery = true)
	public List<P3ViewPromotionList> findByAvpCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III find by AL_CODE
	@Query(value=""
			+ "SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
			+ "	FROM ("
			+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE, ROW_NUMBER()" 
			+ "        OVER (PARTITION BY CONTEST_CODE ORDER BY CONTEST_CODE) as CONTEST_ID"
			+ "    FROM VIEW_PROMOTION_LIST"
			+ "    WHERE AL_CODE = :agentCode AND TYPE = :filter"
			+ " )"
			+ " WHERE CONTEST_ID = 1"
			+ "		AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY CREATE_DATE DESC"
			, nativeQuery = true)
	public List<P3ViewPromotionList> findByAlCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV find by AGENT_CODE
	@Query(value=""
			+ "SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE"
			+ "	FROM ("
			+ "    SELECT ID, CLOS_YM, GM_CODE, AVP_CODE, AL_CODE, AGENT_CODE, AGENT_POSITION, CONTEST_CODE, CONTEST_DESC, TYPE, CREATE_DATE, ROW_NUMBER()" 
			+ "        OVER (PARTITION BY CONTEST_CODE ORDER BY CONTEST_CODE) as CONTEST_ID"
			+ "    FROM VIEW_PROMOTION_LIST"
			+ "    WHERE AGENT_CODE = :agentCode AND TYPE = :filter"
			+ " )"
			+ " WHERE CONTEST_ID = 1"
			+ "		AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY CREATE_DATE DESC"
			, nativeQuery = true)
	public List<P3ViewPromotionList> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV find by ZONE
	@Query(value=""
			+ "    SELECT DISTINCT ID, CLOS_YM,  CONTEST_CODE, CONTEST_DESC, TYPE" 
			+ "    FROM VIEW_PROMOTION_LIST " 
			+ "    WHERE TYPE = :filter AND (:authLevel = 'ZONE') " 
			+" "
			, nativeQuery = true)
	public List<P3ViewPromotionList> findByZone(
			@Param("filter") String filter
			, @Param("authLevel") String authLevel);
}
*/
