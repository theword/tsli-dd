package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.DdTUploadHistory;

@Repository
public interface DdTUploadHistoryRepository extends CrudRepository<DdTUploadHistory, Integer> {

	@Query(value=""
			+ "SELECT * FROM DD_T_UPLOAD_HISTORY"
			+ "	WHERE TYPE = :type"
	, nativeQuery = true)
	public List<DdTUploadHistory> findUploadHistoryByType(
			@Param("type") String type
	);
}
