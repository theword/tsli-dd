package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_RYP_YTD_AGENT", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewYtdAgentRyp.findAll", query = "SELECT v FROM ViewYtdAgentRyp v")
})
public class ViewYtdAgentRyp implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 4)
    private String closYm;
	@Column(name = "AGENT_CODE", length = 30)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	@Column(name = "RYP_TARGET")
    private Double rypTarget;
	@Column(name = "RYP")
	private Double ryp;
	@Column(name = "PERCENT")
	private Double percent;
	@Column(name = "GM_CODE", length = 30)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 30)
    private String avpCode;
	@Column(name = "AL_CODE", length = 30)
    private String alCode;
	
	public ViewYtdAgentRyp() {
		
	}

	public ViewYtdAgentRyp(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public Double getRypTarget() {
		return rypTarget;
	}

	public void setRypTarget(Double rypTarget) {
		this.rypTarget = rypTarget;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getPercent() {
		return percent;
	}

	public void setPercent(Double percent) {
		this.percent = percent;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewYtdAgentRyp)) {
            return false;
        }
        ViewYtdAgentRyp other = (ViewYtdAgentRyp) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewYtdAgentRyp [id=" + id + "]";
	}
}
