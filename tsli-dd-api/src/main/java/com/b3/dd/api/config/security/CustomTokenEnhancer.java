package com.b3.dd.api.config.security;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;


public class CustomTokenEnhancer implements TokenEnhancer {

    public OAuth2AccessToken enhance(
            OAuth2AccessToken accessToken,
            OAuth2Authentication authentication) {
        Map<String, Object> additionalInfo = new HashMap<String, Object>();

        //Read user principal
        CustomUserDetail principal = (CustomUserDetail) authentication.getPrincipal();
        additionalInfo.put("agentCode", principal.getInfo().getUsername());
        additionalInfo.put("name", principal.getInfo().getName()+ " " +principal.getInfo().getSurname());
        additionalInfo.put("role", String.join(",", principal.getPosition()));
        additionalInfo.put("status", principal.getInfo().getStatus());
        additionalInfo.putAll(principal.getOptional());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(
                additionalInfo);
        return accessToken;
    }
}
