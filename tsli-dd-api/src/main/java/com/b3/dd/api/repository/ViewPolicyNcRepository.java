package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewPolicyNc;
import com.b3.dd.api.entity.dd.ViewPolicyNcMemo;

@Repository
public interface ViewPolicyNcRepository extends CrudRepository<ViewPolicyNc, Integer> {
	// CASE I. - COMPANY
	@Query(value=""
			+ "SELECT * "
			+ " FROM VIEW_POLICY_NC"
			+ "	WHERE AGENT_TYPE = :filter"
			+ "		AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<ViewPolicyNc> findByCompany(
			@Param("filter") String filter
			, @Param("authLevel") String authLevel);
	
	// CASE II. - CHANNEL
	@Query(value=""
			+ "SELECT * "
			+ " FROM VIEW_POLICY_NC"
			+ "	WHERE AGENT_TYPE = :filter"
			+ "		AND CHANNEL = :channelCode"
			+ "		AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<ViewPolicyNc> findByChannel(
			@Param("filter") String filter
			, @Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel);
	
	// CASE III. - ZONE
	@Query(value=""
			+ "SELECT * "
			+ " FROM VIEW_POLICY_NC"
			+ "	WHERE AGENT_TYPE = :filter"
			+ "		AND ZONE = :zoneCode"
			+ "		AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<ViewPolicyNc> findByZone(
			@Param("filter") String filter
			, @Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.I
	@Query("SELECT v FROM ViewPolicyNc v WHERE v.alCode = :alCode"
			+ "	AND v.agentType = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :alCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyNc> findByAlCode(
			@Param("alCode") String alCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II
	@Query("SELECT v FROM ViewPolicyNc v WHERE v.avpCode = :avpCode"
			+ "	AND v.agentType = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :avpCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyNc> findByAvpCode(
			@Param("avpCode") String avpCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III
	@Query("SELECT v FROM ViewPolicyNc v WHERE v.gmCode = :gmCode"
			+ "	AND v.agentType = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :gmCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyNc> findByGmCode(
			@Param("gmCode") String gmCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	
	@Query("select v from ViewPolicyNcMemo v "
			+ "where v.agentCode = :agentCode "
			+ "and v.memoType = :memoType")
	public List<ViewPolicyNcMemo> findMomo(	
			@Param("agentCode") String agentCode
			, @Param("memoType") String memoType);
	
}
