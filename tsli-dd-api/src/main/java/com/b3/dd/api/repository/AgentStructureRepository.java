package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.DdTAgentStructure;

@Repository
public interface AgentStructureRepository extends CrudRepository<DdTAgentStructure, Integer> {
	
	@Query("SELECT d FROM DdTAgentStructure d WHERE d.agentCode = :agentCode")
	List<DdTAgentStructure> findByAgentCode(@Param("agentCode") String agentCode);

}
