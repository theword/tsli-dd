package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_WHOLEGROUP_YTD", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewWholeGroupYtd.findAll", query = "SELECT v FROM P3ViewWholeGroupYtd v")
})
public class P3ViewWholeGroupYtd implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 18)
    private String closYm;
	
	@Column(name = "GM_CODE", length = 20)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 20)
    private String avpCode;
	@Column(name = "AL_CODE", length = 20)
    private String alCode;
	@Column(name = "AGENT_CODE", length = 25)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 5)
    private String agentLevel;
	
	@Column(name = "PC_APPROVE")
    private Double pcApprove;
	@Column(name = "PC_SUBMIT")
    private Double pcSubmit;
	@Column(name = "NC")
    private Double nc;
	@Column(name = "RYP")
    private Double ryp;
	@Column(name = "ACTIVE")
    private Double active;
	
	@Column(name = "PC_PERCENT")
    private Double pcPercent;
	@Column(name = "ACTIVE_PERCENT")
    private Double activePercent;
	@Column(name = "NC_PERCENT")
    private Double ncPercent;
	@Column(name = "RYP_PERCENT")
    private Double rypPercent;
	
	@Column(name = "NO_CASE")
	private Double noCase;
	@Column(name = "CASE_SIZE")
	private Double caseSize;
	
	
	public P3ViewWholeGroupYtd() {	
	}

	public P3ViewWholeGroupYtd(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewWholeGroupYtd)) {
            return false;
        }
        P3ViewWholeGroupYtd other = (P3ViewWholeGroupYtd) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public Double getPcApprove() {
		return pcApprove;
	}

	public void setPcApprove(Double pcApprove) {
		this.pcApprove = pcApprove;
	}

	public Double getPcSubmit() {
		return pcSubmit;
	}

	public void setPcSubmit(Double pcSubmit) {
		this.pcSubmit = pcSubmit;
	}

	public Double getNc() {
		return nc;
	}

	public void setNc(Double nc) {
		this.nc = nc;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getActive() {
		return active;
	}

	public void setActive(Double active) {
		this.active = active;
	}

	public Double getPcPercent() {
		return pcPercent;
	}

	public void setPcPercent(Double pcPercent) {
		this.pcPercent = pcPercent;
	}

	public Double getActivePercent() {
		return activePercent;
	}

	public void setActivePercent(Double activePercent) {
		this.activePercent = activePercent;
	}

	public Double getNcPercent() {
		return ncPercent;
	}

	public void setNcPercent(Double ncPercent) {
		this.ncPercent = ncPercent;
	}

	public Double getRypPercent() {
		return rypPercent;
	}

	public void setRypPercent(Double rypPercent) {
		this.rypPercent = rypPercent;
	}

	public Double getNoCase() {
		return noCase;
	}

	public void setNoCase(Double noCase) {
		this.noCase = noCase;
	}

	public Double getCaseSize() {
		return caseSize;
	}

	public void setCaseSize(Double caseSize) {
		this.caseSize = caseSize;
	}

}
