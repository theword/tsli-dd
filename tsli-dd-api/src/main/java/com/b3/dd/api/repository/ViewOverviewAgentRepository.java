package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewOverviewAgent;

@Repository
public interface ViewOverviewAgentRepository extends CrudRepository<ViewOverviewAgent, Integer> {

	// CASE I: Get agent performance in the current month or previous month if current month unavailable
	@Query(value=""+ 
			" SELECT ROW_NUMBER() OVER (ORDER BY 1 ASC) AS ID ,CLOS_YM, GROUP_CHANNEL, ZONE, GM_CODE, GM_POSITION, GM_NAME  " + 
			" , AVP_CODE, AVP_POSITION, AVP_NAME " + 
			" , AL_CODE, AL_POSITION , AL_NAME " + 
			" , AGENT_CODE, AG_POSITION, AGENT_LEVEL, LEVEL_NAME " + 
			" , AGENT_NAME, AGENT_SURNAME " + 
			" , nvl(SUM(ACTIVE_TARGET),0) TARGET_ACT, nvl(SUM(NC_TARGET),0) TARGET_NC " + 
			" , nvl(SUM(PC_TARGET),0) TARGET_PC, nvl(SUM(RT_TARGET),0) TARGET_RT " + 
			" , nvl(SUM(RYP_TARGET),0) TARGET_RYP " + 
			" , PC_SUBMIT, PC_APPROVE, PC_APPROVE_BONUS " + 
			" , NC, ACTIVE, RYP, PRST_RTE," + 
			" case " + 
			"     when SUM(PC_TARGET) > 0 then ROUND((PC_APPROVE/SUM(PC_TARGET))*100,2) " + 
			"     ELSE 0 " + 
			" END PC_PERCENT, " + 
			"  case " + 
			"     when SUM(NC_TARGET) > 0 then ROUND((NC/SUM(NC_TARGET))*100,2) " + 
			"     ELSE 0 " + 
			" END NC_PERCENT, " + 
			"  case " + 
			"     when SUM(ACTIVE_TARGET) > 0 then ROUND((ACTIVE/SUM(ACTIVE_TARGET))*100,2) " + 
			"     ELSE 0 " + 
			" END ACTIVE_PERCENT, " + 
			"  case " + 
			"     when SUM(RYP_TARGET) > 0 then ROUND((RYP/SUM(RYP_TARGET))*100,2) " + 
			"     ELSE 0 " + 
			" END RYP_PERCENT " + 
			"    FROM VIEW_TARGET_AGENT T1  " + 
			"    RIGHT JOIN VIEW_LEVEL_AGENT T2 ON (T1.CODE = T2.AGENT_CODE AND T1.KPI_LEVEL = T2.AGENT_LEVEL) " + 
			"    INNER JOIN (SELECT MIN(CLOS_YM) AS MIN_CLOS_YM " + 
			"  FROM DD_T_CLOS_CALENDAR " + 
			"  WHERE FLAG_END_CLOSING_YMD = 'F') T3 ON (T2.CLOS_YM = T3.MIN_CLOS_YM)  " + 
			"    PIVOT ( SUM(case " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '01' then TARGET_M1 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '02' then TARGET_M2 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '03' then TARGET_M3 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '04' then TARGET_M4 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '05' then TARGET_M5 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '06' then TARGET_M6 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '07' then TARGET_M7 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '08' then TARGET_M8 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '09' then TARGET_M9 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '10' then TARGET_M10 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '11' then TARGET_M11 " + 
			"      when SUBSTR(MIN_CLOS_YM,5,6) = '12' then TARGET_M12 " + 
			"      ELSE 0 " + 
			"  END ) target " + 
			"     FOR (KPI_NAME)  " + 
			"     IN ('NEW_ACTIVE_AGENT' active " + 
			"     , 'NEW_CODE' nc " + 
			"     , 'PC' pc " + 
			"     , 'RETENTION' rt " + 
			"     , 'RYP' ryp " + 
			"     ) " + 
			" )  " + 
			"   WHERE ( "
			+ " 		(GM_CODE = :authCode OR AVP_CODE = :authCode OR AL_CODE = :authCode OR AGENT_CODE = :authCode) "
			+ " 	OR "
			+ " 		(:authLevel = 'ZONE') "
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	) AND " + 
			" AGENT_CODE = :agentCode AND AGENT_LEVEL = :agentLevel " + 
			"    GROUP BY AGENT_CODE, CLOS_YM, PC_SUBMIT, PC_APPROVE, PC_APPROVE_BONUS, NC, ACTIVE, RYP, PRST_RTE,"
			+ " GM_CODE, GM_POSITION, GM_NAME, AVP_CODE, AVP_POSITION, AVP_NAME, AL_CODE, AL_POSITION, AL_NAME, AG_POSITION, AGENT_LEVEL, LEVEL_NAME, AGENT_NAME, AGENT_SURNAME, ZONE, GROUP_CHANNEL" + 
			"", nativeQuery = true)
	public List<ViewOverviewAgent> findByAgentCurrentMonthPerformance(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE II: Get agent (AG) under AL
	@Query("SELECT v FROM ViewOverviewAgent v WHERE v.alCode = :agentCode"
			+ "	AND v.agentLevel = 'AGENT'"
			+ " AND ("
			+ "	(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "	OR "
			+ "	(:authLevel = 'ZONE')"
			+ "	OR"
			+ "	(:authCode IN (SELECT v2.agentCode "
			+ "		FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "	)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewOverviewAgent> findByUnderAlOverview(
			@Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE III: Get agent under AVP based on agentLevel (AL or AG)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE v.avpCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "	(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "	OR "
			+ "	(:authLevel = 'ZONE')"
			+ "	OR"
			+ "	(:authCode IN (SELECT v2.agentCode "
			+ "		FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "	)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewOverviewAgent> findByUnderAvpOverview(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE IV: Get agent under GM based on agentLevel (AVP or AL or AG)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE v.gmCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "	(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "	OR "
			+ "	(:authLevel = 'ZONE')"
			+ "	OR"
			+ "	(:authCode IN (SELECT v2.agentCode "
			+ "		FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "	)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewOverviewAgent> findByUnderGmOverview(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE V: Get agent under ZONE based on agentLevel (GM or AVP or AL or AG)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE v.zone = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "	(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "	OR "
			+ "	(:authLevel = 'ZONE')"
			+ "	OR"
			+ "	(:authCode IN (SELECT v2.agentCode "
			+ "		FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "	)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewOverviewAgent> findByUnderZoneOverview(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE VI: Get agent under COMPANY based on agentLevel (GM or AVP or AL)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE "
			+ " v.agentLevel = :agentLevel"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewOverviewAgent> findByUnderCompanyOverviewProducer(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE VIII.I: Get agent under COMPANY on agentLevel (AGENT) --- PC
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE'"
			+ " ORDER BY PC_SUBMIT DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderCompanyOverviewAgentPc(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE VIII.II: Get agent under COMPANY on agentLevel (AGENT) --- Active
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE'"
			+ " ORDER BY ACTIVE DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderCompanyOverviewAgentActive(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE VIII.III: Get agent under COMPANY on agentLevel (AGENT) --- RYP
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE'"
			+ " ORDER BY RYP DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderCompanyOverviewAgentRyp(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// Get agent under CHANNEL based on agentLevel (GM or AVP or AL)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE "
			+ " v.agentLevel = :agentLevel"
			+ " AND v.groupChannel = :channelCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewOverviewAgent> findByUnderChannelOverviewProducer(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel
			, @Param("channelCode") String channelCode);
	
	// Get agent under Channel GA or SFC on AGENT --- PC
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = :channelCode"
			+ " ORDER BY PC_SUBMIT DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelLevelAgentPc(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel
			, @Param("channelCode") String channelCode);
	
	// Get agent under Channel GA or SFC on AGENT --- ACTIVE
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = :channelCode"
			+ " ORDER BY ACTIVE DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelLevelAgentActive(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel
			, @Param("channelCode") String channelCode);
	
	// Get agent under Channel GA or SFC on AGENT --- RYP
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = :channelCode"
			+ " ORDER BY RYP DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelLevelAgentRyp(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel
			, @Param("channelCode") String channelCode);
	
	// OLD VERSION
	// CASE VII: Get agent under CHANNEL SFC based on agentLevel (GM or AVP or AL)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE "
			+ " v.agentLevel = :agentLevel"
			+ " AND v.groupChannel = 'SFC'"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewOverviewAgent> findByUnderChannelSfcOverviewProducer(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE VII: Get agent under CHANNEL GA based on agentLevel (GM or AVP or AL)
	@Query("SELECT v FROM ViewOverviewAgent v WHERE "
			+ " v.agentLevel = :agentLevel"
			+ " AND v.groupChannel = 'GA'"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewOverviewAgent> findByUnderChannelGaOverviewProducer(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE IX.I: Get agent under CHANNEL SFC on agentLevel (AGENT) --- PC
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = 'SFC'"
			+ " ORDER BY PC_SUBMIT DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelSfcOverviewAgentPc(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE IX.II: Get agent under CHANNEL SFC on agentLevel (AGENT) --- ACTIVE
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = 'SFC'"
			+ " ORDER BY ACTIVE DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelSfcOverviewAgentActive(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE IX.III: Get agent under CHANNEL SFC on agentLevel (AGENT) --- RYP
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = 'SFC'"
			+ " ORDER BY RYP DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelSfcOverviewAgentRyp(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE X.I: Get agent under CHANNEL GA on agentLevel (AGENT) --- PC
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = 'GA'"
			+ " ORDER BY PC_SUBMIT DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelGaOverviewAgentPc(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE X.II: Get agent under CHANNEL GA on agentLevel (AGENT) --- ACTIVE
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = 'GA'"
			+ " ORDER BY ACTIVE DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelGaOverviewAgentActive(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
	
	// CASE X.III: Get agent under CHANNEL GA on agentLevel (AGENT) --- RYP
	@Query(value=""
			+ "SELECT *"
			+ " FROM VIEW_OVERVIEW_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel AND :authLevel = 'ZONE' AND GROUP_CHANNEL = 'GA'"
			+ " ORDER BY RYP DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<ViewOverviewAgent> findByUnderChannelGaOverviewAgentRyp(
			@Param("agentLevel") String agentLevel
			, @Param("authLevel") String authLevel);
}