package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DD_T_USER_ROLES", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "DdTUserRolesList.findAll", query = "SELECT v FROM DdTUserRolesList v")
})
public class DdTUserRolesList implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "USERNAME", length = 256)
	private String username;
	@Column(name = "ROLE_ID")
	private Integer roleId;
	@Column(name = "CREATE_DATE")
	private Date createDate;
	@Column(name = "CREATE_BY", length = 255)
	private String createBy;
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	@Column(name = "UPDATE_BY", length = 255)
	private String updateBy;
	@Column(name = "IS_DELETE", length = 1)
	private char isDelete;
	
	
	public DdTUserRolesList() {
	}

	public DdTUserRolesList(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof DdTUserRolesList)) {
            return false;
        }
        DdTUserRolesList other = (DdTUserRolesList) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "DdTUserRolesList [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public char getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(char isDelete) {
		this.isDelete = isDelete;
	}

}
