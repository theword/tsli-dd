package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_RETENTION_MTD", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewRetentionMtd.findAll", query = "SELECT v FROM ViewRetentionMtd v")
})
public class ViewRetentionMtd implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 18)
    private String closYm;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "ZONE", length = 4)
    private String zone;
	@Column(name = "RETENTION")
	private Double retention;
	@Column(name = "RETENTION_MTD_1")
	private Double retentionMtd1;
	@Column(name = "RETENTION_MTD_2")
	private Double retentionMtd2;
	@Column(name = "RETENTION_MTD_3")
	private Double retentionMtd3;
	@Column(name = "RETENTION_MTD_4")
	private Double retentionMtd4;
	@Column(name = "RETENTION_MTD_5")
	private Double retentionMtd5;
	@Column(name = "RETENTION_MTD_6")
	private Double retentionMtd6;
	
	public ViewRetentionMtd() {
		
	}

	public ViewRetentionMtd(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public Double getRetention() {
		return retention;
	}

	public void setRetention(Double retention) {
		this.retention = retention;
	}

	public Double getRetentionMtd1() {
		return retentionMtd1;
	}

	public void setRetentionMtd1(Double retentionMtd1) {
		this.retentionMtd1 = retentionMtd1;
	}

	public Double getRetentionMtd2() {
		return retentionMtd2;
	}

	public void setRetentionMtd2(Double retentionMtd2) {
		this.retentionMtd2 = retentionMtd2;
	}

	public Double getRetentionMtd3() {
		return retentionMtd3;
	}

	public void setRetentionMtd3(Double retentionMtd3) {
		this.retentionMtd3 = retentionMtd3;
	}

	public Double getRetentionMtd4() {
		return retentionMtd4;
	}

	public void setRetentionMtd4(Double retentionMtd4) {
		this.retentionMtd4 = retentionMtd4;
	}

	public Double getRetentionMtd5() {
		return retentionMtd5;
	}

	public void setRetentionMtd5(Double retentionMtd5) {
		this.retentionMtd5 = retentionMtd5;
	}

	public Double getRetentionMtd6() {
		return retentionMtd6;
	}

	public void setRetentionMtd6(Double retentionMtd6) {
		this.retentionMtd6 = retentionMtd6;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewRetentionMtd)) {
            return false;
        }
        ViewRetentionMtd other = (ViewRetentionMtd) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewRetentionMtd [id=" + id + "]";
	}

}
