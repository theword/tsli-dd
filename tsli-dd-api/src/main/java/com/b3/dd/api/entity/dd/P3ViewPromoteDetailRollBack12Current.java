package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PROMOTE_ROLLBACK12_CURRENT", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewPromoteDetailRollBack12Current.findAll", query = "SELECT v FROM P3ViewPromoteDetailRollBack12Current v")
})
public class P3ViewPromoteDetailRollBack12Current implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private String id;
	
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "ZONE", length = 4)
    private String zone;
	
	@Column(name = "GM_CODE", length = 20)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 20)
    private String avpCode;
	@Column(name = "AL_CODE", length = 20)
    private String alCode;
	@Column(name = "AGENT_CODE", length = 30)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	
	@Column(name = "PERIOD_START", length = 6)
    private String periodStart;
	@Column(name = "PERIOD_END", length = 6)
    private String periodEnd;
	@Column(name = "PERIOD_TYPE", length = 50)
    private String periodType;
	@Column(name = "PROMOTE_TYPE", length = 5)
    private String promoteType;
	
	@Column(name = "POSITION_CODE", length = 25)
    private String positionCode;
	@Column(name = "NEXT_POSITION_CODE", length = 25)
    private String nextPositionCode;
	@Column(name = "NEXT_AGENT_LEVEL", length = 25)
    private String nextAgentLevel;
	
	@Column(name = "NO_AL")
    private Double noAl;
	@Column(name = "PC_DIRECT")
    private Double pcDirect;
	@Column(name = "ACTIVE_PRODUCER")
    private Double activeProducer;
	@Column(name = "NEW_CODE")
    private Double newCode;
	@Column(name = "PERSISTENCY", length = 40)
    private String persistency;
	@Column(name = "SMART_MANAGER_1")
    private String smartManager1;
	@Column(name = "SMART_MANAGER_2")
    private String smartManager2;
	@Column(name = "PC_WHOLE")
    private Double pcWhole;
	
	@Column(name = "SMART_AGENT_1")
    private String smartAgent1;
	@Column(name = "SMART_AGENT_2")
    private String smartAgent2;
	@Column(name = "ACTIVE_PRODUCER_WHOLE")
    private Double activeProducerWhole;
	
	public P3ViewPromoteDetailRollBack12Current() {	
	}

	public P3ViewPromoteDetailRollBack12Current(String id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewPromoteDetailRollBack12Current)) {
            return false;
        }
        P3ViewPromoteDetailRollBack12Current other = (P3ViewPromoteDetailRollBack12Current) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}

	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public Double getNoAl() {
		return noAl;
	}

	public void setNoAl(Double noAl) {
		this.noAl = noAl;
	}

	public Double getPcDirect() {
		return pcDirect;
	}

	public void setPcDirect(Double pcDirect) {
		this.pcDirect = pcDirect;
	}

	public Double getActiveProducer() {
		return activeProducer;
	}

	public void setActiveProducer(Double activeProducer) {
		this.activeProducer = activeProducer;
	}

	public Double getNewCode() {
		return newCode;
	}

	public void setNewCode(Double newCode) {
		this.newCode = newCode;
	}

	public String getPersistency() {
		return persistency;
	}

	public void setPersistency(String persistency) {
		this.persistency = persistency;
	}

	public String getSmartManager1() {
		return smartManager1;
	}

	public void setSmartManager1(String smartManager1) {
		this.smartManager1 = smartManager1;
	}

	public String getSmartManager2() {
		return smartManager2;
	}

	public void setSmartManager2(String smartManager2) {
		this.smartManager2 = smartManager2;
	}

	public Double getPcWhole() {
		return pcWhole;
	}

	public void setPcWhole(Double pcWhole) {
		this.pcWhole = pcWhole;
	}

	public String getPromoteType() {
		return promoteType;
	}

	public void setPromoteType(String promoteType) {
		this.promoteType = promoteType;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getNextPositionCode() {
		return nextPositionCode;
	}

	public void setNextPositionCode(String nextPositionCode) {
		this.nextPositionCode = nextPositionCode;
	}

	public String getNextAgentLevel() {
		return nextAgentLevel;
	}

	public void setNextAgentLevel(String nextAgentLevel) {
		this.nextAgentLevel = nextAgentLevel;
	}

	public String getSmartAgent1() {
		return smartAgent1;
	}

	public void setSmartAgent1(String smartAgent1) {
		this.smartAgent1 = smartAgent1;
	}

	public String getSmartAgent2() {
		return smartAgent2;
	}

	public void setSmartAgent2(String smartAgent2) {
		this.smartAgent2 = smartAgent2;
	}

	public Double getActiveProducerWhole() {
		return activeProducerWhole;
	}

	public void setActiveProducerWhole(Double activeProducerWhole) {
		this.activeProducerWhole = activeProducerWhole;
	}

}
