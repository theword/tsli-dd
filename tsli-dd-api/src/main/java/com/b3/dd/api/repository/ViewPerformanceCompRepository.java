package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewPerformanceComp;

@Repository
public interface ViewPerformanceCompRepository extends CrudRepository<ViewPerformanceComp, Integer> {
	
	// CONDITION I. PERFORMANCE: COMPANY
	@Query("SELECT v FROM ViewPerformanceComp v WHERE v.agentLevel = 'COMPANY'"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewPerformanceComp> findByCompany(@Param("authLevel") String authLevel);
	
	// CONDITION II. PERFORMANCE: CHANNEL
	@Query("SELECT v FROM ViewPerformanceComp v WHERE v.agentLevel = 'CHANNEL' AND v.groupChannel = :channelCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewPerformanceComp> findByChannel(
			@Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel);
		
	// CONDITION III. PERFORMANCE: ZONE
	@Query("SELECT v FROM ViewPerformanceComp v WHERE v.agentLevel = 'ZONE' AND v.zone = :zoneCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewPerformanceComp> findByZone(
			@Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
}
