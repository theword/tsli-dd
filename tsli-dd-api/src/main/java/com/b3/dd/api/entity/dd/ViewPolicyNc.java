package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_POLICY_NC", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewPolicyNc.findAll", query = "SELECT v FROM ViewPolicyNc v")
})
public class ViewPolicyNc implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "AGENT_CODE", length = 25)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 10)
    private String agentLevel;
	@Column(name = "AGENT_TYPE", length = 25)
    private String agentType;
	
	@Column(name = "AL_CODE", length = 30)
    private String alCode;
	@Column(name = "AL_NAME", length = 300)
    private String alName;
	
	@Column(name = "AVP_CODE", length = 30)
    private String avpCode;
	@Column(name = "AVP_NAME", length = 300)
    private String avpName;
	
	@Column(name = "CHANNEL", length = 5)
    private String channel;
	@Column(name = "CLOS_YM", length = 6)
    private String closYm;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	
	@Column(name = "GM_CODE", length = 30)
    private String gmCode;
	@Column(name = "GM_NAME", length = 300)
    private String gmName;
	
	@Column(name = "MEMO", length = 50)
    private String memo;
	@Column(name = "MEMO_OIC", length = 255)
    private String memoOic;
	@Column(name = "NAME", length = 100)
    private String name;
	@Column(name = "RECRUITER", length = 25)
    private String recruiter;
	@Column(name = "RECRUITER_NAME", length = 300)
    private String recruiterName;
	@Column(name = "ZONE", length = 4)
    private String zone;
	
	public ViewPolicyNc() {
		
	}

	public ViewPolicyNc(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAvpName() {
		return avpName;
	}

	public void setAvpName(String avpName) {
		this.avpName = avpName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemoOic() {
		return memoOic;
	}

	public void setMemoOic(String memoOic) {
		this.memoOic = memoOic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(String recruiter) {
		this.recruiter = recruiter;
	}

	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewPolicyNc)) {
            return false;
        }
        ViewPolicyNc other = (ViewPolicyNc) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewPolicyNc [id=" + id + "]";
	}

}
