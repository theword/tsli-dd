package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewPolicyNc;
import com.b3.dd.api.entity.dd.ViewPolicyNcMemo;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewPolicyNcRepository;

@RestController
@RequestMapping("/${api.prefix}/policy/nc")
public class ViewPolicyNcController {
	@Autowired
	private ViewPolicyNcRepository ViewPolicyNcRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.
	@PostMapping("/company")
	public ResponseEntity<List<ViewPolicyNc>> 
	getPolicyNcCompany(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyNc> ViewPolicyNc = ViewPolicyNcRepository.findByCompany(
				request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyNc);
	}
	
	// CASE II.
	@PostMapping("/channel")
	public ResponseEntity<List<ViewPolicyNc>> 
	getPolicyNcChannel(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyNc> ViewPolicyNc = ViewPolicyNcRepository.findByChannel(
				request.getFilter(), request.getChannelCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyNc);
	}
	
	// CASE III.
	@PostMapping("/zone")
	public ResponseEntity<List<ViewPolicyNc>> 
	getPolicyNcZone(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyNc> ViewPolicyNc = ViewPolicyNcRepository.findByZone(
				request.getFilter(), request.getZoneCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyNc);
	}
	
	// CASE I.I
	@PostMapping("/al")
	public ResponseEntity<List<ViewPolicyNc>> 
	getPolicyPcUnderAl(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyNc> ViewPolicyNc = ViewPolicyNcRepository.findByAlCode(
				request.getAlCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyNc);
	}
	
	// CASE I.II
	@PostMapping("/avp")
	public ResponseEntity<List<ViewPolicyNc>> 
	getPolicyPcUnderAvp(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyNc> ViewPolicyNc = ViewPolicyNcRepository.findByAvpCode(
				request.getAvpCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyNc);
	}
		
	// CASE I.III
	@PostMapping("/gm")
	public ResponseEntity<List<ViewPolicyNc>> 
	getPolicyPcUnderGm(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyNc> ViewPolicyNc = ViewPolicyNcRepository.findByGmCode(
				request.getGmCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyNc);
	}
	

	@PostMapping("/memo")
	public @ResponseBody ResponseEntity<List<ViewPolicyNcMemo>> getMemo(@RequestBody RequestModel request) throws ResourceClosedException {
		List<ViewPolicyNcMemo> ViewPolicyNcMemo = ViewPolicyNcRepository.findMomo(request.getAgentCode(), request.getMemoType());
		return ResponseEntity.ok().body(ViewPolicyNcMemo);
		
	}
}
