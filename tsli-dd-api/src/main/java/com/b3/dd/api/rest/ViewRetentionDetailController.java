package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewRetentionDetail;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewRetentionDetailRepository;

@RestController
@RequestMapping("/${api.prefix}/policy/retention")
public class ViewRetentionDetailController {
	@Autowired
	private ViewRetentionDetailRepository ViewRetentionDetailRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.
	@PostMapping("/company")
	public ResponseEntity<List<ViewRetentionDetail>> 
	getPolicyNcCompany(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewRetentionDetail> ViewRetentionDetail = ViewRetentionDetailRepository.findByCompany(
				request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewRetentionDetail);
	}
	
	// CASE II.
	@PostMapping("/channel")
	public ResponseEntity<List<ViewRetentionDetail>> 
	getPolicyNcChannel(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewRetentionDetail> ViewRetentionDetail = ViewRetentionDetailRepository.findByChannel(
				request.getFilter(), request.getChannelCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewRetentionDetail);
	}
	
	// CASE III.
	@PostMapping("/zone")
	public ResponseEntity<List<ViewRetentionDetail>> 
	getPolicyNcZone(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewRetentionDetail> ViewRetentionDetail = ViewRetentionDetailRepository.findByZone(
				request.getFilter(), request.getZoneCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewRetentionDetail);
		}
}
