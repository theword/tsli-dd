package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "BUTTON_NAME", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewButtonName.findAll", query = "SELECT v FROM ViewButtonName v")
})
public class ViewButtonName implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "BUTTON_NAME", length = 41)
    private String buttonName;
	@Column(name = "LEVEL_NAME", length = 20)
    private String levelName;
	
	public ViewButtonName() {
		
	}

	public ViewButtonName(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewButtonName)) {
            return false;
        }
        ViewButtonName other = (ViewButtonName) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewButtonName [id=" + id + "]";
	}
}
