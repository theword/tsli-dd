package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewYtdAgentRyp;

@Repository
public interface ViewYtdAgentRypRepository extends CrudRepository<ViewYtdAgentRyp, Integer> {

	// Missing check authCode
	@Query("SELECT v FROM ViewYtdAgentRyp v WHERE v.agentCode = :agentCode"
			+ " AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewYtdAgentRyp> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
