package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewTargetComp;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewTargetCompRepository;

@RestController
@RequestMapping("/${api.prefix}/company")
public class ViewTargetCompController {
	
	@Autowired
	private ViewTargetCompRepository ViewTargetCompRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("target")
	public ResponseEntity<List<ViewTargetComp>>
	getAgentTarget(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewTargetComp> ViewTargetComp = ViewTargetCompRepository.findByCode(
				request.getAgentCode()
				, userHelper.getUserPosition());

		return ResponseEntity.ok().body(ViewTargetComp);
	}
}
