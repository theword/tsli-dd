package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewOverviewSon;

@Repository
public interface P3ViewOverviewSonRepository extends CrudRepository<P3ViewOverviewSon, Integer> {

	// CASE I.I find by anyCode with PC
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY PC_SUBMIT DESC"
			+ " FETCH FIRST 100 ROWS ONLY"
	, nativeQuery = true)
	public List<P3ViewOverviewSon> findByLevelWithPc(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II find by anyCode with Active
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY ACTIVE DESC"
			+ " FETCH FIRST 100 ROWS ONLY"
	, nativeQuery = true)
	public List<P3ViewOverviewSon> findByLevelWithActive(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III find by anyCode with NewCode
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY NC DESC"
			+ " FETCH FIRST 100 ROWS ONLY"
	, nativeQuery = true)
	public List<P3ViewOverviewSon> findByLevelWithNewCode(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV find by anyCode with RYP
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY RYP DESC"
			+ " FETCH FIRST 100 ROWS ONLY"
	, nativeQuery = true)
	public List<P3ViewOverviewSon> findByLevelWithRyp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
