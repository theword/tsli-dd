package com.b3.dd.api.model;

public class MessageHandleModel {

	private String message;

	public MessageHandleModel() {
		super();
	}

	public MessageHandleModel(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
