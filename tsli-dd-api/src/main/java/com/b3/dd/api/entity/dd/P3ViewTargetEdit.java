package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name = "VIEW_TARGET_EDIT", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewTargetEdit.findAll", query = "SELECT v FROM P3ViewTargetEdit v")
})
public class P3ViewTargetEdit implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "KPI_LEVEL", length = 18)
    private String kpiLevel;
	@Column(name = "CODE", length = 30)
    private String code;
	@Column(name = "KPI_NAME", length = 50)
    private String kpiName;
	@Column(name = "PRODUCTION_YEAR", length = 4)
    private String productionYear;
	
	@Column(name = "TARGET_YEAR")
    private Double targetYear;
	
	@Column(name = "TARGET_M1")
    private Double targetM1;
	@Column(name = "TARGET_M2")
    private Double targetM2;
	@Column(name = "TARGET_M3")
    private Double targetM3;
	@Column(name = "TARGET_M4")
    private Double targetM4;
	@Column(name = "TARGET_M5")
    private Double targetM5;
	@Column(name = "TARGET_M6")
    private Double targetM6;
	@Column(name = "TARGET_M7")
    private Double targetM7;
	@Column(name = "TARGET_M8")
    private Double targetM8;
	@Column(name = "TARGET_M9")
    private Double targetM9;
	@Column(name = "TARGET_M10")
    private Double targetM10;
	@Column(name = "TARGET_M11")
    private Double targetM11;
	@Column(name = "TARGET_M12")
    private Double targetM12;
	
	@Column(name = "AGENT_NAME", length = 300)
    private String agentName;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPEATE_DATE")
    private Date updateDate;
	
	
	public P3ViewTargetEdit() {	
	}

	public P3ViewTargetEdit(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewTargetEdit)) {
            return false;
        }
        P3ViewTargetEdit other = (P3ViewTargetEdit) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKpiLevel() {
		return kpiLevel;
	}

	public void setKpiLevel(String kpiLevel) {
		this.kpiLevel = kpiLevel;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	public String getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(String productionYear) {
		this.productionYear = productionYear;
	}

	public Double getTargetYear() {
		return targetYear;
	}

	public void setTargetYear(Double targetYear) {
		this.targetYear = targetYear;
	}

	public Double getTargetM1() {
		return targetM1;
	}

	public void setTargetM1(Double targetM1) {
		this.targetM1 = targetM1;
	}

	public Double getTargetM2() {
		return targetM2;
	}

	public void setTargetM2(Double targetM2) {
		this.targetM2 = targetM2;
	}

	public Double getTargetM3() {
		return targetM3;
	}

	public void setTargetM3(Double targetM3) {
		this.targetM3 = targetM3;
	}

	public Double getTargetM4() {
		return targetM4;
	}

	public void setTargetM4(Double targetM4) {
		this.targetM4 = targetM4;
	}

	public Double getTargetM5() {
		return targetM5;
	}

	public void setTargetM5(Double targetM5) {
		this.targetM5 = targetM5;
	}

	public Double getTargetM6() {
		return targetM6;
	}

	public void setTargetM6(Double targetM6) {
		this.targetM6 = targetM6;
	}

	public Double getTargetM7() {
		return targetM7;
	}

	public void setTargetM7(Double targetM7) {
		this.targetM7 = targetM7;
	}

	public Double getTargetM8() {
		return targetM8;
	}

	public void setTargetM8(Double targetM8) {
		this.targetM8 = targetM8;
	}

	public Double getTargetM9() {
		return targetM9;
	}

	public void setTargetM9(Double targetM9) {
		this.targetM9 = targetM9;
	}

	public Double getTargetM10() {
		return targetM10;
	}

	public void setTargetM10(Double targetM10) {
		this.targetM10 = targetM10;
	}

	public Double getTargetM11() {
		return targetM11;
	}

	public void setTargetM11(Double targetM11) {
		this.targetM11 = targetM11;
	}

	public Double getTargetM12() {
		return targetM12;
	}

	public void setTargetM12(Double targetM12) {
		this.targetM12 = targetM12;
	}
	
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
