package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.entity.dd.UserDevice;

public interface UserDeviceRepository extends CrudRepository<UserDevice, Integer> {

	public List<UserDevice> findByUsername(@Param("username") String username);

	public UserDevice findByDeviceIdAndUsername(@Param("deviceId") String deviceId, @Param("username") String username);
}
