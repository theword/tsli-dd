package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.ViewAllYtdAgent;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewAllYtdAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class ViewAllYtdAgentController {

	@Autowired
	private ViewAllYtdAgentRepository ViewAllYtdAgentRepository;
	
	@PostMapping("ytd/currentyear") // Current year
	public ResponseEntity<List<ViewAllYtdAgent>>
	getAgentYtdCurrentYear(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewAllYtdAgent> ViewAllYtdAgent = ViewAllYtdAgentRepository.findByAgentCurrentYear(
			request.getAgentCode(), request.getAgentLevel(), principal.getName()
		);
		
		return ResponseEntity.ok().body(ViewAllYtdAgent);
	}
	
	@PostMapping("ytd/currentandlastyear") // Current and Last year
	public ResponseEntity<List<ViewAllYtdAgent>>
	getAgentYtdCurrentAndLastYear(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewAllYtdAgent> ViewAllYtdAgent = ViewAllYtdAgentRepository.findByAgentCurrentAndLastYear(
			request.getAgentCode(), request.getAgentLevel(), principal.getName());
		
		return ResponseEntity.ok().body(ViewAllYtdAgent);
	}
}
