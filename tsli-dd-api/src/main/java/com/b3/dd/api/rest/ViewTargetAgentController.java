package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewTargetAgent;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewTargetAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class ViewTargetAgentController {

	@Autowired
	private ViewTargetAgentRepository ViewTargetAgentRepository;
	@Autowired
	private UserHelper userHelper;
	
	
	@PostMapping("target")
	public ResponseEntity<List<ViewTargetAgent>>
	getAgentTarget(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		
		List<ViewTargetAgent> viewTargetAgent = ViewTargetAgentRepository.findByAgentCode(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(viewTargetAgent);
	}
}