package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3DdTPromotionConfig;
import com.b3.dd.api.repository.DdTPromotionConfigRepository;

@RestController
@RequestMapping("/${api.prefix}/super-admin")
public class DdTPromotionConfigController {
	@Autowired
	private DdTPromotionConfigRepository DdTPromotionConfigRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("promotion-config-list")
	public ResponseEntity<List<P3DdTPromotionConfig>>
	getPromotionConfigList(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		
		List<P3DdTPromotionConfig> DdTPromotionConfig = null;
		DdTPromotionConfig = DdTPromotionConfigRepository.findPromotionConfig(
			userHelper.getUserPosition()
		);
		
		return ResponseEntity.ok().body(DdTPromotionConfig);
	}
	
	@PostMapping("promotion-config-delete")
	public ResponseEntity<Map<String, Object>>
	deletePromotionConfig(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		// Prepare variables
		Map<String, Object> response = new HashMap<String, Object>();
		
		String updateBy = userHelper.getUserLoggedInName() + " (" +userHelper.getName()+ ")";
		java.sql.Date updateDate = new java.sql.Date(new java.util.Date().getTime());
		
		try {
			DdTPromotionConfigRepository.deleteConfigById(
				request.getPromotionConfigId()
				, updateDate
				, updateBy
				, userHelper.getUserPosition()
			);
			DdTPromotionConfigRepository.deleteDetailById(
				request.getPromotionConfigId()
				, updateDate
				, updateBy
				, userHelper.getUserPosition()
			);
			response.put("msg", "success");
		} catch (Exception e) {
			response.put("msg", "failed");
		}
		
		return ResponseEntity.ok().body(response);
	}
}
