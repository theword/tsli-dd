package com.b3.dd.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.b3.dd.api.entity.dd.DdTPromoteValidateMapping;

@Service
public class PromoteValidateMappingService {

	@PersistenceContext
	private EntityManager em;

	public Map<String, String> getMapping() {
		Map<String, String> map = new HashMap<String, String>();
		List<DdTPromoteValidateMapping> rs = em
				.createQuery("SELECT d FROM DdTPromoteValidateMapping d", DdTPromoteValidateMapping.class)
				.getResultList();
		if (!CollectionUtils.isEmpty(rs)) {
			for (DdTPromoteValidateMapping r : rs) {
				map.put(r.getValidateType(), r.getDesc());
			}
		}
		return map;
	}

}
