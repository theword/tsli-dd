package com.b3.dd.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.b3.dd.api.entity.dd.OauthClientDetails;


public interface OauthClientDetailsRepository extends CrudRepository<OauthClientDetails, String> {

}
