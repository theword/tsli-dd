package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.DdTUserRolesList;

@Repository
public interface UserRolesListRepository extends CrudRepository<DdTUserRolesList, Integer> {
	
	@Query(value=""
			+ "SELECT * "
			+ " FROM DD_T_USER_ROLES"
			+ " WHERE USERNAME = :username", nativeQuery = true)
	public List<DdTUserRolesList> findByUsername(
			@Param("username") String username);

}
