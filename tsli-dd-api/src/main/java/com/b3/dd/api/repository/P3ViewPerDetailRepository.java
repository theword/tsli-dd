package com.b3.dd.api.repository;

import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewPerDetail;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

@Repository
public interface P3ViewPerDetailRepository extends PagingAndSortingRepository<P3ViewPerDetail, Integer>{
	
	// CASE I.I - Find by AGent code
	@Query("SELECT v FROM P3ViewPerDetail v "
			+ "	WHERE v.persistencyType = :filter"
			+ "	AND v.collectPc = 0 AND v.plusPc = 0"
			+ "	AND (v.policyStatus = 'Lapsed' OR v.policyStatus = 'Pending')"
			+ "	AND v.agentCode = :agentCode"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			OR v.alCode = :authCode"
			+ "			OR v.agentCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
//	+ " ORDER BY SUBSTR(v.yearTerm, 1, 1) ASC, CAST(SUBSTR(v.yearTerm, 3, 2) AS int) ASC, v.targetPc DESC"
	public Page<P3ViewPerDetail> findByAgentCode(
			@Param("filter") String filter
			, @Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE I.II - Find by AL code
	@Query("SELECT v FROM P3ViewPerDetail v "
			+ "	WHERE v.persistencyType = :filter"
			+ "	AND v.collectPc = 0 AND v.plusPc = 0"
			+ "	AND (v.policyStatus = 'Lapsed' OR v.policyStatus = 'Pending')"
			+ "	AND v.alCode = :agentCode"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			OR v.alCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public Page<P3ViewPerDetail> findByAlCode(
			@Param("filter") String filter
			, @Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE I.III - Find by AVP code
	@Query("SELECT v FROM P3ViewPerDetail v "
			+ "	WHERE v.persistencyType = :filter"
			+ "	AND v.collectPc = 0 AND v.plusPc = 0"
			+ "	AND (v.policyStatus = 'Lapsed' OR v.policyStatus = 'Pending')"
			+ "	AND v.avpCode = :agentCode"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public Page<P3ViewPerDetail> findByAvpCode(
			@Param("filter") String filter
			, @Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
		
	// CASE I.IV - Find by GM code
	@Query("SELECT v FROM P3ViewPerDetail v "
			+ "	WHERE v.persistencyType = :filter"
			+ "	AND v.collectPc = 0 AND v.plusPc = 0"
			+ "	AND (v.policyStatus = 'Lapsed' OR v.policyStatus = 'Pending')"
			+ "	AND v.gmCode = :agentCode"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public Page<P3ViewPerDetail> findByGmCode(
			@Param("filter") String filter
			, @Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
}
