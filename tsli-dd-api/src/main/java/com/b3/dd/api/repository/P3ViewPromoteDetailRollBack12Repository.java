package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewPromoteDetailRollBack12;

@Repository
public interface P3ViewPromoteDetailRollBack12Repository extends CrudRepository<P3ViewPromoteDetailRollBack12, Integer> {
	// CASE -I - find promote detail
	@Query(value=""
			+ "SELECT v FROM P3ViewPromoteDetailRollBack12 v"
			+ " WHERE v.agentCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND 1 = CASE"
			+ "		WHEN :authLevel = 'ZONE' THEN 1"
			+ "		WHEN (:authCode IN (SELECT v2.agentCode"
			+ "	 		FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		) THEN 1"
			+ "		WHEN ("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		) THEN 1"
			+ "		WHEN (v.agentLevel = 'AGENT' OR v.agentLevel = 'AG')"
			+ "			AND ("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			OR v.alCode = :authCode"
			+ "			OR v.agentCode = :authCode"
			+ "		) THEN 1"
			+ "		WHEN v.agentLevel = 'AL'"
			+ "			AND ("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			OR v.alCode = :authCode"
			+ "		) THEN 1"
			+ "		WHEN v.agentLevel = 'AVP'"
			+ "			AND ("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "		) THEN 1"
			+ "		WHEN v.agentLevel = 'GM'"
			+ "			AND ("
			+ "			v.gmCode = :authCode"
			+ "		) THEN 1"
			+ "		ELSE 0"
			+ "	END"
			)
	public List<P3ViewPromoteDetailRollBack12> findByLevel(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.I - find validation detail by AGENT
	@Query(value=""
			+ "SELECT v FROM P3ViewPromoteDetailRollBack12 v"
			+ " WHERE v.agentCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			OR v.alCode = :authCode"
			+ "			OR v.agentCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public List<P3ViewPromoteDetailRollBack12> findByAgent(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II - find validation detail by AL
	@Query(value=""
			+ "SELECT v FROM P3ViewPromoteDetailRollBack12 v"
			+ " WHERE v.agentCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			OR v.alCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public List<P3ViewPromoteDetailRollBack12> findByAl(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III - find validation detail by AVP
	@Query(value=""
			+ "SELECT v FROM P3ViewPromoteDetailRollBack12 v"
			+ " WHERE v.agentCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			OR v.avpCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public List<P3ViewPromoteDetailRollBack12> findByAvp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV - find validation detail by GM
	@Query(value=""
			+ "SELECT v FROM P3ViewPromoteDetailRollBack12 v"
			+ " WHERE v.agentCode = :agentCode"
			+ "	AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "			("
			+ "			v.gmCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			)
	public List<P3ViewPromoteDetailRollBack12> findByGm(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
