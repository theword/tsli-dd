package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.DdTPromoteValidateMapping;

@Repository
public interface DdTPromoteValidateMappingRepository extends CrudRepository<DdTPromoteValidateMapping, Integer> {

	@Query(value=""
			+ "SELECT * FROM DD_T_PROMOTE_VALIDATE_MAPPING"
			, nativeQuery = true)
	public List<DdTPromoteValidateMapping> findAll();
}
