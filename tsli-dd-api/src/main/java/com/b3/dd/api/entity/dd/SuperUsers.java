/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "SUPER_USERS", catalog = "", schema = "", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "USERNAME" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "SuperUsers.findAll", query = "SELECT u FROM SuperUsers u"),
		@NamedQuery(name = "SuperUsers.findByUsername", query = "SELECT u FROM SuperUsers u WHERE u.username = :username") })
public class SuperUsers implements Serializable {
	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
	@Id
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, precision = 38, scale = 0)
	private Integer id;
	@Basic(optional = false)
	@Column(name = "USERNAME", nullable = false, length = 256)
	private String username;
	@Column(name = "CREATE_BY", length = 255)
	private String createBy;
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Column(name = "UPDATE_BY", length = 255)
	private String updateBy;
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	@Column(name = "IS_DELETE")
	private Character isDelete;

	public SuperUsers() {
	}

	public SuperUsers(Integer id) {
		this.id = id;
	}

	public SuperUsers(Integer id, String username) {
		this.id = id;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof SuperUsers)) {
			return false;
		}
		SuperUsers other = (SuperUsers) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.tsli.dd.entity.Users[ id=" + id + " ]";
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

}
