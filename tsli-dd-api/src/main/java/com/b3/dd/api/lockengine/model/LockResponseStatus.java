package com.b3.dd.api.lockengine.model;

import com.deverhood.lock.manager.resource.ResponseStatus;

public class LockResponseStatus extends ResponseStatus{

	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
