package com.b3.dd.api.model;

public class ClosYmModel {
	private String closYm;
	
	public ClosYmModel() {
		super();
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}
}
