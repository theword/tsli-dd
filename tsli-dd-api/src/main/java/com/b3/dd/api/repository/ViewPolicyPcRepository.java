package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewPolicyPc;

@Repository
public interface ViewPolicyPcRepository extends PagingAndSortingRepository<ViewPolicyPc, Integer> {

	// CASE I.
	@Query("SELECT v FROM ViewPolicyPc v WHERE v.agentCode = :agentCode"
			+ "	AND agentLevel = :agentLevel"
			+ "	AND status = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyPc> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.I
	@Query("SELECT v FROM ViewPolicyPc v WHERE v.alCode = :alCode"
			+ "	AND status = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :alCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyPc> findByAlCode(
			@Param("alCode") String alCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II
	@Query("SELECT v FROM ViewPolicyPc v WHERE v.avpCode = :avpCode"
			+ "	AND status = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :avpCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyPc> findByAvpCode(
			@Param("avpCode") String avpCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III
	@Query("SELECT v FROM ViewPolicyPc v WHERE v.gmCode = :gmCode"
			+ "	AND status = :filter"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :gmCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewPolicyPc> findByGmCode(
			@Param("gmCode") String gmCode
			, @Param("filter") String filter
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE II -  companyPagination
	@Query(value=""
			+ "SELECT * "
			+ "	FROM VIEW_POLICY_PC"
			+ "	WHERE STATUS = :filter AND (:authLevel = 'ZONE')"
			+ " ORDER BY PC DESC", nativeQuery = true)
	public Page<ViewPolicyPc> findByCompanyPagination(
			@Param("filter") String filter
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE III - channelPagination
	@Query(value=""
			+ "SELECT * "
			+ "	FROM VIEW_POLICY_PC"
			+ "	WHERE STATUS = :filter AND CHANNEL = :channelCode AND (:authLevel = 'ZONE')"
			+ " ORDER BY PC DESC", nativeQuery = true)
	public Page<ViewPolicyPc> findByChannelPagination(
			@Param("channelCode") String channelCode
			, @Param("filter") String filter
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE VIII - search
	@Query(value=""
			+ "SELECT * FROM VIEW_POLICY_PC"
			+ "	WHERE"
			+ "	STATUS = :filter"
			+ "	AND"
			+ "	("
			+ " 	(GM_CODE LIKE :searchKeyword OR"
			+ "		GM_NAME LIKE :searchKeyword OR"
			+ "		AVP_CODE LIKE :searchKeyword OR"
			+ "		AVP_NAME LIKE :searchKeyword OR"
			+ "		AL_CODE LIKE :searchKeyword OR"
			+ "		AL_NAME LIKE :searchKeyword OR"
			+ "		AGENT_CODE LIKE :searchKeyword OR"
			+ "		AGENT_NAME LIKE :searchKeyword OR"
			+ "		POLICY_CODE LIKE :searchKeyword OR"
			+ "		CUSTOMER_NAME LIKE :searchKeyword OR"
			+ "		PROD_NAME LIKE :searchKeyword OR"
			+ "		PC LIKE :searchKeyword OR"
			+ "		APPROVE_DATE LIKE :searchKeyword OR"
			+ "		SUBMIT_DATE LIKE :searchKeyword)"
			+ "	AND CHANNEL LIKE :channelCode)"
			+ "	AND (:authLevel = 'ZONE')", nativeQuery = true)
	public Page<ViewPolicyPc> findBySearchPagination(
			@Param("filter") String filter
			, @Param("searchKeyword") String searchKeyword
			, @Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE IV.
	@Query("SELECT v FROM ViewPolicyPc v WHERE v.zone = :zoneCode"
			+ "	AND status = :filter"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewPolicyPc> findByZone(
			@Param("zoneCode") String zoneCode
			, @Param("filter") String filter
			, @Param("authLevel") String authLevel);
}
