package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewTargetComp;

@Repository
public interface ViewTargetCompRepository extends CrudRepository<ViewTargetComp, Integer> {

	@Query("SELECT v FROM ViewTargetComp v WHERE v.code = :agentCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewTargetComp> findByCode(
			@Param("agentCode") String agentCode
			, @Param("authLevel") String authLevel);
}
