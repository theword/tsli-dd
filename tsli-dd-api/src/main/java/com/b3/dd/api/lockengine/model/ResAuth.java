package com.b3.dd.api.lockengine.model;

/*
 * Copyright (C) 2018 Deverhood Co.,Ltd.
 *
 * This code is a property of Deverhood Co.,Ltd.
 * Unless stated otherwise, you may not modify or redistribute without any written permission of the owner.
 *
 * For more information, visit <http://www.deverhood.com/>.
 *
 * Contributor(s):
 * 2018-07-05   Krerk Piromsopa, Ph.D. <Krerk.P@Chula.ac.th>
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Krerk Piromsopa, Ph.D. <Krerk.P@Chula.ac.th>
 */
public class ResAuth extends com.deverhood.lock.manager.resource.ResAuth{


	@JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Bangkok")
	private Date accessdate;

	/**
	 * @return the accessdate
	 */
	public Date getAccessdate() {
		return accessdate;
	}

	/**
	 * @param accessdate the accessdate to set
	 */
	public void setAccessdate(Date accessdate) {
		this.accessdate = accessdate;
	}

}
