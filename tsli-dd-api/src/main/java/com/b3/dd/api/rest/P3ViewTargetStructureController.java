package com.b3.dd.api.rest;

import java.security.Principal;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewPerDetail;
import com.b3.dd.api.entity.dd.P3ViewTargetStructure;
import com.b3.dd.api.model.PaggingModel;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewTargetStructureRepository;

@RestController
@RequestMapping("/${api.prefix}/edit")
public class P3ViewTargetStructureController {
	private static final int PAGE_SIZE = 100;
	@Autowired
	private P3ViewTargetStructureRepository P3ViewTargetStructureRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - Find by AGent code
	@PostMapping("target-list-agent")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getAgAgentTargetList(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByAgentCode(
			request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE I.II - Find by AL code
	@PostMapping("target-list-al")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getAlAgentTargetList(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByAlCode(
			request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE I.III - Find by AVP code
	@PostMapping("target-list-avp")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getAvpAgentTargetList(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByAvpCode(
			request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE I.IV - Find by GM code
	@PostMapping("target-list-gm")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getGmAgentTargetList(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByGmCode(
			request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE I.V - Find by Zone code
	@PostMapping("target-list-zone")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getZoneAgentTargetList(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByZoneCode(
			userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE II.I - Find by GM search
	@PostMapping("target-list-search-gm")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getGmSearch(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByGmSearch(
			request.getAgentCode(), request.getSearchKeyword(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE II.II - Find by AVP search
	@PostMapping("target-list-search-avp")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getAvpSearch(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByAvpSearch(
			request.getAgentCode(), request.getSearchKeyword(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE II.III - Find by AL search
	@PostMapping("target-list-search-al")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getAlSearch(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByAlSearch(
			request.getAgentCode(), request.getSearchKeyword(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE II.IV - Find by AG search
	@PostMapping("target-list-search-ag")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getAgSearch(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByAgSearch(
			request.getAgentCode(), request.getSearchKeyword(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}
	
	// CASE II.V - Find by Zone search
	@PostMapping("target-list-search-zone")
	public ResponseEntity<PaggingModel<P3ViewTargetStructure>> 
	getZoneSearch(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		
		Page<P3ViewTargetStructure> P3ViewTargetStructure = P3ViewTargetStructureRepository.findByZoneSearch(
			request.getSearchKeyword(), userHelper.getUserPosition(), pagging
		);
		return ResponseEntity.ok().body( new PaggingModel<P3ViewTargetStructure>(
				P3ViewTargetStructure.getContent(), P3ViewTargetStructure.getTotalPages(), P3ViewTargetStructure.getTotalElements()
			)
		);
	}

}
