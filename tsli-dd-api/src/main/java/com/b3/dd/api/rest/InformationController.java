package com.b3.dd.api.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.model.ScheduleJobModel;
import com.b3.dd.api.service.ScheduleService;
import com.b3.dd.api.service.UploadPromotionExcelService;
import com.b3.dd.api.model.ClosYmModel;
import com.b3.dd.api.model.FileUploadModel;
import com.b3.dd.api.service.ClosYmService;
import com.b3.dd.api.service.ParameterService;

@RestController
@RequestMapping("/${api.prefix}/info")
public class InformationController {

	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private ClosYmService closYm;
	@Autowired
	private ApplicationContext ctx;
	@Autowired
	private ParameterService parameterService;
	
	@GetMapping()
	public ResponseEntity<ScheduleJobModel> getInfo() {
		return ResponseEntity.ok(scheduleService.getSheduleJob());
	}
	
	@GetMapping("/getInfoClosYm")
	public ResponseEntity<ClosYmModel> getInfoClosYm() {
		return ResponseEntity.ok(closYm.getClosYm());
	}
	
	@GetMapping("/get-parameter")
	public ResponseEntity<String> getParameter( @RequestParam("paramName") String paramName) {
		return ResponseEntity.ok(parameterService.getParameter(paramName));
	}
	
	
	@GetMapping("/testReadExcel")
	public ResponseEntity testReadExcel() throws IOException {
		
		try {
			/**
			 * Mock up data
			 */
			File file = new File(TEMPLATE);
			byte[] data = Files.readAllBytes(file.toPath());
			String base64str = Base64.getEncoder().encodeToString(data);
			
			FileUploadModel f = new FileUploadModel();
			f.setData(base64str);
			f.setExtension("xlsx");
			f.setFilename(TEMPLATE);
			f.setSize(Files.size(file.toPath()));
			/**
			 * Mock up data
			 */
			
			 ctx.getBean(UploadPromotionExcelService.class, f, "visanu").importFile();
			 
		}catch (Exception e) {
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getLocalizedMessage());
		}
		return ResponseEntity.ok().build();
	} 
	
	private String TEMPLATE = "C:\\Users\\user\\Desktop\\New folder (2)\\IL2021 - 007 - Potential การแข่งขันคุณวุฒิ Recruitment Challenge อียิปต์.XLSX";

}
