package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_POLICY_NC_MEMO", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "ViewPolicyNcMemo.findAll", query = "SELECT v FROM ViewPolicyNcMemo v") })
public class ViewPolicyNcMemo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "ID", nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(name = "CLOS_YM", length = 10)
	private String closYm;
	@Basic(optional = false)
	@Column(name = "AGENT_CODE", length = 30, nullable = false)
	private String agentCode;
	@Column(name = "MEMO_DESC", length = 4000)
	private String memoDesc;
	@Column(name = "MEMO_TYPE", length = 25)
	private String memoType;
	@Column(name = "AGENT_TYPE", length = 25)
	private String agentType;
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	public ViewPolicyNcMemo() {

	}

	public ViewPolicyNcMemo(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getMemoDesc() {
		return memoDesc;
	}

	public void setMemoDesc(String memoDesc) {
		this.memoDesc = memoDesc;
	}

	public String getMemoType() {
		return memoType;
	}

	public void setMemoType(String memoType) {
		this.memoType = memoType;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(obj instanceof ViewPolicyNcMemo)) {
			return false;
		}
		ViewPolicyNcMemo other = (ViewPolicyNcMemo) obj;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ViewPolicyNcMemo [id=" + id + "]";
	}

}
