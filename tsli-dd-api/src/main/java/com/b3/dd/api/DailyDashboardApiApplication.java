package com.b3.dd.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.databind.ObjectMapper;

@Lazy(true)
@EnableScheduling
@SpringBootApplication(scanBasePackages = "com.b3.dd.api")
@PropertySource("file:${config.path}/application-${spring.profiles.active}.properties")
public class DailyDashboardApiApplication extends SpringBootServletInitializer {

	public static void main(String... args) {
		SpringApplication.run(DailyDashboardApiApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DailyDashboardApiApplication.class);
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

}
