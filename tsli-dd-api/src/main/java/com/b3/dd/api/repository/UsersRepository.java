package com.b3.dd.api.repository;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.constants.Constants.UserStatus;
import com.b3.dd.api.entity.dd.SuperUsers;
import com.b3.dd.api.entity.dd.Users;

public interface UsersRepository extends JpaRepository<Users, Integer> {

	@Query("SELECT u FROM Users u WHERE upper(u.username) = upper(:username)")
	Users findByUsername(@Param("username") String username);

	@Query("SELECT count(u) FROM Users u WHERE u.status = '"+ UserStatus.ACTIVE_STR +"'")
	Integer countStatusActive();
	
	@Query("SELECT count(u) FROM Users u WHERE u.status =  '"+ UserStatus.INACTIVE_STR + "'" )
	Integer countStatusInActive();
	
	@Query("SELECT u FROM Users u "
			+ "WHERE "
			+ " ( UPPER(u.username) like concat('%',UPPER(:username),'%') or :username = null) and "
			+ " ( UPPER(concat(u.username, u.name,u.surname)) like concat('%',UPPER(:name),'%') or :name = null) and "
			+ " ( UPPER(u.userType) = UPPER(:userType) or :userType = null) and "
			+ " (u.status = :status or :status = null) "
			+ " order by u.username asc")
	Page<Users> findUsers(
			@Param("username") String username,
			@Param("name") String name,
			@Param("userType") Character userType,
			@Param("status") Character status,
			Pageable pageable);
	
	@Query( "SELECT u FROM SuperUsers u WHERE upper(u.username) =  upper(:username) and u.isDelete <> 'T' " )
	Optional<SuperUsers> checkSuperUser( @Param("username") String username );

	
}
