package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewOverviewAgent;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewOverviewAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class ViewOverviewAgentController {

	@Autowired
	private ViewOverviewAgentRepository ViewOverviewAgentRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("performance/currentmonth")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceCurrentMonth(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		// Query Case I, sending 3 parameters.
		 System.out.println("# " + request.getAgentCode() + " LEVEL: " + request.getAgentLevel() + " AuthCODE: " + principal.getName() + "userHelper.getUserPosition() " + userHelper.getUserPosition());
		List<ViewOverviewAgent> viewOverviewAgent = ViewOverviewAgentRepository.findByAgentCurrentMonthPerformance(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition());

		return ResponseEntity.ok().body(viewOverviewAgent);
	}
	
	// Get under AL
	@PostMapping("overview/under-line/by-al")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderAl(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> viewOverviewAgent = ViewOverviewAgentRepository.findByUnderAlOverview(
				request.getAgentCode(), principal.getName(), userHelper.getUserPosition());

		return ResponseEntity.ok().body(viewOverviewAgent);
	}
	
	// GET AGENT UNDER AVP
	@PostMapping("overview/under-line/by-avp")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderavp(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderAvpOverview(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER GM
	@PostMapping("overview/under-line/by-gm")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderGm(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderGmOverview(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition());
		
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER ZONE
	@PostMapping("overview/under-line/by-zone")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderZone(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderZoneOverview(
			request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition());
			
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER COMPANY - PRODUCER
	@PostMapping("overview/under-line/by-company-producer") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderCompanyProducer(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderCompanyOverviewProducer(
			request.getAgentLevel(), userHelper.getUserPosition());
				
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL SFC - PRODUCER
	@PostMapping("overview/under-line/by-channel-sfc-producer") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelSfcProducer(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelSfcOverviewProducer(
			request.getAgentLevel(), userHelper.getUserPosition());
					
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL GA - PRODUCER
	@PostMapping("overview/under-line/by-channel-ga-producer") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelGaProducer(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelGaOverviewProducer(
			request.getAgentLevel(), userHelper.getUserPosition());
						
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER COMPANY - AGENT - PC
	@PostMapping("overview/under-line/by-company-agent-pc") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderCompanyAgentPc(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderCompanyOverviewAgentPc(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER COMPANY - AGENT - ACTIVE
	@PostMapping("overview/under-line/by-company-agent-active") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderCompanyAgentActive(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderCompanyOverviewAgentActive(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER COMPANY - AGENT - RYP
	@PostMapping("overview/under-line/by-company-agent-ryp") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderCompanyAgentRyp(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderCompanyOverviewAgentRyp(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	@PostMapping("overview/under-line/by-channel-producer") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelProducer(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelOverviewProducer(
			request.getAgentLevel(), userHelper.getUserPosition(), request.getChannelCode());
						
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	@PostMapping("overview/under-line/by-channel-agent-pc")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelAgentPc(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelLevelAgentPc(
				request.getAgentLevel(), userHelper.getUserPosition(), request.getChannelCode());
		
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	@PostMapping("overview/under-line/by-channel-agent-active")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelAgentActive(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelLevelAgentPc(
				request.getAgentLevel(), userHelper.getUserPosition(), request.getChannelCode());
		
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	@PostMapping("overview/under-line/by-channel-agent-ryp")
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelAgentRyp(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelLevelAgentPc(
				request.getAgentLevel(), userHelper.getUserPosition(), request.getChannelCode());
		
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	
	// OLD VERSION BEFORE CR. - ADD NEW CHANNEL
	// GET AGENT UNDER CHANNEL SFC - AGENT - PC
	@PostMapping("overview/under-line/by-channel-sfc-agent-pc") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelSfcAgentPc(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelSfcOverviewAgentPc(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL SFC - AGENT - ACTIVE
	@PostMapping("overview/under-line/by-channel-sfc-agent-active") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelSfcAgentActive(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelSfcOverviewAgentActive(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL SFC - AGENT - RYP
	@PostMapping("overview/under-line/by-channel-sfc-agent-ryp") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelSfcAgentRyp(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelSfcOverviewAgentRyp(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL GA - AGENT - PC
		@PostMapping("overview/under-line/by-channel-ga-agent-pc") 
		public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelGaAgentPc(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelGaOverviewAgentPc(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL GA - AGENT - ACTIVE
	@PostMapping("overview/under-line/by-channel-ga-agent-active") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelGaAgentActive(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelGaOverviewAgentActive(
				request.getAgentLevel(), userHelper.getUserPosition());
							
		return ResponseEntity.ok().body(agentUnderLine);
	}
	
	// GET AGENT UNDER CHANNEL GA - AGENT - RYP
	@PostMapping("overview/under-line/by-channel-ga-agent-ryp") 
	public ResponseEntity<List<ViewOverviewAgent>>
	getAgentPerformanceUnderChannelGaAgentRyp(@RequestBody RequestModel request, final Principal principal)
			throws ResourceClosedException {
			List<ViewOverviewAgent> agentUnderLine = ViewOverviewAgentRepository.findByUnderChannelGaOverviewAgentRyp(
					request.getAgentLevel(), userHelper.getUserPosition());
								
			return ResponseEntity.ok().body(agentUnderLine);
		}
}
