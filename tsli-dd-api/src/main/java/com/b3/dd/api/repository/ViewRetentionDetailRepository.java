package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewRetentionDetail;

@Repository
public interface ViewRetentionDetailRepository extends CrudRepository<ViewRetentionDetail, Integer> {
	// CASE I. - COMPANY
	@Query(value=""
			+ "SELECT * "
			+ " FROM VIEW_RETENTION_DETAIL"
			+ "	WHERE AGENT_STATUS = :filter"
			+ "		AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<ViewRetentionDetail> findByCompany(
			@Param("filter") String filter
			, @Param("authLevel") String authLevel);
		
	// CASE II. - CHANNEL
	@Query(value=""
			+ "SELECT * "
			+ " FROM VIEW_RETENTION_DETAIL"
			+ "	WHERE AGENT_STATUS = :filter"
			+ "		AND CHANNEL = :channelCode"
			+ "		AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<ViewRetentionDetail> findByChannel(
			@Param("filter") String filter
			, @Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel);
	
	// CASE III. - ZONE
	@Query(value=""
			+ "SELECT * "
			+ " FROM VIEW_RETENTION_DETAIL"
			+ "	WHERE AGENT_STATUS = :filter"
			+ "		AND ZONE = :zoneCode"
			+ "		AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<ViewRetentionDetail> findByZone(
			@Param("filter") String filter
			, @Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
}
