package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewPerformanceQuarterAgent;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewPerformanceQuarterAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewPerformanceQuarterAgentController {
	@Autowired
	private P3ViewPerformanceQuarterAgentRepository P3ViewPerformanceQuarterAgentRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - I.IV : Under Producer
	@PostMapping("agent-performance-quarter")
	public ResponseEntity<List<P3ViewPerformanceQuarterAgent>>
	getTeamDetailWholeMtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String level = request.getAgentLevel();
		List<P3ViewPerformanceQuarterAgent> P3ViewPerformanceQuarterAgent = null;
		
		switch (level) {
		case "AGENT":
			P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByAgent(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "AL":
			P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByAl(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition() 
			);
			break;
		case "AVP":
			P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByAvp(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "GM":
			P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByGm(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewPerformanceQuarterAgent);
	}
}
