package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.DdTPromoteValidateMapping;
import com.b3.dd.api.entity.dd.DdTValidationConfig;
import com.b3.dd.api.entity.dd.P3ViewValidationDetail;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.DdTValidationConfigRepository;
import com.b3.dd.api.repository.P3ViewValidationDetailRepository;
import com.b3.dd.api.repository.DdTPromoteValidateMappingRepository;
import com.b3.dd.api.service.PromoteValidateMappingService;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class DdTValidationConfigController {
	@Autowired
	private DdTValidationConfigRepository DdTValidationConfigRepository;
	@Autowired
	private P3ViewValidationDetailRepository P3ViewValidationDetailRepository;
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private PromoteValidateMappingService PromoteValidateMappingService;
	@Autowired
	private DdTPromoteValidateMappingRepository DdTPromoteValidateMappingRepository;
	
	
	// CASE I.I
    @PostMapping("validation-config")
    public ResponseEntity<Map<String, Object>>
    getTeamDetailWholeMtd(@RequestBody RequestModel request, final Principal principal)
    	throws ResourceClosedException {
    	// Prepare variables
		Map<String, Object> response = new HashMap<String, Object>();
        List<DdTValidationConfig> DdTValidationConfig = null;
        List<P3ViewValidationDetail> P3ViewValidationDetail = null;
        List<DdTPromoteValidateMapping> DdTPromoteValidateMapping = null;
        
        // Query Config
        if (request.getChannelCode().equalsIgnoreCase("SFC")) {
        	DdTValidationConfig = DdTValidationConfigRepository.findConfig(
                request.getClosYm(), request.getChannelCode()
                , request.getAgentLevel(), request.getPositionCode()
            );
        }
        else {
        	DdTValidationConfig = DdTValidationConfigRepository.findConfigGa(
                request.getClosYm(), request.getChannelCode(), request.getAgentLevel()
            );
        }
        
        // Query Validation Detail
        switch (request.getAgentLevel()) {
		case "AGENT":
		case "AG":
			P3ViewValidationDetail = P3ViewValidationDetailRepository.findByAgent(
					request.getAgentCode(), request.getAgentLevel()
					, principal.getName(), userHelper.getUserPosition()
				);
			break;
		case "AL":
			P3ViewValidationDetail = P3ViewValidationDetailRepository.findByAl(
					request.getAgentCode(), request.getAgentLevel()
					, principal.getName(), userHelper.getUserPosition()
				);
			break;
		case "AVP":
			P3ViewValidationDetail = P3ViewValidationDetailRepository.findByAvp(
					request.getAgentCode(), request.getAgentLevel()
					, principal.getName(), userHelper.getUserPosition()
				);
			break;
		case "GM":
			P3ViewValidationDetail = P3ViewValidationDetailRepository.findByGm(
					request.getAgentCode(), request.getAgentLevel()
					, principal.getName(), userHelper.getUserPosition()
				);
			break;
		default:
			break;
		}
        
        // Query Mapping
        DdTPromoteValidateMapping = DdTPromoteValidateMappingRepository.findAll();
        
        // Pack Data Response
        DdTValidationConfig tempValidationConfig = DdTValidationConfig.stream().findFirst().orElse(null);
        if (tempValidationConfig != null) {
        	response.put("config", DdTValidationConfig.stream().findFirst().get());
        } else {
        	response.put("config", null);
        }
        P3ViewValidationDetail tempValidationData = P3ViewValidationDetail.stream().findFirst().orElse(null);
        if (tempValidationData != null) {
        	response.put("data", P3ViewValidationDetail.stream().findFirst().get());
        } else {
        	response.put("data", null);
        }
        response.put("mapping", DdTPromoteValidateMapping);
        
        return ResponseEntity.ok().body(response);
    }
 
}
