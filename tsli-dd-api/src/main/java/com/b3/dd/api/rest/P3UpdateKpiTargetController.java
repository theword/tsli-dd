package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3UpdateKpiTargetRepository;

@RestController
@RequestMapping("/${api.prefix}/edit")
public class P3UpdateKpiTargetController {
	
	@Autowired
	private P3UpdateKpiTargetRepository P3UpdateKpiTargetRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("update-pc-target-year")
	public ResponseEntity<List<String>>
	setYearTarget(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		try {
			java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
			Integer pcYearTarget[] = request.getPcYearTarget();
			String kpiName = "PC";
			P3UpdateKpiTargetRepository.setYearTargetByKpi(
				request.getAgentCode()
				, request.getAgentLevel()
				, request.getClosYm()
				, kpiName
				, request.getPcSumYearTarget()
				, pcYearTarget[0]
				, pcYearTarget[1]
				, pcYearTarget[2]
				, pcYearTarget[3]
				, pcYearTarget[4]
				, pcYearTarget[5]
				, pcYearTarget[6]
				, pcYearTarget[7]
				, pcYearTarget[8]
				, pcYearTarget[9]
				, pcYearTarget[10]
				, pcYearTarget[11]
				, sqlDate
				, principal.getName()
				, userHelper.getUserPosition()
			);
			
			List<String> messages = Arrays.asList("success");
			return ResponseEntity.ok().body(messages);
		} catch (Exception e) {
			List<String> messages = Arrays.asList("failed");
			return ResponseEntity.ok().body(messages);
		}
	}
	
	@PostMapping("update-all-target-year")
	public ResponseEntity<List<String>>
	setAllYearTarget(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		try {
			java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
			
			// Update KPI: PC
			Integer pcYearTarget[] = request.getPcYearTarget();
			String kpiName = "PC";
			P3UpdateKpiTargetRepository.setYearTargetByKpi(
				request.getAgentCode()
				, request.getAgentLevel()
				, request.getClosYm()
				, kpiName
				, request.getPcSumYearTarget()
				, pcYearTarget[0]
				, pcYearTarget[1]
				, pcYearTarget[2]
				, pcYearTarget[3]
				, pcYearTarget[4]
				, pcYearTarget[5]
				, pcYearTarget[6]
				, pcYearTarget[7]
				, pcYearTarget[8]
				, pcYearTarget[9]
				, pcYearTarget[10]
				, pcYearTarget[11]
				, sqlDate
				, principal.getName()
				, userHelper.getUserPosition()
			);
			
			// Update KPI: Active
			Integer activeYearTarget[] = request.getActiveYearTarget();
			kpiName = "NEW_ACTIVE_AGENT";
			P3UpdateKpiTargetRepository.setYearTargetByKpi(
				request.getAgentCode()
				, request.getAgentLevel()
				, request.getClosYm()
				, kpiName
				, request.getActiveSumYearTarget()
				, activeYearTarget[0]
				, activeYearTarget[1]
				, activeYearTarget[2]
				, activeYearTarget[3]
				, activeYearTarget[4]
				, activeYearTarget[5]
				, activeYearTarget[6]
				, activeYearTarget[7]
				, activeYearTarget[8]
				, activeYearTarget[9]
				, activeYearTarget[10]
				, activeYearTarget[11]
				, sqlDate
				, principal.getName()
				, userHelper.getUserPosition()
			);
			
			// Update KPI: NC
			Integer ncYearTarget[] = request.getNcYearTarget();
			kpiName = "NEW_CODE";
			P3UpdateKpiTargetRepository.setYearTargetByKpi(
				request.getAgentCode()
				, request.getAgentLevel()
				, request.getClosYm()
				, kpiName
				, request.getNcSumYearTarget()
				, ncYearTarget[0]
				, ncYearTarget[1]
				, ncYearTarget[2]
				, ncYearTarget[3]
				, ncYearTarget[4]
				, ncYearTarget[5]
				, ncYearTarget[6]
				, ncYearTarget[7]
				, ncYearTarget[8]
				, ncYearTarget[9]
				, ncYearTarget[10]
				, ncYearTarget[11]
				, sqlDate
				, principal.getName()
				, userHelper.getUserPosition()
			);
			
			// Update KPI: RYP
			Integer rypYearTarget[] = request.getRypYearTarget();
			kpiName = "RYP";
			P3UpdateKpiTargetRepository.setYearTargetByKpi(
				request.getAgentCode()
				, request.getAgentLevel()
				, request.getClosYm()
				, kpiName
				, request.getRypSumYearTarget()
				, rypYearTarget[0]
				, rypYearTarget[1]
				, rypYearTarget[2]
				, rypYearTarget[3]
				, rypYearTarget[4]
				, rypYearTarget[5]
				, rypYearTarget[6]
				, rypYearTarget[7]
				, rypYearTarget[8]
				, rypYearTarget[9]
				, rypYearTarget[10]
				, rypYearTarget[11]
				, sqlDate
				, principal.getName()
				, userHelper.getUserPosition()
			);
			
			// Update KPI: RETENTION
			Integer retentionYearTarget[] = request.getRetentionYearTarget();
			kpiName = "RETENTION";
			P3UpdateKpiTargetRepository.setYearTargetByKpi(
				request.getAgentCode()
				, request.getAgentLevel()
				, request.getClosYm()
				, kpiName
				, request.getRetentionSumYearTarget()
				, retentionYearTarget[0]
				, retentionYearTarget[1]
				, retentionYearTarget[2]
				, retentionYearTarget[3]
				, retentionYearTarget[4]
				, retentionYearTarget[5]
				, retentionYearTarget[6]
				, retentionYearTarget[7]
				, retentionYearTarget[8]
				, retentionYearTarget[9]
				, retentionYearTarget[10]
				, retentionYearTarget[11]
				, sqlDate
				, principal.getName()
				, userHelper.getUserPosition()
			);
			
			List<String> messages = Arrays.asList("success");
			return ResponseEntity.ok().body(messages);
		} catch (Exception e) {
			List<String> messages = Arrays.asList("failed");
			return ResponseEntity.ok().body(messages);
		}
	}
}
