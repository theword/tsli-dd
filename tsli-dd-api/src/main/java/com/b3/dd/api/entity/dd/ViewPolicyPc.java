package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_POLICY_PC", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewPolicyPc.findAll", query = "SELECT v FROM ViewPolicyPc v")
})
public class ViewPolicyPc implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 6)
    private String closYm;
	@Column(name = "STATUS", length = 25)
    private String status;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	
	@Column(name = "CHANNEL", length = 5)
    private String channel;
	@Column(name = "ZONE", length = 25)
    private String zone;
	@Column(name = "GM_CODE", length = 25)
    private String gmCode;
	@Column(name = "GM_NAME", length = 300)
    private String gmName;
	@Column(name = "AVP_CODE", length = 25)
    private String avpCode;
	@Column(name = "AVP_NAME", length = 300)
    private String avpName;
	@Column(name = "AL_CODE", length = 25)
    private String alCode;
	@Column(name = "AL_NAME", length = 300)
    private String alName;
	@Column(name = "AGENT_CODE", length = 25)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 25)
    private String agentLevel;
	@Column(name = "AGENT_NAME", length = 300)
    private String agentName;
	
	@Column(name = "POLICY_CODE", length = 15)
    private String policyCode;
	@Column(name = "PROD_NAME", length = 255)
    private String prodName;
	@Column(name = "APPROVE_DATE", length = 10)
    private String approveDate;
	@Column(name = "SUBMIT_DATE", length = 10)
    private String submitDate;
	@Column(name = "PC")
    private Double pc;
	@Column(name = "MEMO", length = 255)
    private String memo;
	
	@Column(name = "COUNTER", length = 255)
    private String counter;
	@Column(name = "CUSTOMER_NAME", length = 255)
    private String customerName;
	
	public ViewPolicyPc() {
		
	}

	public ViewPolicyPc(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	public Double getPc() {
		return pc;
	}

	public void setPc(Double pc) {
		this.pc = pc;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getAvpName() {
		return avpName;
	}

	public void setAvpName(String avpName) {
		this.avpName = avpName;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewPolicyPc)) {
            return false;
        }
        ViewPolicyPc other = (ViewPolicyPc) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewPolicyPc [id=" + id + "]";
	}

}
