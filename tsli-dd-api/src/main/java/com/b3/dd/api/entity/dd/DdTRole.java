/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "DD_T_ROLE", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DdTRole.findAll", query = "SELECT d FROM DdTRole d")
    , @NamedQuery(name = "DdTRole.findById", query = "SELECT d FROM DdTRole d WHERE d.id = :id")
    , @NamedQuery(name = "DdTRole.findByRole", query = "SELECT d FROM DdTRole d WHERE d.role = :role")
    , @NamedQuery(name = "DdTRole.findByDesc", query = "SELECT d FROM DdTRole d WHERE d.desc = :desc")
    , @NamedQuery(name = "DdTRole.findByCreateBy", query = "SELECT d FROM DdTRole d WHERE d.createBy = :createBy")
    , @NamedQuery(name = "DdTRole.findByUpdateDate", query = "SELECT d FROM DdTRole d WHERE d.updateDate = :updateDate")
    , @NamedQuery(name = "DdTRole.findByUpdateBy", query = "SELECT d FROM DdTRole d WHERE d.updateBy = :updateBy")
    , @NamedQuery(name = "DdTRole.findByIsDelete", query = "SELECT d FROM DdTRole d WHERE d.isDelete = :isDelete")})
public class DdTRole implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false )
    private Integer id;
    @Basic(optional = false)
    @Column(name = "ROLE", nullable = false, length = 256)
    private String role;
    @Column(name = "DESC")
    private String desc;
    @Column(name = "CREATE_BY", length = 255)
    private String createBy;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY", length = 255)
    private String updateBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId", fetch = FetchType.EAGER)
    private List<DdTUserRoles> ddTUserRolesList;

    public DdTRole() {
    }

    public DdTRole(Integer id) {
        this.id = id;
    }

    public DdTRole(Integer id, String role) {
        this.id = id;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    @XmlTransient
    public List<DdTUserRoles> getDdTUserRolesList() {
        return ddTUserRolesList;
    }

    public void setDdTUserRolesList(List<DdTUserRoles> ddTUserRolesList) {
        this.ddTUserRolesList = ddTUserRolesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTRole)) {
            return false;
        }
        DdTRole other = (DdTRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.b3.dd.api.entity.DdTRole[ id=" + id + " ]";
    }
    
}
