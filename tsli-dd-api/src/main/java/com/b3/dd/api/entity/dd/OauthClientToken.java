/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "OAUTH_CLIENT_TOKEN", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OauthClientToken.findAll", query = "SELECT o FROM OauthClientToken o")
    , @NamedQuery(name = "OauthClientToken.findByTokenId", query = "SELECT o FROM OauthClientToken o WHERE o.tokenId = :tokenId")
    , @NamedQuery(name = "OauthClientToken.findByAuthenticationId", query = "SELECT o FROM OauthClientToken o WHERE o.authenticationId = :authenticationId")
    , @NamedQuery(name = "OauthClientToken.findByUserName", query = "SELECT o FROM OauthClientToken o WHERE o.userName = :userName")
    , @NamedQuery(name = "OauthClientToken.findByClientId", query = "SELECT o FROM OauthClientToken o WHERE o.clientId = :clientId")})
public class OauthClientToken implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "TOKEN_ID", length = 256)
    private String tokenId;
    @Lob
    @Column(name = "TOKEN")
    private Serializable token;
    @Id
    @Basic(optional = false)
    @Column(name = "AUTHENTICATION_ID", nullable = false, length = 256)
    private String authenticationId;
    @Column(name = "USER_NAME", length = 256)
    private String userName;
    @Column(name = "CLIENT_ID", length = 256)
    private String clientId;

    public OauthClientToken() {
    }

    public OauthClientToken(String authenticationId) {
        this.authenticationId = authenticationId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public Serializable getToken() {
        return token;
    }

    public void setToken(Serializable token) {
        this.token = token;
    }

    public String getAuthenticationId() {
        return authenticationId;
    }

    public void setAuthenticationId(String authenticationId) {
        this.authenticationId = authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (authenticationId != null ? authenticationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OauthClientToken)) {
            return false;
        }
        OauthClientToken other = (OauthClientToken) object;
        if ((this.authenticationId == null && other.authenticationId != null) || (this.authenticationId != null && !this.authenticationId.equals(other.authenticationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsli.dd.entity.OauthClientToken[ authenticationId=" + authenticationId + " ]";
    }
    
}
