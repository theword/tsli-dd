/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "USERS_LOGGING", catalog = "", schema = "")
@XmlRootElement
public class UsersLogging implements Serializable {
	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, insertable = true, updatable = true, precision = 38, scale = 0)
	private Long id;
	@Column(name = "FULL_NAME", nullable = true, length = 1000)
	private String fullName;
	@Basic(optional = false)
	@Column(name = "USERNAME", nullable = false, length = 1000)
	private String username;
	@Basic(optional = false)
	@Column(name = "ACTION_TYPE", nullable = false, length = 1000)
	private String actionType;
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Column(name = "STATUS", nullable = true, length = 1000)
	private String status;
	@Column(name = "INFO_1", nullable = true, length = 2000)
	private String info1;
	@Column(name = "INFO_2", nullable = true, length = 2000)
	private String info2;
	@Column(name = "INFO_3", nullable = true, length = 2000)
	private String info3;
	@Column(name = "INFO_4", nullable = true, length = 2000)
	private String info4;
	@Column(name = "INFO_5", nullable = true, length = 2000)
	private String info5;
	
	public static enum ActionType {
		LOGIN, LOGOUT
	}

	public UsersLogging() {
	}

	public UsersLogging(Long id) {
		this.id = id;
	}

	public UsersLogging(String fullName, String username, String status, ActionType actionType, Date createDate) {
		super();
		this.fullName = fullName;
		this.username = username;
		this.createDate = createDate;
		this.status = status;
		switch (actionType) {
		case LOGIN:
			this.actionType = "LOGIN";
			break;
		case LOGOUT:
			this.actionType = "LOGOUT";
			break;
		default:
			break;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof UsersLogging)) {
			return false;
		}
		UsersLogging other = (UsersLogging) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	
	public String getInfo1() {
		return info1;
	}

	public void setInfo1(String info1) {
		this.info1 = info1;
	}

	public String getInfo2() {
		return info2;
	}

	public void setInfo2(String info2) {
		this.info2 = info2;
	}

	public String getInfo3() {
		return info3;
	}

	public void setInfo3(String info3) {
		this.info3 = info3;
	}

	public String getInfo4() {
		return info4;
	}

	public void setInfo4(String info4) {
		this.info4 = info4;
	}

	public String getInfo5() {
		return info5;
	}

	public void setInfo5(String info5) {
		this.info5 = info5;
	}

	@Override
	public String toString() {
		return "UsersLogging [id=" + id + ", fullName=" + fullName + ", username=" + username + ", actionType="
				+ actionType + ", createDate=" + createDate + "]";
	}

}
