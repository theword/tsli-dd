package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_RETENTION_YTD_COMP", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewYtdCompRetention.findAll", query = "SELECT v FROM ViewYtdCompRetention v")
})
public class ViewYtdCompRetention implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 4)
    private String closYm;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "CODE", length = 30)
    private String code;
	@Column(name = "TARGET_YEAR")
    private Double targetYear;
	@Column(name = "RETENTION_YTD")
    private Double retentionYtd;
	@Column(name = "PERCENT")
    private Double percent;
	
	public ViewYtdCompRetention() {
		
	}

	public ViewYtdCompRetention(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getTargetYear() {
		return targetYear;
	}

	public void setTargetYear(Double targetYear) {
		this.targetYear = targetYear;
	}

	public Double getPercent() {
		return percent;
	}

	public void setPercent(Double percent) {
		this.percent = percent;
	}

	public Double getRetentionYtd() {
		return retentionYtd;
	}

	public void setRetentionYtd(Double retentionYtd) {
		this.retentionYtd = retentionYtd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewYtdCompRetention)) {
            return false;
        }
        ViewYtdCompRetention other = (ViewYtdCompRetention) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewYtdCompRetention [id=" + id + "]";
	}

}
