package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewOverviewYtdAgent;

@Repository
public interface P3ViewOverviewYtdAgentRepository extends CrudRepository<P3ViewOverviewYtdAgent, Integer> {

	// CASE I.I - PRODUCER find by under AL (get AG)
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE AL_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
	, nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderAl(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II - PRODUCER find by under AVP (get AL or AG)
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE AVP_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
	, nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderAvp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III - PRODUCER find by under GM (get AVP or AL or AG)
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE GM_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
	, nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderGm(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE II.I - ZONE find by under Zone (get GM or AVP or AL or AG)
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE ZONE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderZone(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	
	// CHANNEL find by under Channel (100 Rows DESC by KPI) (get GM or AVP or AL or AG)
	// CASE III.I - KPI - PC_SUBMIT
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE GROUP_CHANNEL = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY PC_SUBMIT DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderChannelPc(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	// CASE III.II - KPI - ACTIVE
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE GROUP_CHANNEL = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY ACTIVE DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderChannelActive(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	// CASE III.III - KPI - NC
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE GROUP_CHANNEL = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY NC DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderChannelNc(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	// CASE III.IV - KPI - RYP
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE GROUP_CHANNEL = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY RYP DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderChannelRyp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	
	// COMPANY find by under Company (100 Rows DESC by KPI) (get GM or AVP or AL or AG)
	// CASE IV.I - KPI - PC_SUBMIT
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY PC_SUBMIT DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderCompanyPc(
			@Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	// CASE IV.II - KPI - ACTIVE
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY ACTIVE DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderCompanyActive(
			@Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	// CASE IV.III - KPI - NC
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY NC DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderCompanyNc(
			@Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
	// CASE IV.IV - KPI - RYP
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT"
			+ " WHERE AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND (:authLevel = 'ZONE')"
			+ "	ORDER BY RYP DESC"
			+ "	FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdAgent> findByUnderCompanyRyp(
			@Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authLevel") String authLevel);
}
