package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewPerSummary;

@Repository
public interface P3ViewPerSummaryRepository extends CrudRepository<P3ViewPerSummary, Integer>{

	// CASE I. PERSISTENCY SUMMARY: By agentCode
	@Query("SELECT v FROM P3ViewPerSummary v"
			+ "	WHERE v.persistencyType = :filter"
			+ "		AND v.agentCode = :agentCode"
			+ "		AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "		v.agentCode = :authCode"
			+ "		OR ("
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.gmCode = :authCode AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.avpCode = :authCode AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.alCode = :authCode AND va.agentCode = :agentCode) > 0"
			+ " 	)"
			+ "		OR (:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR ("
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ " 	)"
			+ " )"
			+ " ORDER BY v.closYm DESC")
	public List<P3ViewPerSummary> findByAgentWithPerType(
			@Param("filter") String filter
			, @Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
