package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewOverviewYtdSon;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewOverviewYtdSonRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewOverviewYtdSonController {
	@Autowired
	private P3ViewOverviewYtdSonRepository P3ViewOverviewYtdSonRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - I.IV
	@PostMapping("teamdetail-agent-ytd-by-son")
	public ResponseEntity<List<P3ViewOverviewYtdSon>>
	getGmPromotionDetail(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String filter = request.getFilter();
		List<P3ViewOverviewYtdSon> P3ViewOverviewYtdSon = null;
		
		switch (filter) {
		case "PC":
			P3ViewOverviewYtdSon = P3ViewOverviewYtdSonRepository.findByLevelWithPc(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), principal.getName(), userHelper.getUserPosition() 
			);
			break;
		case "ACTIVE":
			P3ViewOverviewYtdSon = P3ViewOverviewYtdSonRepository.findByLevelWithActive(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "NC":
			P3ViewOverviewYtdSon = P3ViewOverviewYtdSonRepository.findByLevelWithNewCode(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "RYP":
			P3ViewOverviewYtdSon = P3ViewOverviewYtdSonRepository.findByLevelWithRyp(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), principal.getName(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewOverviewYtdSon);
	}
}
