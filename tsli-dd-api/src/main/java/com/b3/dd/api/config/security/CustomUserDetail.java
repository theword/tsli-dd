package com.b3.dd.api.config.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.b3.dd.api.entity.dd.Users;


public class CustomUserDetail implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Users info;
	private String username;
	private String password;
	private Collection<? extends GrantedAuthority> roles;
	private Map<String, Object> optional;
	
	
	public CustomUserDetail() {
	}

	public CustomUserDetail(String username, String password, Collection<? extends GrantedAuthority> roles) {
		this.username = username;
		this.roles = roles;
		this.password = password;
	}
	public CustomUserDetail(String username, String password, Users info, Collection<? extends GrantedAuthority> roles, Map<String, Object> optional) {
		this.username = username;
		this.roles = roles;
		this.info = info;
		this.password = password;
		this.optional = optional;
	}
	
	public CustomUserDetail(String username, String password, Users info, Collection<? extends GrantedAuthority> roles) {
		this.username = username;
		this.roles = roles;
		this.password = password;
		this.info = info;
	}

	public Users getInfo() {
		return info;
	}

	public void setInfo(Users info) {
		this.info = info;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	
	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}
 
	
	public Map<String, Object> getOptional() {
		return optional;
	}

	public void setOptional(Map<String, Object> optional) {
		this.optional = optional;
	}

	@Override
	public String toString() {
		return "CustomUserDetail [info=" + info + ", username=" + username + ", password=" + password + ", roles="
				+ roles + "]";
	}

	public String getPosition() {
		Object[] o =  getAuthorities().stream().map(new Function<GrantedAuthority, Object>() {
			public Object apply(GrantedAuthority item) {
				return item.getAuthority();
			}
		}).toArray();
		String[] stringArray = Arrays.copyOf(o, o.length, String[].class);
		return String.join(",", stringArray);
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

}
