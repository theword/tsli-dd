package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_OVERVIEW_COMP", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewOverviewComp.findAll", query = "SELECT v FROM ViewOverviewComp v")
	, @NamedQuery(name = "ViewOverviewComp.findByCode", query = "SELECT v FROM ViewOverviewComp v WHERE v.code = :code")
})
public class ViewOverviewComp implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 30)
    private String closYm;
	@Column(name = "STAFF_LEVEL", length = 30)
    private String staffLevel;
	@Column(name = "GROUP_CHANNEL", length = 30)
    private String groupChannel;
	@Column(name = "CODE", length = 30)
    private String code;
	@Column(name = "ZONE_NAME", length = 30)
    private String zoneName;
	
	@Column(name = "TARGET_PC")
	private Double targetPc;
	@Column(name = "TARGET_ACT")
	private Double targetAct;
	@Column(name = "TARGET_NC")
	private Double targetNc;
	@Column(name = "TARGET_RT")
	private Double targetRt;
	@Column(name = "TARGET_RYP")
	private Double targetRyp;
	
	@Column(name = "PC_APPROVE")
	private Double pcApprove;
	@Column(name = "PC_SUBMIT")
	private Double pcSubmit;
	@Column(name = "NC")
	private Double nc;
	@Column(name = "ACTIVE")
	private Double active;
	@Column(name = "RYP")
	private Double ryp;
	@Column(name = "RETENTION")
	private Double retention;
	
	@Column(name = "PC_PERCENT")
	private Double pcPercent;
	@Column(name = "ACTIVE_PERCENT")
	private Double activePercent;
	@Column(name = "NC_PERCENT")
	private Double ncPercent;
	@Column(name = "RYP_PERCENT")
	private Double rypPercent;
	
	public ViewOverviewComp() {
		
	}

	public ViewOverviewComp(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getStaffLevel() {
		return staffLevel;
	}

	public void setStaffLevel(String staffLevel) {
		this.staffLevel = staffLevel;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public Double getTargetPc() {
		return targetPc;
	}

	public void setTargetPc(Double targetPc) {
		this.targetPc = targetPc;
	}

	public Double getTargetAct() {
		return targetAct;
	}

	public void setTargetAct(Double targetAct) {
		this.targetAct = targetAct;
	}

	public Double getTargetNc() {
		return targetNc;
	}

	public void setTargetNc(Double targetNc) {
		this.targetNc = targetNc;
	}

	public Double getTargetRt() {
		return targetRt;
	}

	public void setTargetRt(Double targetRt) {
		this.targetRt = targetRt;
	}

	public Double getTargetRyp() {
		return targetRyp;
	}

	public void setTargetRyp(Double targetRyp) {
		this.targetRyp = targetRyp;
	}

	public Double getPcApprove() {
		return pcApprove;
	}

	public void setPcApprove(Double pcApprove) {
		this.pcApprove = pcApprove;
	}

	public Double getPcSubmit() {
		return pcSubmit;
	}

	public void setPcSubmit(Double pcSubmit) {
		this.pcSubmit = pcSubmit;
	}

	public Double getNc() {
		return nc;
	}

	public void setNc(Double nc) {
		this.nc = nc;
	}

	public Double getActive() {
		return active;
	}

	public void setActive(Double active) {
		this.active = active;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getRetention() {
		return retention;
	}

	public void setRetention(Double retention) {
		this.retention = retention;
	}

	public Double getPcPercent() {
		return pcPercent;
	}

	public void setPcPercent(Double pcPercent) {
		this.pcPercent = pcPercent;
	}

	public Double getActivePercent() {
		return activePercent;
	}

	public void setActivePercent(Double activePercent) {
		this.activePercent = activePercent;
	}

	public Double getNcPercent() {
		return ncPercent;
	}

	public void setNcPercent(Double ncPercent) {
		this.ncPercent = ncPercent;
	}

	public Double getRypPercent() {
		return rypPercent;
	}

	public void setRypPercent(Double rypPercent) {
		this.rypPercent = rypPercent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewOverviewComp)) {
            return false;
        }
        ViewOverviewComp other = (ViewOverviewComp) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewOverviewComp [id=" + id + "]";
	}
}
