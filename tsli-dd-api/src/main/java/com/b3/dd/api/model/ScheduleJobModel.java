package com.b3.dd.api.model;

import java.util.Calendar;
import java.util.Date;

public class ScheduleJobModel {

	private String schedule;
	private String closym;
	private Calendar calendar;
	public ScheduleJobModel() {
		super();
		
		calendar = Calendar.getInstance();
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getClosym() {
		return closym;
	}

	public void setClosym(String closym) {
		this.closym = closym;
	}

	public Date getNow() {
		return calendar.getTime();
	}

	public int getYear() {
		return  calendar.get( Calendar.YEAR );
	}
	public String getDay() {
		return String.format("%02d", calendar.get( Calendar.DAY_OF_MONTH )) ;
	}
	public String getMonth() {
		return String.format("%02d", calendar.get( Calendar.MONTH ) +1) ;
	}
}
