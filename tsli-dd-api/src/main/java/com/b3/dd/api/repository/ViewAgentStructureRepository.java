package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewAgentStructure;

@Repository
public interface ViewAgentStructureRepository extends CrudRepository<ViewAgentStructure, Integer> {

//	::NATIVE ORACLE SQL::
//	SELECT * FROM VIEW_AGENT_STRUCTURE 
//    WHERE AGENT_CODE = '221102008'
//        AND (GM_CODE = '221102008' OR AVP_CODE = '221102008' OR AL_CODE = '221102008' OR AGENT_LEVEL = '221102008')
	
	@Query("SELECT v FROM ViewAgentStructure v WHERE v.agentCode = :agentCode"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )")
	public List<ViewAgentStructure> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel );
	
}
