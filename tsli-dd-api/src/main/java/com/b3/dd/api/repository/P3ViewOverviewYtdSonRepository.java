package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.entity.dd.P3ViewOverviewYtdSon;

public interface P3ViewOverviewYtdSonRepository extends CrudRepository<P3ViewOverviewYtdSon, Integer> {

	// CASE I.I find by anyCode with PC
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY PC_SUBMIT DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdSon> findByLevelWithPc(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II find by anyCode with Active
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY ACTIVE DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdSon> findByLevelWithActive(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III find by anyCode with NewCode
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY NC DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdSon> findByLevelWithNewCode(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV find by anyCode with RYP
	@Query(value=""
			+ "SELECT * FROM VIEW_OVERVIEW_YTD_AGENT_SON"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND CLOS_YM = :closYm"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY RYP DESC"
			+ " FETCH FIRST 100 ROWS ONLY", nativeQuery = true)
	public List<P3ViewOverviewYtdSon> findByLevelWithRyp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
