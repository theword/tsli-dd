package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.DdTValidationConfig;

@Repository
public interface DdTValidationConfigRepository extends CrudRepository<DdTValidationConfig, Integer> {
	// CASE I.I GET SFC
	@Query(value=""
			+ "SELECT * FROM DD_T_VALIDATION_CONFIG"
			+ " WHERE EFFECTIVE_YM = :closYm"
			+ "		AND GROUP_CHANNEL = :channelCode"
			+ "		AND AGENT_LEVEL = :agentLevel"
			+ "		AND POSITION_CODE = :positionCode"
	, nativeQuery = true)
	public List<DdTValidationConfig> findConfig(
			@Param("closYm") String closYm
			, @Param("channelCode") String channelCode
			, @Param("agentLevel") String agentLevel
			, @Param("positionCode") String positionCode
	);
	
	// CASE I.I GET GA
	@Query(value=""
			+ "SELECT * FROM DD_T_VALIDATION_CONFIG"
			+ " WHERE EFFECTIVE_YM = :closYm"
			+ "		AND GROUP_CHANNEL = :channelCode"
			+ "		AND AGENT_LEVEL = :agentLevel"
	, nativeQuery = true)
	public List<DdTValidationConfig> findConfigGa(
			@Param("closYm") String closYm
			, @Param("channelCode") String channelCode
			, @Param("agentLevel") String agentLevel
	);
}
