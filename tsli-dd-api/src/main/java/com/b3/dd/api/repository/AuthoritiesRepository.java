package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.entity.dd.Authorities;


public interface AuthoritiesRepository extends CrudRepository<Authorities, Integer> {

	@Query("SELECT a FROM Authorities a WHERE a.username = :username")
	List<Authorities> findByUsername(@Param("username") String username);
}
