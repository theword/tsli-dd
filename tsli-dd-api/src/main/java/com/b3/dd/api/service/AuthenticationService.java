package com.b3.dd.api.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.web.client.RestTemplate;

import com.b3.dd.api.model.AuthenticationRequestModel;
import com.b3.dd.api.model.AuthenticationResponseModel;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AuthenticationService {

	private RestTemplate restTemplate;
	private String uri;

	public AuthenticationService(@Value("${auth.login.url}") String uri, RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
		this.uri = uri;
	}

	public AuthenticationResponseModel login(String username, String password) {

		AuthenticationRequestModel request = new AuthenticationRequestModel(username, password);
		AuthenticationResponseModel response = restTemplate.postForObject(uri, request,
				AuthenticationResponseModel.class);

		return response;
	}

}
