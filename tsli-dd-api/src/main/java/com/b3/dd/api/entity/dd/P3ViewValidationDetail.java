package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_VALIDATION_DETAIL", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewValidationDetail.findAll", query = "SELECT v FROM P3ViewValidationDetail v")
})
public class P3ViewValidationDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "ZONE", length = 4)
    private String zone;
	
	@Column(name = "GM_CODE", length = 20)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 20)
    private String avpCode;
	@Column(name = "AL_CODE", length = 20)
    private String alCode;
	@Column(name = "AGENT_CODE", length = 30)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	
	@Column(name = "PC_DIRECT")
    private Double pcDirect;
	@Column(name = "PC_WHOLE")
    private Double pcWhole;
	@Column(name = "NO_AL")
    private Double noAl;
	@Column(name = "ACTIVE_PRODUCER")
    private Double activeProducer;
	@Column(name = "NEW_CODE")
    private Double newCode;
	
	@Column(name = "ACTIVE_PRODUCER_WHOLE")
    private Double activeProducerWhole;
	@Column(name = "NEW_CODE_WHOLE")
    private Double newCodeWhole;
	
	
	@Column(name = "SMART_MANAGER_2", length = 9)
    private String smartManager2;
	
	@Column(name = "SMART_MANAGER_1", length = 9)
    private String smartManager1;
	
	@Column(name = "PERSISTENCY", length = 40)
    private String persistency;
	
	public P3ViewValidationDetail() {	
	}

	public P3ViewValidationDetail(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewValidationDetail)) {
            return false;
        }
        P3ViewValidationDetail other = (P3ViewValidationDetail) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public Double getPcDirect() {
		return pcDirect;
	}

	public void setPcDirect(Double pcDirect) {
		this.pcDirect = pcDirect;
	}

	public Double getPcWhole() {
		return pcWhole;
	}

	public void setPcWhole(Double pcWhole) {
		this.pcWhole = pcWhole;
	}

	public Double getNoAl() {
		return noAl;
	}

	public void setNoAl(Double noAl) {
		this.noAl = noAl;
	}

	public Double getActiveProducer() {
		return activeProducer;
	}

	public void setActiveProducer(Double activeProducer) {
		this.activeProducer = activeProducer;
	}

	public Double getNewCode() {
		return newCode;
	}

	public void setNewCode(Double newCode) {
		this.newCode = newCode;
	}

	public String getSmartManager1() {
		return smartManager1;
	}

	public void setSmartManager1(String smartManager1) {
		this.smartManager1 = smartManager1;
	}
	
	
	public String getSmartManager2() {
		return smartManager2;
	}

	public void setSmartManager2(String smartManager2) {
		this.smartManager2 = smartManager2;
	}

	public String getPersistency() {
		return persistency;
	}

	public void setPersistency(String persistency) {
		this.persistency = persistency;
	}

	public Double getActiveProducerWhole() {
		return activeProducerWhole;
	}

	public void setActiveProducerWhole(Double activeProducerWhole) {
		this.activeProducerWhole = activeProducerWhole;
	}

	public Double getNewCodeWhole() {
		return newCodeWhole;
	}

	public void setNewCodeWhole(Double newCodeWhole) {
		this.newCodeWhole = newCodeWhole;
	}

}
