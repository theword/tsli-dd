package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewCompensationConfig;
import com.b3.dd.api.entity.dd.P3ViewPerformanceQuarterAgent;
import com.b3.dd.api.entity.dd.ViewAllYtdAgent;
import com.b3.dd.api.entity.dd.ViewOverviewAgent;
import com.b3.dd.api.entity.dd.ViewPerformanceAgent;
import com.b3.dd.api.entity.dd.ViewYtdAgentPc;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewCompensationConfigRepository;
import com.b3.dd.api.repository.P3ViewPerformanceQuarterAgentRepository;
import com.b3.dd.api.repository.ViewOverviewAgentRepository;
import com.b3.dd.api.repository.ViewYtdAgentPcRepository;
import com.b3.dd.api.repository.ViewAllYtdAgentRepository;
import com.b3.dd.api.repository.ViewPerformanceAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewCompensationConfigController {
	private static final Logger log = LogManager.getLogger(P3ViewCompensationConfigController.class);
	@Autowired
	private P3ViewCompensationConfigRepository P3ViewCompensationConfigRepository;
	@Autowired
	private ViewOverviewAgentRepository ViewOverviewAgentRepository;
	@Autowired
	private ViewYtdAgentPcRepository ViewYtdAgentPcRepository;
	@Autowired
	private P3ViewPerformanceQuarterAgentRepository P3ViewPerformanceQuarterAgentRepository;
	@Autowired
	private ViewAllYtdAgentRepository ViewAllYtdAgentRepository;
	@Autowired
	private ViewPerformanceAgentRepository ViewPerformanceAgentRepository;
	
	
	@Autowired
	private UserHelper userHelper;
	
    // CASE I.I - I.IV : Under Producer
    @PostMapping("compensation-config")
    public ResponseEntity<List<P3ViewCompensationConfig>>
    getTeamDetailWholeMtd(@RequestBody RequestModel request, final Principal principal)
        throws ResourceClosedException {
        List<P3ViewCompensationConfig> P3ViewCompensationConfig = null;
        
        P3ViewCompensationConfig = P3ViewCompensationConfigRepository.findConfig(
            request.getClosYm(), request.getChannelCode()
            , request.getAgentLevel(), request.getFilter()
        );
        
        return ResponseEntity.ok().body(P3ViewCompensationConfig);
    }
    
    // CASE II : OV
    @PostMapping("compensation-config-ov")
    public ResponseEntity<Map<String, Object>>
    getCompenConfigWithOv(@RequestBody RequestModel request, final Principal principal)
        throws ResourceClosedException {
    	// Prepare variables
		Map<String, Object> response = new HashMap<String, Object>();
		List<P3ViewCompensationConfig> P3ViewCompensationConfig = null;
        
		// Query Config
        P3ViewCompensationConfig = P3ViewCompensationConfigRepository.findConfig(
            request.getClosYm(), request.getChannelCode()
            , request.getAgentLevel(), request.getFilter()
        );
        
        // Query PC & Active MTD
        Double pcApproved = 0D;
        Double active = 0D;
        List<ViewOverviewAgent> viewOverviewAgent = ViewOverviewAgentRepository.findByAgentCurrentMonthPerformance(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
		);
		ViewOverviewAgent m = viewOverviewAgent.stream().findFirst().get();
		pcApproved = m.getPcApprove();
		active = m.getActive();
        
        response.put("config", P3ViewCompensationConfig);
		response.put("pcApproved", pcApproved);
		response.put("active", active);
        
        return ResponseEntity.ok().body(response);
    }
	
	// CASE III : Under Producer with monthly, quarterly and yearly
	@PostMapping("compensation-config-with-pc")
	public ResponseEntity<Map<String, Object>>
	getCompenConfigWithPc(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		
		//@require postData closYm, channelCode, agentLevel, filter, agentCode
		// Prepare variables
		Map<String, Object> response = new HashMap<String, Object>();
		List<P3ViewCompensationConfig> P3ViewCompensationConfig = null;
		
		String agentLevelConfig = request.getAgentLevel();
//		if (agentLevelConfig.equalsIgnoreCase("AVP")) {
//			agentLevelConfig = "VP";
//		}
		
		P3ViewCompensationConfig = P3ViewCompensationConfigRepository.findConfig(
			request.getClosYm(), request.getChannelCode()
			, agentLevelConfig, request.getFilter()
		);
		
		
		P3ViewCompensationConfig _res = P3ViewCompensationConfig.stream().findFirst().orElse(null);
		Double pcApproved = 0D;
		Double noCase = 0D;
		
		if (_res != null) {
			
			String periodType = _res.getCalculationPeriod();
			
			switch (periodType) {
			// Month
			case "M":
				List<ViewOverviewAgent> viewOverviewAgent = ViewOverviewAgentRepository.findByAgentCurrentMonthPerformance(
						request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
				);
				ViewOverviewAgent m = viewOverviewAgent.stream().findFirst().get();
				pcApproved = m.getPcApproveBonus();
				
				// Career Benefit
				if (request.getFilter().equals("โบนัสตัวแทนอาชีพ")) {
					List<ViewPerformanceAgent> ViewPerformanceAgent = ViewPerformanceAgentRepository.findByAgentCode(
							request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
					);
					ViewPerformanceAgent mtdCase = ViewPerformanceAgent.stream().findFirst().get();
					noCase = mtdCase.getNoCase();
					response.put("noCase", noCase);
				}
				break;
				
			// Quarter
			case "Q":
				List<P3ViewPerformanceQuarterAgent> P3ViewPerformanceQuarterAgent = null;
				switch (request.getAgentLevel()) {
				case "AGENT":
					P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByAgent(
						request.getAgentCode(), request.getAgentLevel()
						, principal.getName(), userHelper.getUserPosition()
					);
					break;
				case "AL":
					P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByAl(
						request.getAgentCode(), request.getAgentLevel()
						, principal.getName(), userHelper.getUserPosition() 
					);
					break;
				case "AVP":
					P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByAvp(
						request.getAgentCode(), request.getAgentLevel()
						, principal.getName(), userHelper.getUserPosition()
					);
					break;
				case "GM":
					P3ViewPerformanceQuarterAgent = P3ViewPerformanceQuarterAgentRepository.findByGm(
						request.getAgentCode(), request.getAgentLevel()
						, principal.getName(), userHelper.getUserPosition()
					);
					break;
				default:
					break;
				}
				P3ViewPerformanceQuarterAgent q = P3ViewPerformanceQuarterAgent.stream().findFirst().get();
				
				// findByQuarter
				switch (calQuarter(request.getClosYm())) {
				case 1:
					pcApproved = q.getQ1();
					break;
				case 2:
					pcApproved = q.getQ2();
					break;
				case 3:
					pcApproved = q.getQ3();
					break;
				case 4:
					pcApproved = q.getQ4();
					break;
				default:
					pcApproved = q.getQ1();
					break;
				}
				break;
				
			// Year
			case "Y":
				List<ViewYtdAgentPc> ViewYtdAgentPc = ViewYtdAgentPcRepository.findByAgentCode(
						request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition());
				ViewYtdAgentPc y = ViewYtdAgentPc.stream().findFirst().get();
				pcApproved = y.getPcApproveBonus();
				
				// Career Benefit
				if (request.getFilter().equals("โบนัสตัวแทนอาชีพ")) {
					List<ViewAllYtdAgent> ViewAllYtdAgent = ViewAllYtdAgentRepository.findByAgentCurrentYear(
						request.getAgentCode(), request.getAgentLevel(), principal.getName()
					);
					ViewAllYtdAgent ytdCase = ViewAllYtdAgent.stream().findFirst().get();
					noCase = ytdCase.getNoCase();
					response.put("noCase", noCase);
				}
				break;
			default:
				break;
			}
			response.put("config", P3ViewCompensationConfig);
			response.put("pcApproved", pcApproved);
		}
		else {
			response.put("config", null);
			response.put("pcApproved", pcApproved);
		}
		
		return ResponseEntity.ok().body(response);
	}
	
	public static int calQuarter(String closYm) {
		String month = closYm.substring(4, 6);
		int monthNumber = Integer.parseInt(month) - 1;
		return (monthNumber / 3) + 1;
	}
}
