package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewOverviewComp;
import com.b3.dd.api.repository.ViewOverviewCompRepository;

@RestController
@RequestMapping("/${api.prefix}/company")
public class ViewOverviewCompController {

	@Autowired
	private ViewOverviewCompRepository ViewOverviewCompRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("/overview/all")
	public ResponseEntity<List<ViewOverviewComp>> 
	findAllOverview()
	throws ResourceClosedException {
		List<ViewOverviewComp> viewOverviewComp = ViewOverviewCompRepository.findAll(
				userHelper.getUserPosition());
		return ResponseEntity.ok().body(viewOverviewComp);
	}
}
