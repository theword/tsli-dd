package com.b3.dd.api.model;

import java.util.Map;

public class AuthenticationResponseModel {

	private String token;
	private Boolean status;
	private String message;

	private Map<String, String> data;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "AuthenticationResponseModel [token=" + token + ", status=" + status + ", message=" + message + ", data="
				+ data + "]";
	}
	
}
