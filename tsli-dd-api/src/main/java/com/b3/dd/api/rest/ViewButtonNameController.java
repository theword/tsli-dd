package com.b3.dd.api.rest;

import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.ViewButtonName;
import com.b3.dd.api.repository.ViewButtonNameRepository;

@RestController
@RequestMapping("/${api.prefix}/utilities")
public class ViewButtonNameController {
	
	@Autowired
	private ViewButtonNameRepository ViewButtonNameRepository;
	
	@PostMapping("button-agent-name")
	public ResponseEntity<List<ViewButtonName>>
	getAgentPerformanceCurrentMonth()
		throws ResourceClosedException {
		List<ViewButtonName> ViewButtonName = ViewButtonNameRepository.findAll();

		return ResponseEntity.ok().body(ViewButtonName);
	}

}
