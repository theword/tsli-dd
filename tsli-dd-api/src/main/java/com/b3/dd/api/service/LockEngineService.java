package com.b3.dd.api.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Service;

import com.b3.dd.api.entity.dd.ClientDeviceLog;
import com.b3.dd.api.entity.dd.UserDevice;
import com.b3.dd.api.repository.ClientDeviceLogRepository;
import com.b3.dd.api.repository.UserDeviceRepository;

@Service
public class LockEngineService {

	public static enum DeviceMode {
		MOBILE, DESKTOP
	}

	private static final Logger logger = LogManager.getLogger(LockEngineService.class);
	@Value("${lockengine.license.max-device}")
	private Integer max;
	@Autowired
	private ClientDeviceLogRepository clientDeviceLogRepository;
	@Autowired
	private UserDeviceRepository userDeviceRepository;
	@Autowired
	private DefaultTokenServices tokenServices;

	@Transactional
	public Boolean validateDevice(String token, String deviceId, DeviceMode mode) {
		Boolean result = true;
		List<UserDevice> devices = null;
		OAuth2AccessToken oath2 = tokenServices.readAccessToken(token);
		String username = (String) oath2.getAdditionalInformation().get("agentCode");
		if ((devices = getUserDevices(username)) != null) {

			for (UserDevice device : devices) {
				if (device.getDeviceId().equals(deviceId)) {
					return true;
				}
			}
			logger.info("Device size: " + devices.size() + " is more than license: " + max);
			if (devices.size() > max) {
				return false;
			}
			saveDevice(username, deviceId, deviceId.substring(0, 10), mode);
		}

		return result;
	}

	@Transactional
	public ClientDeviceLog saveDeviceLog(String username, String token, String deviceId) {

		ClientDeviceLog log = null;
		if ((log = this.clientDeviceLogRepository.findByToken(token)) == null) {
			log = new ClientDeviceLog(token, deviceId);
			log.setCreateDate(new Date());
			log.setCreateBy(username);
		} else {
			log.setDeviceId(deviceId);
		}

		return this.clientDeviceLogRepository.save(log);

	}

	@Transactional
	public Boolean saveDevice(String username, String deviceId, String deviceName, DeviceMode deviceMode) {
		List<UserDevice> devices = getUserDevices(username);
		UserDevice userDevice;
		if( (userDevice=userDeviceRepository.findByDeviceIdAndUsername(deviceId, username)) == null ) {
			userDevice = new UserDevice(username, deviceId);
			userDevice.setCreateBy(username);
			userDevice.setCreateDate(new Date());
			userDevice.setDeviceType(deviceMode.toString());

			if (devices.size() > max) {
				return false;
			}

		}
		userDevice.setDeviceName(deviceName);
		userDeviceRepository.save(userDevice);
		return true;

	}


	public List<UserDevice> getDevices(String username) {
		return userDeviceRepository.findByUsername(username);
	}

	public void deleteDevice(Integer deviceId) {
		userDeviceRepository.deleteById(deviceId);
	}

	public List<UserDevice> getUserDevices(String username) {
		return userDeviceRepository.findByUsername(username);
	}

	public UserDevice getDeviceInfo(String username, String deviceId ) {

		UserDevice userDevice = userDeviceRepository.findByDeviceIdAndUsername(deviceId, username);

		return userDevice;
	}
}
