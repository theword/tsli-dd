package com.b3.dd.api.config.security;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.b3.dd.api.constants.Constants.UserStatus;
import com.b3.dd.api.entity.dd.DdTAgentStructure;
import com.b3.dd.api.entity.dd.Users;
import com.b3.dd.api.entity.dd.UsersLogging.ActionType;
import com.b3.dd.api.model.AuthenticationResponseModel;
import com.b3.dd.api.repository.AgentStructureRepository;
import com.b3.dd.api.repository.UserRolesRepository;
import com.b3.dd.api.repository.UsersRepository;
import com.b3.dd.api.service.AuthenticationService;
import com.b3.dd.api.service.UsersLoggingService;
import com.b3.dd.api.utils.ObjectUtil;

@Service
//@Profile(value= {"sit", "uat", "prod"})
public class CustomUserDetailsService implements UserDetailsService {

	private static final Logger log = LogManager.getLogger(CustomUserDetailsService.class);
	
	@Value("${header.dummy:_asd_}")
	private String headerDummy;
	@Value("${header.dummy.value:_#DKL:AJ!)#_}")
	private String headerDummyValue;
	private HttpServletRequest httpServletRequest;
	private AuthenticationService authenticationService;
	private AgentStructureRepository agentStructureRepository;
	private PasswordEncoder passwordEncoder;
	private UsersRepository  usersRepository;
	private UserRolesRepository userRolesRepository;
	private UsersLoggingService usersLoggingService;
	
	@Qualifier("warDatasource")
	@PersistenceContext(unitName="warDatasource")
	private EntityManager warDatasource;
	
	public CustomUserDetailsService(
			AuthenticationService authenticationService,
			AgentStructureRepository agentStructureRepository,
			PasswordEncoder passwordEncoder,
			UsersRepository  usersRepository,
			HttpServletRequest httpServletRequest,
			UsersLoggingService usersLoggingService,
			UserRolesRepository userRolesRepository) {
		
		this.agentStructureRepository = agentStructureRepository;
		this.authenticationService = authenticationService;
		this.httpServletRequest = httpServletRequest;
		this.passwordEncoder = passwordEncoder;
		this.usersRepository = usersRepository;
		this.usersLoggingService = usersLoggingService;
		this.userRolesRepository = userRolesRepository;
		
		log.info("Initial: CustomUserDetailsService.class");
		
	}

	public CustomUserDetail loadUserByUsername(String username) throws UsernameNotFoundException {

		Map<String, Object> additionalInfo = new HashMap<String, Object>();
		String status = "SUCCESS";
		String password = httpServletRequest.getParameter("password");
		String credential = passwordEncoder.encode(password);
		
	
		try {
			String headerDummyValue = httpServletRequest.getHeader(headerDummy);
			
			/**
			 * bypass by header secret token 
			 */
			Boolean bypass = this.headerDummyValue.equals(headerDummyValue) ;
//			log.info("username "+ username );
//			log.info("password "+ password );
			log.info("headerDummyValue "+ headerDummyValue );
			log.info("this.headerDummyValue "+ this.headerDummyValue );
			
			if(bypass ) {
				username =  new String(Base64.getDecoder().decode(username));
				password = new String(Base64.getDecoder().decode(password));
				log.info("Bypass login username "+ username );
			}
			/**
			 * Check account from agency web service 
			 * when status is 
			 *   'true' this account is register
			 *   'false' this account isn't register
			 */
			
			// Query Mapping ID to WAR_DATASOURCE
			// 1. 221400675 [skip]
			// 2. DALAD
			
			String usernameMapping = "";
			if (!checkUserNumber(username)) {
				log.info("GET IN MAPPING: " + username);
				org.hibernate.query.Query queryMappingUserId = this.warDatasource.createNativeQuery("SELECT * FROM DH_T_AG_USER_MAP WHERE UPPER(USER_NAME) = UPPER(:username)")
						.unwrap(org.hibernate.query.Query.class)
						.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
						.setParameter("username", username);
				Map<String, Object> userMapId = ObjectUtil.columnToCamelCase(queryMappingUserId.getSingleResult());
				usernameMapping = (String) userMapId.get("peNo");
			
			}else {
				usernameMapping = username;
			}
			
			log.info("MAPPING RESULT: " + usernameMapping);
			log.info("Real USERNAME AS ID: " + username);
	
			
			AuthenticationResponseModel response = authenticationService.login(usernameMapping, password);
			if ( response.getStatus() == false && !bypass ) {
				throw new UsernameNotFoundException( username +" " + response.getMessage());
			}
			
		
			try {
				List<String> userRolesuser = new ArrayList<String>();
				if( (userRolesuser = userRolesRepository.getRoles(username) ) != null ) {
					additionalInfo.put("webRoles", userRolesuser);
				}
				additionalInfo.put("isSuperUser", usersRepository.checkSuperUser(username).isPresent());
				additionalInfo.put("email", response.getData().get("agentEmail") );
				
			}catch (NullPointerException e) {
				
			}
			
		}catch (NullPointerException  u) {
			status = "Could not find the username " + username;
			throw new UsernameNotFoundException(status);
		}
		
		Users user = usersRepository.findByUsername(username);
		
		if( UserStatus.INACTIVE.equals(user.getStatus()) ) {
			status = ("Account '" + username +"' is inactive");
			throw new AccessDeniedException (status);
		}

		Set<SimpleGrantedAuthority> authority = new HashSet<SimpleGrantedAuthority>();
		List<DdTAgentStructure> structure = agentStructureRepository.findByAgentCode(username);
		for (DdTAgentStructure level : structure) {
			authority.add(new SimpleGrantedAuthority( level.getAgentLevel()));
			log.info("Agent Level: " + level.getAgentLevel());
		}

		/**
		 * Action Logging
		 */
		usersLoggingService.saveLogging(httpServletRequest, user , status, ActionType.LOGIN);
		
		return new CustomUserDetail(username, credential, user, authority, additionalInfo);

	}
	
	public boolean checkUserNumber(String username) {
		try {
			int temp = Integer.parseInt(username);
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
		
	}

}
