package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CLIENT_DEVICE_LOG", catalog = "", schema = "", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "token" }) })
@XmlRootElement
public class ClientDeviceLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TOKEN_ID", nullable = false)
	private Integer tokenId;
	@Column(name = "TOKEN", nullable = false, length = 1024)
	private String token;
	@Column(name = "DEVICE_ID", nullable = false, length = 256)
	private String deviceId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false)
	private Date createDate;
	@Column(name = "CREATE_BY", nullable = false, length = 256)
	private String createBy;

	public ClientDeviceLog() {
		super();
	}

	public ClientDeviceLog(Integer tokenId) {
		super();
		this.tokenId = tokenId;
	}

	public ClientDeviceLog(String token, String deviceId) {
		super();
		this.token = token;
		this.deviceId = deviceId;
	}

	public Integer getTokenId() {
		return tokenId;
	}

	public void setTokenId(Integer tokenId) {
		this.tokenId = tokenId;
	}

	public Serializable getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (tokenId != null ? tokenId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof OauthClientDetails)) {
			return false;
		}
		ClientDeviceLog other = (ClientDeviceLog) object;
		if ((this.tokenId == null && other.tokenId != null)
				|| (this.tokenId != null && !this.tokenId.equals(other.tokenId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.tsli.dd.entity.ClientDeviceLog[ tokenId=" + tokenId + " ]";
	}
}
