package com.b3.dd.api.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserHelper {


	@Autowired
	private  TokenStore tokenStore;
	/**
	 * Get the current logged in user model.
	 * @return UserLoggedIn model of current user
	 */
	public  OAuth2AuthenticationDetails getAuthenticationDetails(){
		// TODO: 14-12-2019 get current user authenticated
		if(!isAuthenticated()){
			return null;
		}
		return (OAuth2AuthenticationDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
	}

	/**
	 * Return authentication status.
	 * @return Boolean true is authenticated
	 */
	public   Boolean isAuthenticated(){
		return SecurityContextHolder.getContext().getAuthentication() != null && !SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser");
	}


	public  String getUserLoggedInName() {

		if(!isAuthenticated()){
			return null;
		}

		final String userToken = (getAuthenticationDetails()).getTokenValue();
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(userToken);

		return (String) Objects.requireNonNull(accessToken).getAdditionalInformation().get("agentCode");
	}
	
	public  String getName() {

		if(!isAuthenticated()){
			return null;
		}

		final String userToken = (getAuthenticationDetails()).getTokenValue();
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(userToken);

		return (String) Objects.requireNonNull(accessToken).getAdditionalInformation().get("name");
	}

	public  String getUserPosition() {

		if(!isAuthenticated()){
			return null;
		}

		final String userToken = (getAuthenticationDetails()).getTokenValue();
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(userToken);

		return Objects.requireNonNull(accessToken).getAdditionalInformation().get("role").toString();

	}



}