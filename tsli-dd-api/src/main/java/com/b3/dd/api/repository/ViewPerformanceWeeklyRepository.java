package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewPerformanceWeekly;

@Repository
public interface ViewPerformanceWeeklyRepository extends CrudRepository<ViewPerformanceWeekly, Integer> {

	@Query("SELECT v FROM ViewPerformanceWeekly v WHERE (:authLevel = 'ZONE')"
			+ "	ORDER BY"
			+ "	CASE"
			+ "		WHEN v.agentLevel = 'CHANNEL' THEN 1"
			+ "		WHEN v.agentLevel = 'COMPANY' THEN 2"
			+ "		ELSE 0"
			+ "	END, v.zone")
	public List<ViewPerformanceWeekly> findByCompany(
			@Param("authLevel") String authLevel);
	
	@Query("SELECT v FROM ViewPerformanceWeekly v WHERE v.groupChannel = :channelCode"
			+ " AND (:authLevel = 'ZONE')"
			+ "	ORDER BY "
			+ "	CASE"
			+ "		WHEN v.agentLevel = 'CHANNEL' THEN 1"
			+ "		ELSE 0"
			+ "	END, v.zone")
	public List<ViewPerformanceWeekly> findByChannel(
			@Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel);
	
	@Query("SELECT v FROM ViewPerformanceWeekly v WHERE v.zone = :zoneCode"
			+ " AND (:authLevel = 'ZONE')")
	public List<ViewPerformanceWeekly> findByZone(
			@Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
}
