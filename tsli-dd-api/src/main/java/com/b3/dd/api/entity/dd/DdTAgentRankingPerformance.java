/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "DD_T_AGENT_RANKING_PERFORMANCE", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DdTAgentRankingPerformance.findAll", query = "SELECT d FROM DdTAgentRankingPerformance d")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findById", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.id = :id")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByClosYm", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.closYm = :closYm")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByKpiLevel", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.kpiLevel = :kpiLevel")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByCode", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.code = :code")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByCodeDescription", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.codeDescription = :codeDescription")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue1", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue1 = :rankKpiValue1")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue2", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue2 = :rankKpiValue2")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue3", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue3 = :rankKpiValue3")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue4", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue4 = :rankKpiValue4")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue5", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue5 = :rankKpiValue5")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue6", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue6 = :rankKpiValue6")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue7", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue7 = :rankKpiValue7")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue8", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue8 = :rankKpiValue8")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue9", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue9 = :rankKpiValue9")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue10", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue10 = :rankKpiValue10")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue11", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue11 = :rankKpiValue11")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue12", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue12 = :rankKpiValue12")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue13", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue13 = :rankKpiValue13")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue14", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue14 = :rankKpiValue14")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue15", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue15 = :rankKpiValue15")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue16", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue16 = :rankKpiValue16")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue17", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue17 = :rankKpiValue17")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue18", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue18 = :rankKpiValue18")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue19", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue19 = :rankKpiValue19")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue20", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue20 = :rankKpiValue20")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue21", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue21 = :rankKpiValue21")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue22", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue22 = :rankKpiValue22")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue23", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue23 = :rankKpiValue23")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue24", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue24 = :rankKpiValue24")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue25", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue25 = :rankKpiValue25")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue26", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue26 = :rankKpiValue26")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue27", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue27 = :rankKpiValue27")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue28", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue28 = :rankKpiValue28")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue29", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue29 = :rankKpiValue29")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue30", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue30 = :rankKpiValue30")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue31", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue31 = :rankKpiValue31")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue32", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue32 = :rankKpiValue32")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue33", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue33 = :rankKpiValue33")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue34", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue34 = :rankKpiValue34")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue35", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue35 = :rankKpiValue35")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue36", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue36 = :rankKpiValue36")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue37", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue37 = :rankKpiValue37")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue38", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue38 = :rankKpiValue38")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue39", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue39 = :rankKpiValue39")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue40", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue40 = :rankKpiValue40")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue1", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue1 = :sumKpiValue1")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue2", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue2 = :sumKpiValue2")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue3", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue3 = :sumKpiValue3")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue4", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue4 = :sumKpiValue4")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue5", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue5 = :sumKpiValue5")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue6", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue6 = :sumKpiValue6")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue7", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue7 = :sumKpiValue7")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue8", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue8 = :sumKpiValue8")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue9", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue9 = :sumKpiValue9")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue10", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue10 = :sumKpiValue10")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue11", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue11 = :sumKpiValue11")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue12", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue12 = :sumKpiValue12")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue13", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue13 = :sumKpiValue13")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue14", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue14 = :sumKpiValue14")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue15", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue15 = :sumKpiValue15")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue16", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue16 = :sumKpiValue16")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue17", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue17 = :sumKpiValue17")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue18", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue18 = :sumKpiValue18")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue19", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue19 = :sumKpiValue19")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue20", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue20 = :sumKpiValue20")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue21", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue21 = :sumKpiValue21")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue22", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue22 = :sumKpiValue22")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue23", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue23 = :sumKpiValue23")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue24", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue24 = :sumKpiValue24")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue25", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue25 = :sumKpiValue25")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue26", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue26 = :sumKpiValue26")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue27", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue27 = :sumKpiValue27")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue28", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue28 = :sumKpiValue28")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue29", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue29 = :sumKpiValue29")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue30", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue30 = :sumKpiValue30")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue31", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue31 = :sumKpiValue31")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue32", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue32 = :sumKpiValue32")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue33", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue33 = :sumKpiValue33")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue34", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue34 = :sumKpiValue34")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue35", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue35 = :sumKpiValue35")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue36", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue36 = :sumKpiValue36")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue37", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue37 = :sumKpiValue37")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue38", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue38 = :sumKpiValue38")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue39", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue39 = :sumKpiValue39")
    , @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue40", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue40 = :sumKpiValue40")})
public class DdTAgentRankingPerformance implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "CLOS_YM", length = 18)
    private String closYm;
    @Column(name = "KPI_LEVEL", length = 18)
    private String kpiLevel;
    @Column(name = "CODE", length = 4)
    private String code;
    @Column(name = "CODE_DESCRIPTION")
    private Character codeDescription;
    @Column(name = "RANK_KPI_VALUE_1")
    private BigInteger rankKpiValue1;
    @Column(name = "RANK_KPI_VALUE_2")
    private BigInteger rankKpiValue2;
    @Column(name = "RANK_KPI_VALUE_3")
    private BigInteger rankKpiValue3;
    @Column(name = "RANK_KPI_VALUE_4")
    private BigInteger rankKpiValue4;
    @Column(name = "RANK_KPI_VALUE_5")
    private BigInteger rankKpiValue5;
    @Column(name = "RANK_KPI_VALUE_6")
    private BigInteger rankKpiValue6;
    @Column(name = "RANK_KPI_VALUE_7")
    private BigInteger rankKpiValue7;
    @Column(name = "RANK_KPI_VALUE_8")
    private BigInteger rankKpiValue8;
    @Column(name = "RANK_KPI_VALUE_9")
    private BigInteger rankKpiValue9;
    @Column(name = "RANK_KPI_VALUE_10")
    private BigInteger rankKpiValue10;
    @Column(name = "RANK_KPI_VALUE_11")
    private BigInteger rankKpiValue11;
    @Column(name = "RANK_KPI_VALUE_12")
    private BigInteger rankKpiValue12;
    @Column(name = "RANK_KPI_VALUE_13")
    private BigInteger rankKpiValue13;
    @Column(name = "RANK_KPI_VALUE_14")
    private BigInteger rankKpiValue14;
    @Column(name = "RANK_KPI_VALUE_15")
    private BigInteger rankKpiValue15;
    @Column(name = "RANK_KPI_VALUE_16")
    private BigInteger rankKpiValue16;
    @Column(name = "RANK_KPI_VALUE_17")
    private BigInteger rankKpiValue17;
    @Column(name = "RANK_KPI_VALUE_18")
    private BigInteger rankKpiValue18;
    @Column(name = "RANK_KPI_VALUE_19")
    private BigInteger rankKpiValue19;
    @Column(name = "RANK_KPI_VALUE_20")
    private BigInteger rankKpiValue20;
    @Column(name = "RANK_KPI_VALUE_21")
    private BigInteger rankKpiValue21;
    @Column(name = "RANK_KPI_VALUE_22")
    private BigInteger rankKpiValue22;
    @Column(name = "RANK_KPI_VALUE_23")
    private BigInteger rankKpiValue23;
    @Column(name = "RANK_KPI_VALUE_24")
    private BigInteger rankKpiValue24;
    @Column(name = "RANK_KPI_VALUE_25")
    private BigInteger rankKpiValue25;
    @Column(name = "RANK_KPI_VALUE_26")
    private BigInteger rankKpiValue26;
    @Column(name = "RANK_KPI_VALUE_27")
    private BigInteger rankKpiValue27;
    @Column(name = "RANK_KPI_VALUE_28")
    private BigInteger rankKpiValue28;
    @Column(name = "RANK_KPI_VALUE_29")
    private BigInteger rankKpiValue29;
    @Column(name = "RANK_KPI_VALUE_30")
    private BigInteger rankKpiValue30;
    @Column(name = "RANK_KPI_VALUE_31")
    private BigInteger rankKpiValue31;
    @Column(name = "RANK_KPI_VALUE_32")
    private BigInteger rankKpiValue32;
    @Column(name = "RANK_KPI_VALUE_33")
    private BigInteger rankKpiValue33;
    @Column(name = "RANK_KPI_VALUE_34")
    private BigInteger rankKpiValue34;
    @Column(name = "RANK_KPI_VALUE_35")
    private BigInteger rankKpiValue35;
    @Column(name = "RANK_KPI_VALUE_36")
    private BigInteger rankKpiValue36;
    @Column(name = "RANK_KPI_VALUE_37")
    private BigInteger rankKpiValue37;
    @Column(name = "RANK_KPI_VALUE_38")
    private BigInteger rankKpiValue38;
    @Column(name = "RANK_KPI_VALUE_39")
    private BigInteger rankKpiValue39;
    @Column(name = "RANK_KPI_VALUE_40")
    private BigInteger rankKpiValue40;
    @Column(name = "SUM_KPI_VALUE_1")
    private BigInteger sumKpiValue1;
    @Column(name = "SUM_KPI_VALUE_2")
    private BigInteger sumKpiValue2;
    @Column(name = "SUM_KPI_VALUE_3")
    private BigInteger sumKpiValue3;
    @Column(name = "SUM_KPI_VALUE_4")
    private BigInteger sumKpiValue4;
    @Column(name = "SUM_KPI_VALUE_5")
    private BigInteger sumKpiValue5;
    @Column(name = "SUM_KPI_VALUE_6")
    private BigInteger sumKpiValue6;
    @Column(name = "SUM_KPI_VALUE_7")
    private BigInteger sumKpiValue7;
    @Column(name = "SUM_KPI_VALUE_8")
    private BigInteger sumKpiValue8;
    @Column(name = "SUM_KPI_VALUE_9")
    private BigInteger sumKpiValue9;
    @Column(name = "SUM_KPI_VALUE_10")
    private BigInteger sumKpiValue10;
    @Column(name = "SUM_KPI_VALUE_11")
    private BigInteger sumKpiValue11;
    @Column(name = "SUM_KPI_VALUE_12")
    private BigInteger sumKpiValue12;
    @Column(name = "SUM_KPI_VALUE_13")
    private BigInteger sumKpiValue13;
    @Column(name = "SUM_KPI_VALUE_14")
    private BigInteger sumKpiValue14;
    @Column(name = "SUM_KPI_VALUE_15")
    private BigInteger sumKpiValue15;
    @Column(name = "SUM_KPI_VALUE_16")
    private BigInteger sumKpiValue16;
    @Column(name = "SUM_KPI_VALUE_17")
    private BigInteger sumKpiValue17;
    @Column(name = "SUM_KPI_VALUE_18")
    private BigInteger sumKpiValue18;
    @Column(name = "SUM_KPI_VALUE_19")
    private BigInteger sumKpiValue19;
    @Column(name = "SUM_KPI_VALUE_20")
    private BigInteger sumKpiValue20;
    @Column(name = "SUM_KPI_VALUE_21")
    private BigInteger sumKpiValue21;
    @Column(name = "SUM_KPI_VALUE_22")
    private BigInteger sumKpiValue22;
    @Column(name = "SUM_KPI_VALUE_23")
    private BigInteger sumKpiValue23;
    @Column(name = "SUM_KPI_VALUE_24")
    private BigInteger sumKpiValue24;
    @Column(name = "SUM_KPI_VALUE_25")
    private BigInteger sumKpiValue25;
    @Column(name = "SUM_KPI_VALUE_26")
    private BigInteger sumKpiValue26;
    @Column(name = "SUM_KPI_VALUE_27")
    private BigInteger sumKpiValue27;
    @Column(name = "SUM_KPI_VALUE_28")
    private BigInteger sumKpiValue28;
    @Column(name = "SUM_KPI_VALUE_29")
    private BigInteger sumKpiValue29;
    @Column(name = "SUM_KPI_VALUE_30")
    private BigInteger sumKpiValue30;
    @Column(name = "SUM_KPI_VALUE_31")
    private BigInteger sumKpiValue31;
    @Column(name = "SUM_KPI_VALUE_32")
    private BigInteger sumKpiValue32;
    @Column(name = "SUM_KPI_VALUE_33")
    private BigInteger sumKpiValue33;
    @Column(name = "SUM_KPI_VALUE_34")
    private BigInteger sumKpiValue34;
    @Column(name = "SUM_KPI_VALUE_35")
    private BigInteger sumKpiValue35;
    @Column(name = "SUM_KPI_VALUE_36")
    private BigInteger sumKpiValue36;
    @Column(name = "SUM_KPI_VALUE_37")
    private BigInteger sumKpiValue37;
    @Column(name = "SUM_KPI_VALUE_38")
    private BigInteger sumKpiValue38;
    @Column(name = "SUM_KPI_VALUE_39")
    private BigInteger sumKpiValue39;
    @Column(name = "SUM_KPI_VALUE_40")
    private BigInteger sumKpiValue40;

    public DdTAgentRankingPerformance() {
    }

    public DdTAgentRankingPerformance(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClosYm() {
        return closYm;
    }

    public void setClosYm(String closYm) {
        this.closYm = closYm;
    }

    public String getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(String kpiLevel) {
        this.kpiLevel = kpiLevel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Character getCodeDescription() {
        return codeDescription;
    }

    public void setCodeDescription(Character codeDescription) {
        this.codeDescription = codeDescription;
    }

    public BigInteger getRankKpiValue1() {
        return rankKpiValue1;
    }

    public void setRankKpiValue1(BigInteger rankKpiValue1) {
        this.rankKpiValue1 = rankKpiValue1;
    }

    public BigInteger getRankKpiValue2() {
        return rankKpiValue2;
    }

    public void setRankKpiValue2(BigInteger rankKpiValue2) {
        this.rankKpiValue2 = rankKpiValue2;
    }

    public BigInteger getRankKpiValue3() {
        return rankKpiValue3;
    }

    public void setRankKpiValue3(BigInteger rankKpiValue3) {
        this.rankKpiValue3 = rankKpiValue3;
    }

    public BigInteger getRankKpiValue4() {
        return rankKpiValue4;
    }

    public void setRankKpiValue4(BigInteger rankKpiValue4) {
        this.rankKpiValue4 = rankKpiValue4;
    }

    public BigInteger getRankKpiValue5() {
        return rankKpiValue5;
    }

    public void setRankKpiValue5(BigInteger rankKpiValue5) {
        this.rankKpiValue5 = rankKpiValue5;
    }

    public BigInteger getRankKpiValue6() {
        return rankKpiValue6;
    }

    public void setRankKpiValue6(BigInteger rankKpiValue6) {
        this.rankKpiValue6 = rankKpiValue6;
    }

    public BigInteger getRankKpiValue7() {
        return rankKpiValue7;
    }

    public void setRankKpiValue7(BigInteger rankKpiValue7) {
        this.rankKpiValue7 = rankKpiValue7;
    }

    public BigInteger getRankKpiValue8() {
        return rankKpiValue8;
    }

    public void setRankKpiValue8(BigInteger rankKpiValue8) {
        this.rankKpiValue8 = rankKpiValue8;
    }

    public BigInteger getRankKpiValue9() {
        return rankKpiValue9;
    }

    public void setRankKpiValue9(BigInteger rankKpiValue9) {
        this.rankKpiValue9 = rankKpiValue9;
    }

    public BigInteger getRankKpiValue10() {
        return rankKpiValue10;
    }

    public void setRankKpiValue10(BigInteger rankKpiValue10) {
        this.rankKpiValue10 = rankKpiValue10;
    }

    public BigInteger getRankKpiValue11() {
        return rankKpiValue11;
    }

    public void setRankKpiValue11(BigInteger rankKpiValue11) {
        this.rankKpiValue11 = rankKpiValue11;
    }

    public BigInteger getRankKpiValue12() {
        return rankKpiValue12;
    }

    public void setRankKpiValue12(BigInteger rankKpiValue12) {
        this.rankKpiValue12 = rankKpiValue12;
    }

    public BigInteger getRankKpiValue13() {
        return rankKpiValue13;
    }

    public void setRankKpiValue13(BigInteger rankKpiValue13) {
        this.rankKpiValue13 = rankKpiValue13;
    }

    public BigInteger getRankKpiValue14() {
        return rankKpiValue14;
    }

    public void setRankKpiValue14(BigInteger rankKpiValue14) {
        this.rankKpiValue14 = rankKpiValue14;
    }

    public BigInteger getRankKpiValue15() {
        return rankKpiValue15;
    }

    public void setRankKpiValue15(BigInteger rankKpiValue15) {
        this.rankKpiValue15 = rankKpiValue15;
    }

    public BigInteger getRankKpiValue16() {
        return rankKpiValue16;
    }

    public void setRankKpiValue16(BigInteger rankKpiValue16) {
        this.rankKpiValue16 = rankKpiValue16;
    }

    public BigInteger getRankKpiValue17() {
        return rankKpiValue17;
    }

    public void setRankKpiValue17(BigInteger rankKpiValue17) {
        this.rankKpiValue17 = rankKpiValue17;
    }

    public BigInteger getRankKpiValue18() {
        return rankKpiValue18;
    }

    public void setRankKpiValue18(BigInteger rankKpiValue18) {
        this.rankKpiValue18 = rankKpiValue18;
    }

    public BigInteger getRankKpiValue19() {
        return rankKpiValue19;
    }

    public void setRankKpiValue19(BigInteger rankKpiValue19) {
        this.rankKpiValue19 = rankKpiValue19;
    }

    public BigInteger getRankKpiValue20() {
        return rankKpiValue20;
    }

    public void setRankKpiValue20(BigInteger rankKpiValue20) {
        this.rankKpiValue20 = rankKpiValue20;
    }

    public BigInteger getRankKpiValue21() {
        return rankKpiValue21;
    }

    public void setRankKpiValue21(BigInteger rankKpiValue21) {
        this.rankKpiValue21 = rankKpiValue21;
    }

    public BigInteger getRankKpiValue22() {
        return rankKpiValue22;
    }

    public void setRankKpiValue22(BigInteger rankKpiValue22) {
        this.rankKpiValue22 = rankKpiValue22;
    }

    public BigInteger getRankKpiValue23() {
        return rankKpiValue23;
    }

    public void setRankKpiValue23(BigInteger rankKpiValue23) {
        this.rankKpiValue23 = rankKpiValue23;
    }

    public BigInteger getRankKpiValue24() {
        return rankKpiValue24;
    }

    public void setRankKpiValue24(BigInteger rankKpiValue24) {
        this.rankKpiValue24 = rankKpiValue24;
    }

    public BigInteger getRankKpiValue25() {
        return rankKpiValue25;
    }

    public void setRankKpiValue25(BigInteger rankKpiValue25) {
        this.rankKpiValue25 = rankKpiValue25;
    }

    public BigInteger getRankKpiValue26() {
        return rankKpiValue26;
    }

    public void setRankKpiValue26(BigInteger rankKpiValue26) {
        this.rankKpiValue26 = rankKpiValue26;
    }

    public BigInteger getRankKpiValue27() {
        return rankKpiValue27;
    }

    public void setRankKpiValue27(BigInteger rankKpiValue27) {
        this.rankKpiValue27 = rankKpiValue27;
    }

    public BigInteger getRankKpiValue28() {
        return rankKpiValue28;
    }

    public void setRankKpiValue28(BigInteger rankKpiValue28) {
        this.rankKpiValue28 = rankKpiValue28;
    }

    public BigInteger getRankKpiValue29() {
        return rankKpiValue29;
    }

    public void setRankKpiValue29(BigInteger rankKpiValue29) {
        this.rankKpiValue29 = rankKpiValue29;
    }

    public BigInteger getRankKpiValue30() {
        return rankKpiValue30;
    }

    public void setRankKpiValue30(BigInteger rankKpiValue30) {
        this.rankKpiValue30 = rankKpiValue30;
    }

    public BigInteger getRankKpiValue31() {
        return rankKpiValue31;
    }

    public void setRankKpiValue31(BigInteger rankKpiValue31) {
        this.rankKpiValue31 = rankKpiValue31;
    }

    public BigInteger getRankKpiValue32() {
        return rankKpiValue32;
    }

    public void setRankKpiValue32(BigInteger rankKpiValue32) {
        this.rankKpiValue32 = rankKpiValue32;
    }

    public BigInteger getRankKpiValue33() {
        return rankKpiValue33;
    }

    public void setRankKpiValue33(BigInteger rankKpiValue33) {
        this.rankKpiValue33 = rankKpiValue33;
    }

    public BigInteger getRankKpiValue34() {
        return rankKpiValue34;
    }

    public void setRankKpiValue34(BigInteger rankKpiValue34) {
        this.rankKpiValue34 = rankKpiValue34;
    }

    public BigInteger getRankKpiValue35() {
        return rankKpiValue35;
    }

    public void setRankKpiValue35(BigInteger rankKpiValue35) {
        this.rankKpiValue35 = rankKpiValue35;
    }

    public BigInteger getRankKpiValue36() {
        return rankKpiValue36;
    }

    public void setRankKpiValue36(BigInteger rankKpiValue36) {
        this.rankKpiValue36 = rankKpiValue36;
    }

    public BigInteger getRankKpiValue37() {
        return rankKpiValue37;
    }

    public void setRankKpiValue37(BigInteger rankKpiValue37) {
        this.rankKpiValue37 = rankKpiValue37;
    }

    public BigInteger getRankKpiValue38() {
        return rankKpiValue38;
    }

    public void setRankKpiValue38(BigInteger rankKpiValue38) {
        this.rankKpiValue38 = rankKpiValue38;
    }

    public BigInteger getRankKpiValue39() {
        return rankKpiValue39;
    }

    public void setRankKpiValue39(BigInteger rankKpiValue39) {
        this.rankKpiValue39 = rankKpiValue39;
    }

    public BigInteger getRankKpiValue40() {
        return rankKpiValue40;
    }

    public void setRankKpiValue40(BigInteger rankKpiValue40) {
        this.rankKpiValue40 = rankKpiValue40;
    }

    public BigInteger getSumKpiValue1() {
        return sumKpiValue1;
    }

    public void setSumKpiValue1(BigInteger sumKpiValue1) {
        this.sumKpiValue1 = sumKpiValue1;
    }

    public BigInteger getSumKpiValue2() {
        return sumKpiValue2;
    }

    public void setSumKpiValue2(BigInteger sumKpiValue2) {
        this.sumKpiValue2 = sumKpiValue2;
    }

    public BigInteger getSumKpiValue3() {
        return sumKpiValue3;
    }

    public void setSumKpiValue3(BigInteger sumKpiValue3) {
        this.sumKpiValue3 = sumKpiValue3;
    }

    public BigInteger getSumKpiValue4() {
        return sumKpiValue4;
    }

    public void setSumKpiValue4(BigInteger sumKpiValue4) {
        this.sumKpiValue4 = sumKpiValue4;
    }

    public BigInteger getSumKpiValue5() {
        return sumKpiValue5;
    }

    public void setSumKpiValue5(BigInteger sumKpiValue5) {
        this.sumKpiValue5 = sumKpiValue5;
    }

    public BigInteger getSumKpiValue6() {
        return sumKpiValue6;
    }

    public void setSumKpiValue6(BigInteger sumKpiValue6) {
        this.sumKpiValue6 = sumKpiValue6;
    }

    public BigInteger getSumKpiValue7() {
        return sumKpiValue7;
    }

    public void setSumKpiValue7(BigInteger sumKpiValue7) {
        this.sumKpiValue7 = sumKpiValue7;
    }

    public BigInteger getSumKpiValue8() {
        return sumKpiValue8;
    }

    public void setSumKpiValue8(BigInteger sumKpiValue8) {
        this.sumKpiValue8 = sumKpiValue8;
    }

    public BigInteger getSumKpiValue9() {
        return sumKpiValue9;
    }

    public void setSumKpiValue9(BigInteger sumKpiValue9) {
        this.sumKpiValue9 = sumKpiValue9;
    }

    public BigInteger getSumKpiValue10() {
        return sumKpiValue10;
    }

    public void setSumKpiValue10(BigInteger sumKpiValue10) {
        this.sumKpiValue10 = sumKpiValue10;
    }

    public BigInteger getSumKpiValue11() {
        return sumKpiValue11;
    }

    public void setSumKpiValue11(BigInteger sumKpiValue11) {
        this.sumKpiValue11 = sumKpiValue11;
    }

    public BigInteger getSumKpiValue12() {
        return sumKpiValue12;
    }

    public void setSumKpiValue12(BigInteger sumKpiValue12) {
        this.sumKpiValue12 = sumKpiValue12;
    }

    public BigInteger getSumKpiValue13() {
        return sumKpiValue13;
    }

    public void setSumKpiValue13(BigInteger sumKpiValue13) {
        this.sumKpiValue13 = sumKpiValue13;
    }

    public BigInteger getSumKpiValue14() {
        return sumKpiValue14;
    }

    public void setSumKpiValue14(BigInteger sumKpiValue14) {
        this.sumKpiValue14 = sumKpiValue14;
    }

    public BigInteger getSumKpiValue15() {
        return sumKpiValue15;
    }

    public void setSumKpiValue15(BigInteger sumKpiValue15) {
        this.sumKpiValue15 = sumKpiValue15;
    }

    public BigInteger getSumKpiValue16() {
        return sumKpiValue16;
    }

    public void setSumKpiValue16(BigInteger sumKpiValue16) {
        this.sumKpiValue16 = sumKpiValue16;
    }

    public BigInteger getSumKpiValue17() {
        return sumKpiValue17;
    }

    public void setSumKpiValue17(BigInteger sumKpiValue17) {
        this.sumKpiValue17 = sumKpiValue17;
    }

    public BigInteger getSumKpiValue18() {
        return sumKpiValue18;
    }

    public void setSumKpiValue18(BigInteger sumKpiValue18) {
        this.sumKpiValue18 = sumKpiValue18;
    }

    public BigInteger getSumKpiValue19() {
        return sumKpiValue19;
    }

    public void setSumKpiValue19(BigInteger sumKpiValue19) {
        this.sumKpiValue19 = sumKpiValue19;
    }

    public BigInteger getSumKpiValue20() {
        return sumKpiValue20;
    }

    public void setSumKpiValue20(BigInteger sumKpiValue20) {
        this.sumKpiValue20 = sumKpiValue20;
    }

    public BigInteger getSumKpiValue21() {
        return sumKpiValue21;
    }

    public void setSumKpiValue21(BigInteger sumKpiValue21) {
        this.sumKpiValue21 = sumKpiValue21;
    }

    public BigInteger getSumKpiValue22() {
        return sumKpiValue22;
    }

    public void setSumKpiValue22(BigInteger sumKpiValue22) {
        this.sumKpiValue22 = sumKpiValue22;
    }

    public BigInteger getSumKpiValue23() {
        return sumKpiValue23;
    }

    public void setSumKpiValue23(BigInteger sumKpiValue23) {
        this.sumKpiValue23 = sumKpiValue23;
    }

    public BigInteger getSumKpiValue24() {
        return sumKpiValue24;
    }

    public void setSumKpiValue24(BigInteger sumKpiValue24) {
        this.sumKpiValue24 = sumKpiValue24;
    }

    public BigInteger getSumKpiValue25() {
        return sumKpiValue25;
    }

    public void setSumKpiValue25(BigInteger sumKpiValue25) {
        this.sumKpiValue25 = sumKpiValue25;
    }

    public BigInteger getSumKpiValue26() {
        return sumKpiValue26;
    }

    public void setSumKpiValue26(BigInteger sumKpiValue26) {
        this.sumKpiValue26 = sumKpiValue26;
    }

    public BigInteger getSumKpiValue27() {
        return sumKpiValue27;
    }

    public void setSumKpiValue27(BigInteger sumKpiValue27) {
        this.sumKpiValue27 = sumKpiValue27;
    }

    public BigInteger getSumKpiValue28() {
        return sumKpiValue28;
    }

    public void setSumKpiValue28(BigInteger sumKpiValue28) {
        this.sumKpiValue28 = sumKpiValue28;
    }

    public BigInteger getSumKpiValue29() {
        return sumKpiValue29;
    }

    public void setSumKpiValue29(BigInteger sumKpiValue29) {
        this.sumKpiValue29 = sumKpiValue29;
    }

    public BigInteger getSumKpiValue30() {
        return sumKpiValue30;
    }

    public void setSumKpiValue30(BigInteger sumKpiValue30) {
        this.sumKpiValue30 = sumKpiValue30;
    }

    public BigInteger getSumKpiValue31() {
        return sumKpiValue31;
    }

    public void setSumKpiValue31(BigInteger sumKpiValue31) {
        this.sumKpiValue31 = sumKpiValue31;
    }

    public BigInteger getSumKpiValue32() {
        return sumKpiValue32;
    }

    public void setSumKpiValue32(BigInteger sumKpiValue32) {
        this.sumKpiValue32 = sumKpiValue32;
    }

    public BigInteger getSumKpiValue33() {
        return sumKpiValue33;
    }

    public void setSumKpiValue33(BigInteger sumKpiValue33) {
        this.sumKpiValue33 = sumKpiValue33;
    }

    public BigInteger getSumKpiValue34() {
        return sumKpiValue34;
    }

    public void setSumKpiValue34(BigInteger sumKpiValue34) {
        this.sumKpiValue34 = sumKpiValue34;
    }

    public BigInteger getSumKpiValue35() {
        return sumKpiValue35;
    }

    public void setSumKpiValue35(BigInteger sumKpiValue35) {
        this.sumKpiValue35 = sumKpiValue35;
    }

    public BigInteger getSumKpiValue36() {
        return sumKpiValue36;
    }

    public void setSumKpiValue36(BigInteger sumKpiValue36) {
        this.sumKpiValue36 = sumKpiValue36;
    }

    public BigInteger getSumKpiValue37() {
        return sumKpiValue37;
    }

    public void setSumKpiValue37(BigInteger sumKpiValue37) {
        this.sumKpiValue37 = sumKpiValue37;
    }

    public BigInteger getSumKpiValue38() {
        return sumKpiValue38;
    }

    public void setSumKpiValue38(BigInteger sumKpiValue38) {
        this.sumKpiValue38 = sumKpiValue38;
    }

    public BigInteger getSumKpiValue39() {
        return sumKpiValue39;
    }

    public void setSumKpiValue39(BigInteger sumKpiValue39) {
        this.sumKpiValue39 = sumKpiValue39;
    }

    public BigInteger getSumKpiValue40() {
        return sumKpiValue40;
    }

    public void setSumKpiValue40(BigInteger sumKpiValue40) {
        this.sumKpiValue40 = sumKpiValue40;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTAgentRankingPerformance)) {
            return false;
        }
        DdTAgentRankingPerformance other = (DdTAgentRankingPerformance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsli.dd.batch.entity.DdTAgentRankingPerformance[ id=" + id + " ]";
    }
    
}
