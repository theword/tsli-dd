package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PERFORMANCE_DAILY", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewPerformanceDaily.findAll", query = "SELECT v FROM ViewPerformanceDaily v")
})
public class ViewPerformanceDaily implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 18)
    private String closYm;
	@Column(name = "DAY_OF_MONTH")
    private Date dayOfMonth;
	
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "CHANNEL", length = 5)
    private String channel;
	@Column(name = "ZONE", length = 4)
    private String zone;
	
	@Column(name = "GM_CODE", length = 30)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 30)
    private String avpCode;
	@Column(name = "AL_CODE", length = 30)
    private String alCode;
	@Column(name = "AGENT_CODE", length = 30)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 10)
    private String agentLevel;
	
	@Column(name = "PC_SUBMIT")
	private Double pcSubmit;
	@Column(name = "PC_ACTUAL")
	private Double pcActual;
	@Column(name = "ACTIVE")
	private Double active;
	@Column(name = "NC")
	private Double nc;
	@Column(name = "RETENTION")
	private Double retention;
	@Column(name = "RYP")
	private Double ryp;
	
	public ViewPerformanceDaily() {
		
	}

	public ViewPerformanceDaily(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getClosYm() {
		return closYm;
	}
	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}
	public Date getDayOfMonth() {
		return dayOfMonth;
	}
	public void setDayOfMonth(Date dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}
	public String getGroupChannel() {
		return groupChannel;
	}
	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getGmCode() {
		return gmCode;
	}
	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}
	public String getAvpCode() {
		return avpCode;
	}
	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}
	public String getAlCode() {
		return alCode;
	}
	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getAgentLevel() {
		return agentLevel;
	}
	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}
	public Double getPcSubmit() {
		return pcSubmit;
	}
	public void setPcSubmit(Double pcSubmit) {
		this.pcSubmit = pcSubmit;
	}
	public Double getPcActual() {
		return pcActual;
	}
	public void setPcActual(Double pcActual) {
		this.pcActual = pcActual;
	}
	public Double getActive() {
		return active;
	}
	public void setActive(Double active) {
		this.active = active;
	}
	public Double getNc() {
		return nc;
	}
	public void setNc(Double nc) {
		this.nc = nc;
	}
	public Double getRetention() {
		return retention;
	}
	public void setRetention(Double retention) {
		this.retention = retention;
	}
	public Double getRyp() {
		return ryp;
	}
	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewPerformanceDaily)) {
            return false;
        }
        ViewPerformanceDaily other = (ViewPerformanceDaily) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewPerformanceDaily [id=" + id + "]";
	}
	
}
