package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewPerformanceQuarterAgent;

@Repository
public interface P3ViewPerformanceQuarterAgentRepository extends CrudRepository<P3ViewPerformanceQuarterAgent, Integer> {
	// CASE I.I - AGENT find by AGENT_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_PERFORMANCE_QUARTER_AGENT"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AGENT_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)", nativeQuery = true)
	public List<P3ViewPerformanceQuarterAgent> findByAgent(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II - AGENT find by AL_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_PERFORMANCE_QUARTER_AGENT"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AL_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "				OR GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)", nativeQuery = true)
	public List<P3ViewPerformanceQuarterAgent> findByAl(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III - AGENT find by AVP_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_PERFORMANCE_QUARTER_AGENT"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AVP_CODE = :authCode"
			+ "				OR GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)", nativeQuery = true)
	public List<P3ViewPerformanceQuarterAgent> findByAvp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV - AGENT find by GM_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_PERFORMANCE_QUARTER_AGENT"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)", nativeQuery = true)
	public List<P3ViewPerformanceQuarterAgent> findByGm(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
		
}
