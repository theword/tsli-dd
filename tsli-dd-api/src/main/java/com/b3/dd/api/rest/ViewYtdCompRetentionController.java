package com.b3.dd.api.rest;

import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewYtdCompRetention;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewYtdCompRetentionRepository;

@RestController
@RequestMapping("/${api.prefix}/company")
public class ViewYtdCompRetentionController {

	@Autowired
	private ViewYtdCompRetentionRepository ViewYtdCompRetentionRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.
	@PostMapping("/ytd/retention/all")
	public ResponseEntity<List<ViewYtdCompRetention>> 
	getRTPerformanceAll(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewYtdCompRetention> ViewYtdCompRetention = ViewYtdCompRetentionRepository.findAll(
				request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewYtdCompRetention);
	}
	
	// CASE II.
	@PostMapping("/ytd/retention/channel")
	public ResponseEntity<List<ViewYtdCompRetention>> 
	getRTPerformanceChannel(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewYtdCompRetention> ViewYtdCompRetention = ViewYtdCompRetentionRepository.findByChannel(
				request.getFilter(), request.getChannelCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewYtdCompRetention);
	}
	
	// CASE III.
	@PostMapping("/ytd/retention/zone")
	public ResponseEntity<List<ViewYtdCompRetention>> 
	getRTPerformanceZone(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewYtdCompRetention> ViewYtdCompRetention = ViewYtdCompRetentionRepository.findByZone(
			request.getFilter(), request.getChannelCode(), request.getZoneCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewYtdCompRetention);
	}
}
