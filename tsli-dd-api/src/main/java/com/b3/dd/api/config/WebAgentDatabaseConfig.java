package com.b3.dd.api.config;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.b3.dd.api.config.security",
entityManagerFactoryRef = "warEntityManagerFactory",
transactionManagerRef= "warTransactionManager")

public class WebAgentDatabaseConfig {

	@Qualifier("warDatasource")
	@Bean("warDatasource")
	@ConfigurationProperties("spring.datasource-war")
	public DataSource warDatasource() {
		return DataSourceBuilder.create().type(HikariDataSource.class).build();
	}

	@Bean(name = "warEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean warEntityManagerFactory() {
		return new LocalContainerEntityManagerFactoryBean() {{
			setDataSource(warDatasource());
			setPersistenceProviderClass(HibernatePersistenceProvider.class);
			setPersistenceUnitName("warDatasource");
			setPackagesToScan("com.b3.web.agent.api.entity.war");
			setJpaProperties(warJpaProperties());
		}};
	}

	@Bean("warJpaProperties")
	@ConfigurationProperties("spring.jpa.properties-war")
	public Properties warJpaProperties() {
		return new Properties();
	}

	@Bean("warTransactionManager")
	public PlatformTransactionManager warTransactionManager(
			final @Qualifier("warEntityManagerFactory") LocalContainerEntityManagerFactoryBean warEntityManagerFactory) {
		return new JpaTransactionManager(warEntityManagerFactory.getObject());
	}
}
