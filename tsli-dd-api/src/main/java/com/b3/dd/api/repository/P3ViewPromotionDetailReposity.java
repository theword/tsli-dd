package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewPromotionDetail;

@Repository
public interface P3ViewPromotionDetailReposity extends CrudRepository<P3ViewPromotionDetail, Integer>{

	// CASE I.I find by GM_CODE
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter AND v.gmCode = :agentCode"
			+ "	AND v.type = :filterType"
			+ "	AND ("
			+ "			("
			+ "				v.gmCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "	)"
			+ "	ORDER BY"
			+ "		CASE WHEN v.gmCode = :agentCode THEN 1"
			+ "	END")
	public List<P3ViewPromotionDetail> findByGmCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("filterType") String filterType
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II find by AVP_CODE
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter AND v.avpCode = :agentCode"
			+ "	AND v.type = :filterType"
			+ "	AND ("
			+ "			("
			+ "				v.gmCode = :authCode"
			+ "				OR v.avpCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "	)"
			+ "	ORDER BY"
			+ "		CASE WHEN v.avpCode = :agentCode THEN 1"
			+ "	END")
	public List<P3ViewPromotionDetail> findByAvpCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("filterType") String filterType
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III find by AL_CODE
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter AND v.alCode = :agentCode"
			+ "	AND v.type = :filterType"
			+ "	AND ("
			+ "			("
			+ "				v.gmCode = :authCode"
			+ "				OR v.avpCode = :authCode"
			+ "				OR v.alCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "	)"
			+ "	ORDER BY"
			+ "		CASE WHEN v.alCode = :agentCode THEN 1"
			+ "	END")
	public List<P3ViewPromotionDetail> findByAlCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("filterType") String filterType
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV find by AGENT_CODE
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter AND v.agentCode = :agentCode"
			+ "	AND v.type = :filterType"
			+ "	AND ("
			+ "			("
			+ "				v.gmCode = :authCode"
			+ "				OR v.avpCode = :authCode"
			+ "				OR v.alCode = :authCode"
			+ "				OR v.agentCode = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "	)"
			+ "	ORDER BY"
			+ "		CASE WHEN v.agentCode = :agentCode THEN 1"
			+ "	END")
	public List<P3ViewPromotionDetail> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("filter") String filter
			, @Param("filterType") String filterType
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.V find by COMPANY
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter"
			+ "	AND v.type = :filterType"
			+ "	AND (:authLevel = 'ZONE')")
	public List<P3ViewPromotionDetail> findByCompany(
			@Param("filter") String filter
			, @Param("filterType") String filterType
			, @Param("authLevel") String authLevel);
	
	// CASE I.VI find by CHANNEL
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter"
			+ "	AND v.type = :filterType"
			+ "	AND v.groupChannel = :channelCode"
			+ "	AND (:authLevel = 'ZONE')")
	public List<P3ViewPromotionDetail> findByChannel(
			@Param("filter") String filter
			, @Param("channelCode") String channelCode
			, @Param("filterType") String filterType
			, @Param("authLevel") String authLevel);
	
	// CASE I.VII find by ZONE
	@Query("SELECT v FROM P3ViewPromotionDetail v"
			+ "	WHERE to_char(v.promotionConfigId) = :filter"
			+ "	AND v.type = :filterType"
			+ "	AND v.groupChannel = :channelCode"
			+ "	AND v.zone = :zoneCode"
			+ "	AND (:authLevel = 'ZONE')")
	public List<P3ViewPromotionDetail> findByZone(
			@Param("filter") String filter
			, @Param("channelCode") String channelCode
			, @Param("zoneCode") String zoneCode
			, @Param("filterType") String filterType
			, @Param("authLevel") String authLevel);
	
}
