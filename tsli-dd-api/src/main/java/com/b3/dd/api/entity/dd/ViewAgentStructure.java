package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_AGENT_STRUCTURE", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewAgentStructure.findAll", query = "SELECT v FROM ViewAgentStructure v")
	, @NamedQuery(name = "ViewAgentStructure.findByAgentCode", query = "SELECT v FROM ViewAgentStructure v WHERE v.agentCode = :agentCode")
})
public class ViewAgentStructure implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "GROUP_CHANNEL", length = 20)
    private String groupChannel;
	@Column(name = "CHANNEL", length = 20)
    private String channel;
	@Column(name = "CHANNEL_NAME", length = 20)
    private String channelName;
	@Column(name = "ZONE", length = 20)
    private String zone;
	@Column(name = "ZONE_CODE", length = 20)
    private String zoneCode;
	@Column(name = "ZONE_NAME", length = 20)
    private String zoneName;
	@Column(name = "GM_CODE", length = 20)
    private String gmCode;
	@Column(name = "GM_POSITION", length = 300)
    private String gmPosition;
	@Column(name = "GM_NAME", length = 300)
    private String gmName;
	@Column(name = "AVP_CODE", length = 20)
    private String avpCode;
	@Column(name = "AVP_POSITION", length = 300)
    private String avpPosition;
	@Column(name = "AVP_NAME", length = 300)
    private String avpName;
	@Column(name = "AL_CODE", length = 20)
    private String alCode;
	@Column(name = "AL_POSITION", length = 300)
    private String alPosition;
	@Column(name = "AL_NAME", length = 300)
    private String alName;
	@Column(name = "AGENT_CODE", length = 20)
    private String agentCode;
	@Column(name = "AG_POSITION", length = 300)
    private String agPosition;
    @Column(name = "THAI_NAME", length = 300)
    private String thaiName;
    @Column(name = "THAI_SURNAME", length = 300)
    private String thaiSurname;
    @Column(name = "AGENT_LEVEL", length = 30)
    private String agentLevel;
    @Column(name = "LEVEL_NAME", length = 20)
    private String levelName;
    @Column(name = "POSITION", length = 300)
    private String position;
    @Column(name = "POSITION_CODE", length = 25)
    private String positionCode;
    
	
	public ViewAgentStructure() {
		
	}

	public ViewAgentStructure(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getGmPosition() {
		return gmPosition;
	}

	public void setGmPosition(String gmPosition) {
		this.gmPosition = gmPosition;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAvpPosition() {
		return avpPosition;
	}

	public void setAvpPosition(String avpPosition) {
		this.avpPosition = avpPosition;
	}

	public String getAvpName() {
		return avpName;
	}

	public void setAvpName(String avpName) {
		this.avpName = avpName;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAlPosition() {
		return alPosition;
	}

	public void setAlPosition(String alPosition) {
		this.alPosition = alPosition;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgPosition() {
		return agPosition;
	}

	public void setAgPosition(String agPosition) {
		this.agPosition = agPosition;
	}

	public String getThaiName() {
		return thaiName;
	}

	public void setThaiName(String thaiName) {
		this.thaiName = thaiName;
	}

	public String getThaiSurname() {
		return thaiSurname;
	}

	public void setThaiSurname(String thaiSurname) {
		this.thaiSurname = thaiSurname;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewAgentStructure)) {
            return false;
        }
        ViewAgentStructure other = (ViewAgentStructure) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewAgentStructure [id=" + id + "]";
	}
}
