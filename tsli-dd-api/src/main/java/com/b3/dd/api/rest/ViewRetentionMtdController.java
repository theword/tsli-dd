package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewRetentionMtd;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewRetentionMtdRepository;

@RestController
@RequestMapping("/${api.prefix}/company")
public class ViewRetentionMtdController {

	@Autowired
	private ViewRetentionMtdRepository ViewRetentionMtdRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.
	@PostMapping("/retention/company")
	public ResponseEntity<List<ViewRetentionMtd>> 
	getCompanyPerformance()
	throws ResourceClosedException {
		List<ViewRetentionMtd> ViewRetentionMtd = ViewRetentionMtdRepository.findByCompany(
				userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewRetentionMtd);
	}
		
	// CASE II.
	@PostMapping("/retention/channel")
	public ResponseEntity<List<ViewRetentionMtd>> 
	getChannelPerformance(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewRetentionMtd> ViewRetentionMtd = ViewRetentionMtdRepository.findByChannel(
				request.getChannelCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewRetentionMtd);
	}
			
	// CASE III.
	@PostMapping("/retention/zone")
	public ResponseEntity<List<ViewRetentionMtd>> 
	getZonePerformance(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewRetentionMtd> ViewRetentionMtd = ViewRetentionMtdRepository.findByZone(
				request.getZoneCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewRetentionMtd);
	}
		
}
