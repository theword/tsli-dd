package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewPolicyRyp;
import com.b3.dd.api.model.PaggingModel;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewPolicyRypRepository;

@RestController
@RequestMapping("/${api.prefix}/policy/ryp")
public class ViewPolicyRypController {
	private static final int PAGE_SIZE = 500;
	@Autowired
	private ViewPolicyRypRepository ViewPolicyRypRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE II.
	@PostMapping("/company")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcCompany(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByCompany(
				request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
		
	// CASE III.
	@PostMapping("/channel")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcChannel(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByChannel(
				request.getChannelCode(), request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
	
	// CASE IV.
	@PostMapping("/zone")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcZone(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByZone(
				request.getZoneCode(), request.getFilter(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
	
	// CASE I.
	@PostMapping("/agent")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcAgent(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByAgentCode(
				request.getAgentCode(), request.getAgentLevel(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
	
	// CASE I.I
	@PostMapping("/al")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcUnderAl(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByAlCode(
				request.getAlCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
	
	// CASE I.II
	@PostMapping("/avp")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcUnderAvp(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByAvpCode(
				request.getAvpCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
		
	// CASE I.III
	@PostMapping("/gm")
	public ResponseEntity<List<ViewPolicyRyp>> 
	getPolicyPcUnderGm(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		List<ViewPolicyRyp> ViewPolicyRyp = ViewPolicyRypRepository.findByGmCode(
				request.getGmCode(), request.getFilter(), 
				principal.getName(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(ViewPolicyRyp);
	}
	
	// CASE V getAllRowByStatus
	@PostMapping("/company-pagination")
	public ResponseEntity<PaggingModel<ViewPolicyRyp>> 
	getPagingCompany(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		
		
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		
		
		Page<ViewPolicyRyp>  ViewPolicyRyp = ViewPolicyRypRepository.findByCompanyPagination(
				request.getFilter(), userHelper.getUserPosition(), pagging);
		
		
		return ResponseEntity.ok().body( new PaggingModel<ViewPolicyRyp>(ViewPolicyRyp.getContent(), ViewPolicyRyp.getTotalPages(), ViewPolicyRyp.getTotalElements()));
	
	}
		
	// CASE III.I channelPagination
	@PostMapping("/channel-pagination")
	public ResponseEntity<PaggingModel<ViewPolicyRyp>> 
	getPagingChannel(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		
		
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		
		
		Page<ViewPolicyRyp>  ViewPolicyRyp = ViewPolicyRypRepository.findByChannelPagination(
				request.getChannelCode(), request.getFilter(), userHelper.getUserPosition(), pagging);
		
		
		return ResponseEntity.ok().body( new PaggingModel<ViewPolicyRyp>(ViewPolicyRyp.getContent(), ViewPolicyRyp.getTotalPages(), ViewPolicyRyp.getTotalElements()));
	
	}
	
	// CASE VIII findBySearchPagination
	@PostMapping("/search-pagination")
	public ResponseEntity<PaggingModel<ViewPolicyRyp>> 
	getSearchPaging(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		
		
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by(request.getSortColName()).descending());
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by(request.getSortColName()).ascending());
		}
		else {
			pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE);
		}
		
		Page<ViewPolicyRyp>  ViewPolicyRyp = ViewPolicyRypRepository.findBySearchPagination(
				request.getFilter(), request.getSearchKeyword(), request.getChannelCode(), userHelper.getUserPosition(), pagging);
		
		
		return ResponseEntity.ok().body( new PaggingModel<ViewPolicyRyp>(ViewPolicyRyp.getContent(), ViewPolicyRyp.getTotalPages(), ViewPolicyRyp.getTotalElements()));
	
	}
}
