package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewYtdCompRetention;

@Repository
public interface ViewYtdCompRetentionRepository extends CrudRepository<ViewYtdCompRetention, Integer> {

	// CONDITION I. RETENTION: CURRENTYEAR ONLY, all channel and zone
	@Query("SELECT v FROM ViewYtdCompRetention v WHERE v.closYm = :filter"
			+ " AND (:authLevel = 'ZONE')"
			+ " ORDER BY "
			+ " CASE"
			+ "		WHEN v.agentLevel = 'CHANNEL' THEN 1"
			+ "		WHEN v.agentLevel = 'COMPANY' THEN 2"
			+ "		ELSE 0"
			+ "	END, CODE")
	public List<ViewYtdCompRetention> findAll(
			@Param("filter") String filter
			, @Param("authLevel") String authLevel);
	
	// CONDITION II. RETENTION: CHANNEL, only zone under this channel
	@Query("SELECT v FROM ViewYtdCompRetention v WHERE v.closYm = :filter"
			+ " AND ((v.agentLevel = 'CHANNEL' AND v.groupChannel = :channelCode) OR v.groupChannel = :channelCode"
			+ " AND (:authLevel = 'ZONE'))"
			+ " ORDER BY CODE")
	public List<ViewYtdCompRetention> findByChannel(
			@Param("filter") String filter
			,@Param("channelCode") String channelCode
			, @Param("authLevel") String authLevel);
	
	// CONDITION III. RETENTION: ZONE only
	@Query("SELECT v FROM ViewYtdCompRetention v WHERE v.closYm = :filter"
			+ " AND v.agentLevel = 'ZONE' "
			+ " AND v.groupChannel = :channelCode "
			+ " AND v.code = :zoneCode"
			+ " AND :authLevel = 'ZONE'")
	public List<ViewYtdCompRetention> findByZone(
			@Param("filter") String filter
			, @Param("channelCode") String channelCode
			, @Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
		
	
}
