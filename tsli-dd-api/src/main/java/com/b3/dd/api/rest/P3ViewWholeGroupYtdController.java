package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewWholeGroupYtd;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewWholeGroupYtdRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewWholeGroupYtdController {
	@Autowired
	private P3ViewWholeGroupYtdRepository P3ViewWholeGroupYtdRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - I.IV : Under Producer
	@PostMapping("kpidetail-wholegroup-ytd")
	public ResponseEntity<List<P3ViewWholeGroupYtd>>
	getTeamDetailWholeYtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String level = userHelper.getUserPosition();
		List<P3ViewWholeGroupYtd> P3ViewWholeGroupYtd = null;
		
		switch (level) {
		case "AGENT":
			P3ViewWholeGroupYtd = P3ViewWholeGroupYtdRepository.findByAgent(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "AL":
			P3ViewWholeGroupYtd = P3ViewWholeGroupYtdRepository.findByAl(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition() 
			);
			break;
		case "AVP":
			P3ViewWholeGroupYtd = P3ViewWholeGroupYtdRepository.findByAvp(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "GM":
			P3ViewWholeGroupYtd = P3ViewWholeGroupYtdRepository.findByGm(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "ZONE":
			P3ViewWholeGroupYtd = P3ViewWholeGroupYtdRepository.findByGm(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewWholeGroupYtd);
	}
}
