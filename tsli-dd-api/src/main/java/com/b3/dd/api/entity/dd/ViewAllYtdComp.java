package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_ALL_YTD_COMP", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewAllYtdComp.findAll", query = "SELECT v FROM ViewAllYtdComp v")
})
public class ViewAllYtdComp implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 4)
    private String closYm;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "CODE", length = 30)
    private String code;
	
	@Column(name = "PC_TARGET")
	private Double pcTarget;
	@Column(name = "ACTIVE_TARGET")
	private Double activeTarget;
	@Column(name = "NC_TARGET")
	private Double ncTarget;
	@Column(name = "RETENTION_TARGET")
	private Double retentionTarget;
	@Column(name = "RYP_TARGET")
	private Double rypTarget;
	
	@Column(name = "PC_APPROVE")
	private Double pcApprove;
	@Column(name = "PC_SUBMIT")
	private Double pcSubmit;
	@Column(name = "ACTIVE")
	private Double active;
	@Column(name = "NC")
	private Double nc;
	@Column(name = "RETENTION_YTD")
	private Double retentionYtd;
	@Column(name = "RYP")
	private Double ryp;
	
	@Column(name = "PC_PERCENT")
	private Double pcPercent;
	@Column(name = "ACTIVE_PERCENT")
	private Double activePercent;
	@Column(name = "NC_PERCENT")
	private Double ncPercent;
	@Column(name = "RETENTION_PERCENT")
	private Double retentionPercent;
	@Column(name = "RYP_PERCENT")
	private Double rypPercent;
	
	@Column(name = "NO_CASE")
	private Double noCase;
	@Column(name = "CASE_SIZE")
	private Double caseSize;
	
	public ViewAllYtdComp() {
		
	}

	public ViewAllYtdComp(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getPcTarget() {
		return pcTarget;
	}

	public void setPcTarget(Double pcTarget) {
		this.pcTarget = pcTarget;
	}

	public Double getActiveTarget() {
		return activeTarget;
	}

	public void setActiveTarget(Double activeTarget) {
		this.activeTarget = activeTarget;
	}

	public Double getNcTarget() {
		return ncTarget;
	}

	public void setNcTarget(Double ncTarget) {
		this.ncTarget = ncTarget;
	}

	public Double getRetentionTarget() {
		return retentionTarget;
	}

	public void setRetentionTarget(Double retentionTarget) {
		this.retentionTarget = retentionTarget;
	}

	public Double getRypTarget() {
		return rypTarget;
	}

	public void setRypTarget(Double rypTarget) {
		this.rypTarget = rypTarget;
	}

	public Double getPcApprove() {
		return pcApprove;
	}

	public void setPcApprove(Double pcApprove) {
		this.pcApprove = pcApprove;
	}

	public Double getPcSubmit() {
		return pcSubmit;
	}

	public void setPcSubmit(Double pcSubmit) {
		this.pcSubmit = pcSubmit;
	}

	public Double getActive() {
		return active;
	}

	public void setActive(Double active) {
		this.active = active;
	}

	public Double getNc() {
		return nc;
	}

	public void setNc(Double nc) {
		this.nc = nc;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getPcPercent() {
		return pcPercent;
	}

	public void setPcPercent(Double pcPercent) {
		this.pcPercent = pcPercent;
	}

	public Double getActivePercent() {
		return activePercent;
	}

	public void setActivePercent(Double activePercent) {
		this.activePercent = activePercent;
	}

	public Double getNcPercent() {
		return ncPercent;
	}

	public void setNcPercent(Double ncPercent) {
		this.ncPercent = ncPercent;
	}

	public Double getRetentionPercent() {
		return retentionPercent;
	}

	public void setRetentionPercent(Double retentionPercent) {
		this.retentionPercent = retentionPercent;
	}

	public Double getRypPercent() {
		return rypPercent;
	}

	public void setRypPercent(Double rypPercent) {
		this.rypPercent = rypPercent;
	}

	public Double getRetentionYtd() {
		return retentionYtd;
	}

	public void setRetentionYtd(Double retentionYtd) {
		this.retentionYtd = retentionYtd;
	}

	public Double getNoCase() {
		return noCase;
	}

	public void setNoCase(Double noCase) {
		this.noCase = noCase;
	}

	public Double getCaseSize() {
		return caseSize;
	}

	public void setCaseSize(Double caseSize) {
		this.caseSize = caseSize;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewAllYtdComp)) {
            return false;
        }
        ViewAllYtdComp other = (ViewAllYtdComp) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewAllYtdComp [id=" + id + "]";
	}

}
