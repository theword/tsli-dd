package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PERFORMANCE_QUARTER_AGENT", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewPerformanceQuarterAgent.findAll", query = "SELECT v FROM P3ViewPerformanceQuarterAgent v")
})
public class P3ViewPerformanceQuarterAgent implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "ZONE", length = 4)
    private String zone;
	@Column(name = "GM_CODE", length = 30)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 30)
    private String avpCode;
	@Column(name = "AL_CODE", length = 30)
    private String alCode;
	@Column(name = "AGENT_CODE", length = 30)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 20)
    private String agentLevel;
	
	@Column(name = "Q1")
    private Double q1;
	@Column(name = "Q2")
    private Double q2;
	@Column(name = "Q3")
    private Double q3;
	@Column(name = "Q4")
    private Double q4;
	
	public P3ViewPerformanceQuarterAgent() {	
	}

	public P3ViewPerformanceQuarterAgent(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewPerformanceQuarterAgent)) {
            return false;
        }
        P3ViewPerformanceQuarterAgent other = (P3ViewPerformanceQuarterAgent) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public Double getQ1() {
		return q1;
	}

	public void setQ1(Double q1) {
		this.q1 = q1;
	}

	public Double getQ2() {
		return q2;
	}

	public void setQ2(Double q2) {
		this.q2 = q2;
	}

	public Double getQ3() {
		return q3;
	}

	public void setQ3(Double q3) {
		this.q3 = q3;
	}

	public Double getQ4() {
		return q4;
	}

	public void setQ4(Double q4) {
		this.q4 = q4;
	}
	

}
