package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.DdTPromoteConfig;

@Repository
public interface DdTPromoteConfigRepository extends CrudRepository<DdTPromoteConfig, Integer> {
	// CASE I.I
	@Query(value=""
			+ "SELECT * FROM DD_T_PROMOTE_CONFIG"
			+ " WHERE EFFECTIVE_YM = :closYm"
			+ "		AND GROUP_CHANNEL = :channelCode"
			+ "		AND AGENT_LEVEL = :agentLevel"
			+ "		AND PERIOD_TYPE = :periodType"
			+ "		AND POSITION_CODE = :positionCode"
	, nativeQuery = true)
	public List<DdTPromoteConfig> findConfig(
			@Param("closYm") String closYm
			, @Param("channelCode") String channelCode
			, @Param("agentLevel") String agentLevel
			, @Param("periodType") String periodType
			, @Param("positionCode") String positionCode
	);
}
