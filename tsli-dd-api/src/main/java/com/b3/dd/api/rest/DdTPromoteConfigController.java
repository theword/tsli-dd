package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.DdTPromoteConfig;
import com.b3.dd.api.entity.dd.DdTPromoteValidateMapping;
import com.b3.dd.api.entity.dd.P3ViewPromoteDetailNormal;
import com.b3.dd.api.entity.dd.P3ViewPromoteDetailRollBack12;
import com.b3.dd.api.entity.dd.P3ViewPromoteDetailRollBack12Current;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.DdTPromoteConfigRepository;
import com.b3.dd.api.repository.DdTPromoteValidateMappingRepository;
import com.b3.dd.api.repository.P3ViewPromoteDetailNormalRepository;
import com.b3.dd.api.repository.P3ViewPromoteDetailRollBack12Repository;
import com.b3.dd.api.repository.P3ViewPromoteDetailRollBack12CurrentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class DdTPromoteConfigController {
	@Autowired
	private DdTPromoteConfigRepository DdTPromoteConfigRepository;
	@Autowired
	private P3ViewPromoteDetailNormalRepository P3ViewPromoteDetailNormalRepository;
	@Autowired
	private P3ViewPromoteDetailRollBack12Repository P3ViewPromoteDetailRollBack12Repository;
	@Autowired
	private P3ViewPromoteDetailRollBack12CurrentRepository P3ViewPromoteDetailRollBack12CurrentRepository;
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private DdTPromoteValidateMappingRepository DdTPromoteValidateMappingRepository;
	
	// CASE I.I
    @PostMapping("promote-config")
    public ResponseEntity<Map<String, Object>>
    getTeamDetailWholeMtd(@RequestBody RequestModel request, final Principal principal)
        throws ResourceClosedException {
    	// Prepare variables
    	Map<String, Object> response = new HashMap<String, Object>();
        List<DdTPromoteConfig> DdTPromoteConfig = null;
        List<DdTPromoteValidateMapping> DdTPromoteValidateMapping = null;
        
        // periodType = ["NORMAL", "ROLLBACK12", "ROLLBACK12_CURRENT"]
        String periodType = request.getPeriodType();
        
        // Query Promote Configuration
        DdTPromoteConfig = DdTPromoteConfigRepository.findConfig(
            request.getClosYm(), request.getChannelCode()
            , request.getAgentLevel(), request.getPeriodType(), request.getPositionCode()
        );
        
        // Query Mapping
        DdTPromoteValidateMapping = DdTPromoteValidateMappingRepository.findAll();
        response.put("mapping", DdTPromoteValidateMapping);
        
        // Pack Response Data
        DdTPromoteConfig tempPromoteConfig = DdTPromoteConfig.stream().findFirst().orElse(null);
        if (tempPromoteConfig != null) {
        	response.put("config", DdTPromoteConfig);
        } else {
        	response.put("config", null);
        }
        
        switch (periodType) {
		case "NORMAL":
			List<P3ViewPromoteDetailNormal> P3ViewPromoteDetailNormal = null;
			P3ViewPromoteDetailNormal = P3ViewPromoteDetailNormalRepository.findByLevel(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			response.put("data", P3ViewPromoteDetailNormal);
			break;
			
		case "ROLLBACK12":
			List<P3ViewPromoteDetailRollBack12> P3ViewPromoteDetailRollBack12 = null;
			P3ViewPromoteDetailRollBack12 = P3ViewPromoteDetailRollBack12Repository.findByLevel(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			response.put("data", P3ViewPromoteDetailRollBack12);
			break;
			
		case "ROLLBACK12_CURRENT":
			List<P3ViewPromoteDetailRollBack12Current> P3ViewPromoteDetailRollBack12Current = null;
			P3ViewPromoteDetailRollBack12Current = P3ViewPromoteDetailRollBack12CurrentRepository.findByLevel(
				request.getAgentCode(), request.getAgentLevel()
				, principal.getName(), userHelper.getUserPosition()
			);
			response.put("data", P3ViewPromoteDetailRollBack12Current);
			break;

		default:
			List emptyData = new ArrayList();
			response.put("data", emptyData);
			break;
		}
        
        return ResponseEntity.ok().body(response);
    }
}
