package com.b3.dd.api.rest;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;


//@RestController
//@RequestMapping
public class RevokeTokenEndpoint {

    @Autowired
    private DefaultTokenServices tokenServices;

    @DeleteMapping("/oauth/token")
    @ResponseBody
    public void revokeToken(HttpServletRequest request, Authentication authentication) {
    	
    	final String userToken = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
//        String authorization = request.getHeader("Authorization");
        tokenServices.revokeToken(userToken);
//        if (authorization != null && authorization.contains("Bearer")) {
//            String tokenId = authorization.substring("Bearer".length() + 1);
//            tokenServices.revokeToken(userToken);
//        }
    }
    


}