package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewTargetEdit;

@Repository
public interface P3ViewTargetEditRepository extends CrudRepository<P3ViewTargetEdit, Integer>{
	// CASE I. find by code
	@Query("SELECT v FROM P3ViewTargetEdit v"
			+ "	WHERE v.code = :agentCode"
			+ "	AND v.kpiLevel = :agentLevel"
//			+ "	AND (v.kpiName = 'PC' OR v.kpiName = 'NEW_CODE' OR v.kpiName = 'NEW_ACTIVE_AGENT')"
			+ " AND (v.productionYear = :closYm)"
			+ " AND ("
			+ "		v.code = :authCode"
			+ "		OR ("
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.gmCode = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.avpCode = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.alCode = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ " 	)"
			+ "		OR (:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR ("
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ " 	)"
			+ " )"
			+ " ORDER BY v.updateDate DESC")
	public List<P3ViewTargetEdit> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("closYm") String closYm
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
