package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewAllYtdComp;

@Repository
public interface ViewAllYtdCompRepository extends CrudRepository<ViewAllYtdComp, Integer> {

	@Query("SELECT v FROM ViewAllYtdComp v WHERE v.agentLevel = :agentLevel "
			+ " AND v.code = :zoneCode"
			+ " AND	(:authLevel = 'ZONE')")
	public List<ViewAllYtdComp> findByLevelAndCode(
			@Param("agentLevel") String agentLevel
			,@Param("zoneCode") String zoneCode
			, @Param("authLevel") String authLevel);
}
