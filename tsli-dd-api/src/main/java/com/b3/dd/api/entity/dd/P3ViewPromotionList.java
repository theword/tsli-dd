package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PROMOTION_LIST", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewPromotionList.findAll", query = "SELECT v FROM P3ViewPromotionList v")
})
public class P3ViewPromotionList implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 18, nullable = true)
    private String closYm;
	@Column(name = "GM_CODE", length = 20, nullable = true)
    private String gmCode;
	@Column(name = "AVP_CODE", length = 20, nullable = true)
    private String avpCode;
	@Column(name = "AL_CODE", length = 20, nullable = true)
    private String alCode;
	@Column(name = "AGENT_CODE", length = 20, nullable = true)
    private String agentCode;
	@Column(name = "AGENT_POSITION", length = 30, nullable = true)
    private String agentPosition;
	@Column(name = "CONTEST_CODE", length = 64, nullable = true)
    private String contestCode;
	@Column(name = "CONTEST_DESC" , nullable = true)
    private String contestDesc;
	@Column(name = "TYPE", length = 1024 , nullable = true)
    private String type;
	@Column(name = "CREATE_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
	
	public P3ViewPromotionList() {	
	}

	public P3ViewPromotionList(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewPromotionList)) {
            return false;
        }
        P3ViewPromotionList other = (P3ViewPromotionList) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentPosition() {
		return agentPosition;
	}

	public void setAgentPosition(String agentPosition) {
		this.agentPosition = agentPosition;
	}

	public String getContestCode() {
		return contestCode;
	}

	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	public String getContestDesc() {
		return contestDesc;
	}

	public void setContestDesc(String contestDesc) {
		this.contestDesc = contestDesc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
