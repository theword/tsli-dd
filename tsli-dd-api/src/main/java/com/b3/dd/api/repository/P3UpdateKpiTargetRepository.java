package com.b3.dd.api.repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.b3.dd.api.entity.dd.DdTKpiTarget;

import org.springframework.data.repository.query.Param;

public interface P3UpdateKpiTargetRepository extends JpaRepository<DdTKpiTarget, Integer>{

	@Transactional
	@Modifying
	@Query(value="UPDATE DD_T_KPI_TARGET"
			+ "	SET TARGET_YEAR = :pcSumYearTarget"
			+ "		, TARGET_M1 = :targetM1"
			+ "		, TARGET_M2 = :targetM2"
			+ "		, TARGET_M3 = :targetM3"
			+ "		, TARGET_M4 = :targetM4"
			+ "		, TARGET_M5 = :targetM5"
			+ "		, TARGET_M6 = :targetM6"
			+ "		, TARGET_M7 = :targetM7"
			+ "		, TARGET_M8 = :targetM8"
			+ "		, TARGET_M9 = :targetM9"
			+ "		, TARGET_M10 = :targetM10"
			+ "		, TARGET_M11 = :targetM11"
			+ "		, TARGET_M12 = :targetM12"
			+ "		, UPDATE_BY = :authCode"
			+ "		, UPEATE_DATE = :timeStamp"
			+ "	WHERE CODE = :agentCode"
			+ "		AND PRODUCTION_YEAR = :closYm"
			+ "		AND KPI_NAME = :kpiName"
			+ "		AND KPI_LEVEL = :agentLevel"
			+ " AND ("
			+ "		CODE = :authCode"
			+ "		OR ("
			+ "			(SELECT COUNT(va.AGENT_CODE) "
			+ "			FROM VIEW_AGENT_STRUCTURE va "
			+ "			WHERE va.GM_CODE = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.AGENT_CODE) "
			+ "			FROM VIEW_AGENT_STRUCTURE va "
			+ "			WHERE va.AVP_CODE = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.AGENT_CODE) "
			+ "			FROM VIEW_AGENT_STRUCTURE va "
			+ "			WHERE va.AL_CODE = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ " 	)"
			+ "		OR (:authLevel = 'ZONE')"
			+ " )"
	, nativeQuery = true)
	void setYearTargetByKpi(
		@Param("agentCode") String agentCode
		, @Param("agentLevel") String agentLevel
		, @Param("closYm") String closYm
		, @Param("kpiName") String kpiName
		, @Param("pcSumYearTarget") Integer pcSumYearTarget
		, @Param("targetM1") Integer targetM1
		, @Param("targetM2") Integer targetM2
		, @Param("targetM3") Integer targetM3
		, @Param("targetM4") Integer targetM4
		, @Param("targetM5") Integer targetM5
		, @Param("targetM6") Integer targetM6
		, @Param("targetM7") Integer targetM7
		, @Param("targetM8") Integer targetM8
		, @Param("targetM9") Integer targetM9
		, @Param("targetM10") Integer targetM10
		, @Param("targetM11") Integer targetM11
		, @Param("targetM12") Integer targetM12
		, @Param("timeStamp") Date timeStamp
		, @Param("authCode") String authCode
		, @Param("authLevel") String authLevel
	);
}
