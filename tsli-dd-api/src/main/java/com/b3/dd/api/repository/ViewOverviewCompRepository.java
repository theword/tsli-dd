package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewOverviewComp;

@Repository
public interface ViewOverviewCompRepository extends CrudRepository<ViewOverviewComp, Integer> {
	
	
	@Query("SELECT v FROM ViewOverviewComp v"
			+ " WHERE (:authLevel = 'ZONE')"
			+ " ORDER BY"
			+ "  CASE"
			+ "		WHEN v.staffLevel = 'CHANNEL' THEN 1"
			+ "		WHEN v.staffLevel = 'COMPANY' THEN 2"
			+ "	 ELSE 0"
			+ " END, CODE")
	public List<ViewOverviewComp> findAll(
			@Param("authLevel") String authLevel);
}
