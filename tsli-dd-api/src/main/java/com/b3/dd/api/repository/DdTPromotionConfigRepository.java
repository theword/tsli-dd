package com.b3.dd.api.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.b3.dd.api.entity.dd.P3DdTPromotionConfig;

@Repository
public interface DdTPromotionConfigRepository extends CrudRepository<P3DdTPromotionConfig, Integer> {

	@Query(value=""
			+ "SELECT * FROM DD_T_PROMOTION_CONFIG"
			+ "	WHERE :authLevel = 'ZONE'"
			+ "	ORDER BY CREATE_DATE DESC"
	, nativeQuery = true)
	public List<P3DdTPromotionConfig> findPromotionConfig(
			@Param("authLevel") String authLevel
	);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE DD_T_PROMOTION_CONFIG"
			+ "	SET IS_DELETE = 'T'"
			+ "		, UPDATE_DATE = :updateDate"
			+ "		, UPDATE_BY = :updateBy"
			+ "	WHERE ID = :promotionConfigId"
			+ "		AND :authLevel = 'ZONE'"
	, nativeQuery = true)
	public void deleteConfigById(
			@Param("promotionConfigId") Integer promotionConfigId
			, @Param("updateDate") Date updateDate
			, @Param("updateBy") String updateBy
			, @Param("authLevel") String authLevel
	);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE DD_T_PROMOTION_DETAIL"
			+ "	SET IS_DELETE = 'T'"
			+ "		, UPDATE_DATE = :updateDate"
			+ "		, UPDATE_BY = :updateBy"
			+ "	WHERE PROMOTION_CONFIG_ID = :promotionConfigId"
			+ "		AND :authLevel = 'ZONE'"
	, nativeQuery = true)
	public void deleteDetailById(
			@Param("promotionConfigId") Integer promotionConfigId
			, @Param("updateDate") Date updateDate
			, @Param("updateBy") String updateBy
			, @Param("authLevel") String authLevel
	);
}
