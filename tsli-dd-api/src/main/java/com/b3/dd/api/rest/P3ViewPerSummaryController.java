package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.apache.commons.compress.utils.Lists;
import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewPerSummary;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewPerSummaryRepository;
import com.b3.dd.api.service.PersistencyService;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewPerSummaryController {

	@Autowired
	private P3ViewPerSummaryRepository P3ViewPerSummaryRepository;
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private PersistencyService persistencyService;
	
	@PostMapping("persistency/summary")
	public ResponseEntity<List<P3ViewPerSummary>>
	getAgentTarget(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		
		List<P3ViewPerSummary> P3ViewPerSummary = P3ViewPerSummaryRepository.findByAgentWithPerType(
			request.getFilter(), request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
		);

		
		P3ViewPerSummary = persistencyService.getBlockMonth(request.getFilter(), request.getAgentCode(), request.getAgentLevel(), P3ViewPerSummary);
		
		return ResponseEntity.ok().body(P3ViewPerSummary);

	}
}
