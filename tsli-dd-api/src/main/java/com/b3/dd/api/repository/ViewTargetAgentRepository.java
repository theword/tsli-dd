package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewTargetAgent;

@Repository
public interface ViewTargetAgentRepository extends CrudRepository<ViewTargetAgent, Integer> {

	@Query("SELECT v FROM ViewTargetAgent v WHERE v.code = :agentCode"
			+ " AND v.productionYear = (SELECT (SUBSTR(MIN(vc.closYm),1 ,4)) AS minClosYm"
			+ "			FROM DdTClosCalendar vc WHERE vc.flagEndClosingYmd = 'F'"
			+ "		)"
			+ "	AND v.kpiLevel = :agentLevel"
			+ " AND ("
			+ " 		v.code = :authCode"
			+ " 	OR ("
			+ " 		(SELECT COUNT(va.agentCode) FROM ViewAgentStructure va WHERE va.gmCode = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ "			OR"
			+ " 		(SELECT COUNT(va.agentCode) FROM ViewAgentStructure va WHERE va.avpCode = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ "			OR"
			+ " 		(SELECT COUNT(va.agentCode) FROM ViewAgentStructure va WHERE va.alCode = :authCode AND AGENT_CODE = :agentCode) > 0"
			+ " 	)"
			+ " 	OR (:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.code IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ "		OR ("
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ "			OR"
			+ "			(SELECT COUNT(va.agentCode) "
			+ "			FROM ViewAgentStructure va "
			+ "			WHERE va.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "				AND va.agentCode = :agentCode) > 0"
			+ " 	)"
			+ ")"
	)
	public List<ViewTargetAgent> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
