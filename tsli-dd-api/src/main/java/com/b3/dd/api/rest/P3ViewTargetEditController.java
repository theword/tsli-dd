package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewTargetEdit;
import com.b3.dd.api.entity.dd.ViewPerformanceAgent;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewTargetEditRepository;
import com.b3.dd.api.repository.ViewPerformanceAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/edit")
public class P3ViewTargetEditController {

	@Autowired
	private P3ViewTargetEditRepository P3ViewTargetEditRepository;
	@Autowired
	private ViewPerformanceAgentRepository ViewPerformanceAgentRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("target-year-by-agent")
	public ResponseEntity<Map<String, Object>>
	getAgentTarget(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		// Prepare variables
		Map<String, Object> response = new HashMap<String, Object>();
		
		// Get Current Year - Target
		List<P3ViewTargetEdit> P3ViewTargetEdit = P3ViewTargetEditRepository.findByAgentCode(
			request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), principal.getName(), userHelper.getUserPosition()
		);
		response.put("currentYear", P3ViewTargetEdit);
		
		// Query Last Year - PC, Active, NC
		List<ViewPerformanceAgent> ViewPerformanceAgent = null;
		ViewPerformanceAgent = ViewPerformanceAgentRepository.findLastYearByAgentCode(
			request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
		);
		List<ViewPerformanceAgent> lastYear = ViewPerformanceAgent.stream().collect(Collectors.toList());
		
		// Get lastYear - PcApprove (Actual), active, nc, ryp and retention
		List<Double> pcLastYear = new ArrayList<Double>();
		List<Double> activeLastYear = new ArrayList<Double>();
		List<Double> ncLastYear = new ArrayList<Double>();
		List<Double> rypLastYear = new ArrayList<Double>();
		List<Double> retentionLastYear = new ArrayList<Double>();
		

//		System.out.println("lastYear " + lastYear.get(0).getClosYm());
		String firstMonth =  lastYear.get(0).getClosYm();
		int monthNo = Integer.parseInt(firstMonth.substring(4,6));
//		System.out.println("monthNo " + monthNo);
		
		for(int i=1; i < monthNo; i++) {
			pcLastYear.add(0.0);
            activeLastYear.add(0.0);
            ncLastYear.add(0.0);
            rypLastYear.add(0.0);
            retentionLastYear.add(0.0);
		}
		
		for (ViewPerformanceAgent model : lastYear) {
            pcLastYear.add(model.getPcApprove());
            activeLastYear.add(model.getActive());
            ncLastYear.add(model.getNc());
            rypLastYear.add(model.getRyp());
            retentionLastYear.add(model.getRetention());
        }
		response.put("pcLastYear", pcLastYear);
		response.put("activeLastYear", activeLastYear);
		response.put("ncLastYear", ncLastYear);
		response.put("rypLastYear", rypLastYear);
		response.put("retentionLastYear", retentionLastYear);
		
		return ResponseEntity.ok().body(response);
	}
}
