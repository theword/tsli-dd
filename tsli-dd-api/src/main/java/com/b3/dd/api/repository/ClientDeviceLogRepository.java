package com.b3.dd.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.entity.dd.ClientDeviceLog;

public interface ClientDeviceLogRepository extends CrudRepository<ClientDeviceLog, Integer> {

	@Query("select c from ClientDeviceLog c where c.token = :token")
	public ClientDeviceLog findByToken(@Param("token") String token);
//	public ClientDeviceLog findByToken(String token);
}
