package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewButtonName;

@Repository
public interface ViewButtonNameRepository extends CrudRepository<ViewButtonName, Integer> {

	// CASE II: Get agent (AG) under AL
		@Query("SELECT v FROM ViewButtonName v")
		public List<ViewButtonName> findAll();
}
