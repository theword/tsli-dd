package com.b3.dd.api.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.b3.dd.api.model.ScheduleJobModel;

@Service
public class ScheduleService {
	private static final Logger log = LogManager.getLogger(ScheduleService.class);
	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
	private static final String sql = "SELECT B3_START_DATETIME FROM DATAHUB_PROD.STG_SCHEDULE_LOG WHERE BATCH_TYPE = 'D' AND TSLI_STATUS = 2 ";
	private ScheduleJobModel sheduleJob;
	
	@PersistenceContext(unitName="ddDatasource")
	private EntityManager em;

//	public ScheduleService(EntityManager em) {
	public ScheduleService() {
		super();
//		this.em = em;
		sheduleJob = new ScheduleJobModel();
	}
	
	@Scheduled(fixedRate = 60000L)
	public void scheduleTaskRefreshSchedule() {
		refreshSchedule();
		log.debug("Schedule Task Refresh Schedule On: " + new Date() );
	}
	
	public synchronized void refreshSchedule() {
		readSchedule();
	};
	
	private void readSchedule() {
		try {
			java.sql.Timestamp startDate = (Timestamp) em.createNativeQuery(sql).getSingleResult();
			if( startDate == null ) {
				return;
			}
			sheduleJob.setSchedule(SDF.format(startDate));
		}catch (Exception e) {
			log.warn("Schedule Task Refresh Schedule error.");
		}
	}

	public ScheduleJobModel getSheduleJob() {
		return sheduleJob;
	}

	public void setSheduleJob(ScheduleJobModel sheduleJob) {
		this.sheduleJob = sheduleJob;
	};
	
}