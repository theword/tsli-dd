package com.b3.dd.api.lockengine.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.UserDevice;
import com.b3.dd.api.entity.dd.Users;
import com.b3.dd.api.entity.dd.UsersLogging;
import com.b3.dd.api.lockengine.model.ResAuth;
import com.b3.dd.api.repository.UsersLoggingRepository;
import com.b3.dd.api.repository.UsersRepository;
import com.b3.dd.api.service.LockEngineService;
import com.b3.dd.api.service.LockEngineService.DeviceMode;
import com.b3.dd.api.service.UsageReportService;
import com.deverhood.lock.manager.model.DeviceManager;
import com.deverhood.lock.manager.resource.ResAuthToken;
import com.deverhood.lock.manager.resource.ResponseStatus;
import com.deverhood.util.ApiClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/${api.prefix}/lock-engine")
public class LockEngineManagerController {

	private static final Logger logger = LogManager.getLogger(LockEngineManagerController.class);
	@Value("${lockengine.license.key}")
	private String lockengineLicenseKey;
	@Value("${lockengine.license.name}")
	private String lockengineLicenseName;

	@Autowired
	private UsageReportService usageReportService;

	@Autowired
	private UsersLoggingRepository usersLoggingRepository;

	@Autowired
	private LockEngineService lockEngineService;
	@Autowired
	private UsersRepository usersRepository ;

	@GetMapping("/devices")
	public ResponseEntity<List<UserDevice>> getDevices(final Principal principal) {
		List<UserDevice> devices = lockEngineService.getDevices(principal.getName());
		return ResponseEntity.ok(devices);
	}

	@PostMapping("/save/device")
	public ResponseEntity<Map<String, Object>> saveDevice(
			@RequestBody UserDevice userDevice
			, @RequestParam("isMobile") Boolean isMobile
			, final Principal principal ) {
		Map<String, Object> response = new HashMap<String ,Object>();
		try {
			Boolean status = lockEngineService.saveDevice(principal.getName(), userDevice.getDeviceId(), userDevice.getDeviceName(), isMobile?DeviceMode.MOBILE:DeviceMode.DESKTOP);
			List<UserDevice> devices = lockEngineService.getDevices(principal.getName());
			response.put("data", devices);
			response.put("status", status);
			return ResponseEntity.status(HttpStatus.CREATED).body(response);
		}catch (Exception e) {
			response.put("status", false);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}

	}


	@DeleteMapping("/delete-devices/{deviceId}")
	public ResponseEntity<List<UserDevice>> deleteDevices(@PathVariable("deviceId") Integer deviceId, final Principal principal) {
		lockEngineService.deleteDevice(deviceId);
		return ResponseEntity.ok().build();
	}
	/**
	 *
	 * @param token
	 * @return
	 */
	@PostMapping(value = "/deviceValidation/mobile")
	public ResponseStatus postValidationMobile(@RequestBody ResAuthToken token) {
		logger.info("Validate access device permission mobile");

		return validateAuth(token, DeviceMode.MOBILE);
	}

	/**
	 *
	 * @param token
	 * @return
	 */
	@PostMapping(value = "/deviceValidation/desktop")
	public ResponseStatus postValidationDesktop(@RequestBody ResAuthToken token) {
		logger.info("Validate access device permission desktop");
		return validateAuth(token, DeviceMode.DESKTOP);
	}

	private ResponseStatus validateAuth(ResAuthToken token, DeviceMode deviceMode) {
		Date start = new Date();
		String json = ApiClient.decodeToken(token.getToken(), ResAuthToken.PROTOCOL);
		ResAuth auth = null;
		ResponseStatus res = new ResponseStatus();

		try {
			ObjectMapper objectMapper = new ObjectMapper();

			auth = objectMapper.readValue(json, ResAuth.class);
		} catch (JsonProcessingException ex) {
			// assume invalid;
			ex.printStackTrace();
		}
		boolean valid = false;
		if (auth != null) {
			// Use this to validate with database
			logger.info("Token: " + auth.getUser() + ",platform:" + auth.getPlatformUUID());
			res.setMeta(auth.getPlatformUUID());
			valid = DeviceManager.validate(auth);
		}

		if (valid) {

			if (lockEngineService.validateDevice(auth.getUser(), auth.getPlatformUUID(), deviceMode)) {
				res.setStatus("valid");
			} else {
				res.setStatus("invalid");
			}

		}else {
			res.setStatus("invalid");
		}
		logger.info("success");
		Date end = new Date();
		logger.info("success: " + ((end.getTime() - start.getTime())/1000F) + "ms.");

		return res;
	}




	@GetMapping("/devicesInfo")
	public ResponseEntity<UserDevice> getDeviceInfo( @RequestParam("deviceId") String deviceId, final Principal principal ) {
		logger.info("Get Device Infomation");
		UserDevice userDevice = lockEngineService.getDeviceInfo(principal.getName(), deviceId);
		return ResponseEntity.ok(userDevice);
	}



	@PostMapping("/searchUser")
	@PreAuthorize("hasAuthority('STAFF') or hasAuthority('ADMIN') or hasAuthority('ZONE')")
	public ResponseEntity<Map<String, Object>>  searchUser( 
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String name,
			@RequestParam(required = false) Character type,
			@RequestParam(required = false) Character status,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "100") int size) {

		logger.info(username);
		logger.info(page);
		logger.info(size);
		List<Users> users = new ArrayList<Users>();
		Pageable paging = PageRequest.of(page, size);

		Page<Users> pageTuts = usersRepository.findUsers(username, name, type, status , paging);
		users = pageTuts.getContent();

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("members", users);
		response.put("currentPage", pageTuts.getNumber());
		response.put("totalItems", pageTuts.getTotalElements());
		response.put("totalPages", pageTuts.getTotalPages());

		return ResponseEntity.ok(response);
	}

	@PostMapping("/searchUserAccessLog")
	@PreAuthorize("hasAuthority('STAFF') or hasAuthority('ADMIN') or hasAuthority('ZONE')")
	public ResponseEntity<Map<String, Object>>  searchUserAccessLog( 
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String name,
			@RequestParam(required = false)  String device,
			@RequestParam(required = false) @DateTimeFormat(pattern="yyyy-mm-dd") Date startDate,
			@RequestParam(required = false) @DateTimeFormat(pattern="yyyy-mm-dd") Date endDate,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "150") int size) {

		//		logger.info(username);
		//		logger.info(page);
		//		logger.info(size);
		//		logger.info(startDate);
		//		logger.info(endDate);

		List<UsersLogging> logs = new ArrayList<UsersLogging>();
		Pageable paging = PageRequest.of(page, size);

		Page<UsersLogging> pageTuts = usersLoggingRepository.searchUserAccessLog(username, name, startDate, endDate ,device, paging);
		logs = pageTuts.getContent();

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("data", logs);
		response.put("currentPage", pageTuts.getNumber());
		response.put("totalItems", pageTuts.getTotalElements());
		response.put("totalPages", pageTuts.getTotalPages());

		return ResponseEntity.ok(response);
	}

	@PostMapping("/exportUserAccessLog")
	@PreAuthorize("hasAuthority('STAFF') or hasAuthority('ADMIN') or hasAuthority('ZONE')")
	public void  exportUserAccessLog( HttpServletResponse response,
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String name,
			@RequestParam(required = false)  String device,
			@RequestParam(required = false) @DateTimeFormat(pattern="yyyy-mm-dd") Date startDate,
			@RequestParam(required = false) @DateTimeFormat(pattern="yyyy-mm-dd") Date endDate,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "150") int size) throws IOException {

		//		List<UsersLogging> logs = new ArrayList<UsersLogging>();
		Pageable paging = PageRequest.of(0, Integer.MAX_VALUE);

		Page<UsersLogging> pageTuts = usersLoggingRepository.searchUserAccessLog(username, name, startDate, endDate ,device, paging);
		//		logs = pageTuts.getContent();
		ByteArrayOutputStream outFile = usageReportService.export(pageTuts.getContent());

		// Set the content type and attachment header.
		response.addHeader("Content-disposition", "attachment;filename=02 - Feb 2021 - Usage.xlsx");
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		// Copy the stream to the response's output stream.
		response.getOutputStream().write(outFile.toByteArray());
		response.flushBuffer();
	}

	
	@GetMapping("/getLicense")
	@PreAuthorize("hasAuthority('STAFF') or hasAuthority('ADMIN') or hasAuthority('ZONE')")
	public ResponseEntity<Map<String, Object>>  getLicense() {
		String json = ApiClient.decodeToken(lockengineLicenseKey, ResAuthToken.PROTOCOL);

		Map<String, Object> response = new HashMap<String, Object>();
		Integer active = usersRepository.countStatusActive();
		Integer inactive = usersRepository.countStatusInActive();


		response.put("active", active);
		response.put("inactive", inactive);
		response.put("license", ResAuth.fromJsonString(json).getPlatformInfo());

		return ResponseEntity.ok(response);

	}
}
