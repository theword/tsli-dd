package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DD_T_PROMOTE_VALIDATE_MAPPING", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "DdTPromoteValidateMapping.findAll", query = "SELECT v FROM DdTPromoteValidateMapping v")
})
public class DdTPromoteValidateMapping implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "VALIDATE_TYPE", length = 64)
    private String validateType;
	@Column(name = "DESC", length = 256)
    private String desc;
	@Column(name = "ORDER")
    private Integer order;
	
	public DdTPromoteValidateMapping() {	
	}

	public DdTPromoteValidateMapping(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof DdTPromoteValidateMapping)) {
            return false;
        }
        DdTPromoteValidateMapping other = (DdTPromoteValidateMapping) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "DdTPromoteValidateMapping [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValidateType() {
		return validateType;
	}

	public void setValidateType(String validateType) {
		this.validateType = validateType;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

}
