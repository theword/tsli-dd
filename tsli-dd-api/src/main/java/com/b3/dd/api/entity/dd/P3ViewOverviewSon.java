package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_OVERVIEW_AGENT_SON", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewOverviewSon.findAll", query = "SELECT v FROM P3ViewOverviewSon v")
})

public class P3ViewOverviewSon implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 18)
    private String closYm;
	@Column(name = "GROUP_CHANNEL", length = 20)
    private String groupChannel;
	@Column(name = "ZONE", length = 20)
    private String zone;
	
	@Column(name = "GM_CODE", length = 20)
    private String gmCode;
	@Column(name = "GM_POSITION", length = 300)
    private String gmPosition;
	@Column(name = "GM_NAME", length = 300)
    private String gmName;
	
	@Column(name = "AVP_CODE", length = 20)
    private String avpCode;
	@Column(name = "AVP_POSITION", length = 300)
    private String avpPosition;
	@Column(name = "AVP_NAME", length = 300)
    private String avpName;
	
	@Column(name = "AL_CODE", length = 20)
    private String alCode;
	@Column(name = "AL_POSITION", length = 300)
    private String alPosition;
	@Column(name = "AL_NAME", length = 300)
    private String alName;
	
	@Column(name = "AGENT_CODE", length = 25)
    private String agentCode;
	@Column(name = "AG_POSITION", length = 300)
    private String agPosition;
	@Column(name = "AGENT_LEVEL", length = 5)
    private String agentLevel;
	@Column(name = "LEVEL_NAME", length = 20)
    private String levelName;
	@Column(name = "SON_CODE", length = 25)
    private String sonCode;
	@Column(name = "SON_NAME", length = 300)
    private String sonName;
	
	@Column(name = "TARGET_ACT")
    private Double targetAct;
	@Column(name = "TARGET_NC")
    private Double targetNc;
	@Column(name = "TARGET_PC")
    private Double targetPc;
	@Column(name = "TARGET_RT")
    private Double targetRt;
	@Column(name = "TARGET_RYP")
    private Double targetRyp;
	
	@Column(name = "PC_SUBMIT")
    private Double pcSubmit;
	@Column(name = "PC_APPROVE")
    private Double pcApprove;

	@Column(name = "NC")
    private Double nc;
	@Column(name = "ACTIVE")
    private Double active;
	@Column(name = "RYP")
    private Double ryp;
	@Column(name = "PRST_RTE")
    private Double prstRte;
	
	@Column(name = "PC_PERCENT")
    private Double pcPercent;
	@Column(name = "NC_PERCENT")
    private Double ncPercent;
	@Column(name = "ACTIVE_PERCENT")
    private Double activePercent;
	@Column(name = "RYP_PERCENT")
    private Double rypPercent;
	
	public P3ViewOverviewSon() {	
	}

	public P3ViewOverviewSon(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewOverviewSon)) {
            return false;
        }
        P3ViewOverviewSon other = (P3ViewOverviewSon) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getGmPosition() {
		return gmPosition;
	}

	public void setGmPosition(String gmPosition) {
		this.gmPosition = gmPosition;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getAvpCode() {
		return avpCode;
	}

	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}

	public String getAvpPosition() {
		return avpPosition;
	}

	public void setAvpPosition(String avpPosition) {
		this.avpPosition = avpPosition;
	}

	public String getAvpName() {
		return avpName;
	}

	public void setAvpName(String avpName) {
		this.avpName = avpName;
	}

	public String getAlCode() {
		return alCode;
	}

	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}

	public String getAlPosition() {
		return alPosition;
	}

	public void setAlPosition(String alPosition) {
		this.alPosition = alPosition;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgPosition() {
		return agPosition;
	}

	public void setAgPosition(String agPosition) {
		this.agPosition = agPosition;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getSonCode() {
		return sonCode;
	}

	public void setSonCode(String sonCode) {
		this.sonCode = sonCode;
	}

	public Double getTargetAct() {
		return targetAct;
	}

	public void setTargetAct(Double targetAct) {
		this.targetAct = targetAct;
	}

	public Double getTargetNc() {
		return targetNc;
	}

	public void setTargetNc(Double targetNc) {
		this.targetNc = targetNc;
	}

	public Double getTargetPc() {
		return targetPc;
	}

	public void setTargetPc(Double targetPc) {
		this.targetPc = targetPc;
	}

	public Double getTargetRt() {
		return targetRt;
	}

	public void setTargetRt(Double targetRt) {
		this.targetRt = targetRt;
	}

	public Double getTargetRyp() {
		return targetRyp;
	}

	public void setTargetRyp(Double targetRyp) {
		this.targetRyp = targetRyp;
	}

	public Double getPcSubmit() {
		return pcSubmit;
	}

	public void setPcSubmit(Double pcSubmit) {
		this.pcSubmit = pcSubmit;
	}

	public Double getPcApprove() {
		return pcApprove;
	}

	public void setPcApprove(Double pcApprove) {
		this.pcApprove = pcApprove;
	}

	public Double getNc() {
		return nc;
	}

	public void setNc(Double nc) {
		this.nc = nc;
	}

	public Double getActive() {
		return active;
	}

	public void setActive(Double active) {
		this.active = active;
	}

	public Double getRyp() {
		return ryp;
	}

	public void setRyp(Double ryp) {
		this.ryp = ryp;
	}

	public Double getPrstRte() {
		return prstRte;
	}

	public void setPrstRte(Double prstRte) {
		this.prstRte = prstRte;
	}

	public Double getPcPercent() {
		return pcPercent;
	}

	public void setPcPercent(Double pcPercent) {
		this.pcPercent = pcPercent;
	}

	public Double getNcPercent() {
		return ncPercent;
	}

	public void setNcPercent(Double ncPercent) {
		this.ncPercent = ncPercent;
	}

	public Double getActivePercent() {
		return activePercent;
	}

	public void setActivePercent(Double activePercent) {
		this.activePercent = activePercent;
	}

	public Double getRypPercent() {
		return rypPercent;
	}

	public void setRypPercent(Double rypPercent) {
		this.rypPercent = rypPercent;
	}

	public String getSonName() {
		return sonName;
	}

	public void setSonName(String sonName) {
		this.sonName = sonName;
	}

}
