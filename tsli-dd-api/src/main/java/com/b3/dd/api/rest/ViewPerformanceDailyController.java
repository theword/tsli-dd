package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewPerformanceDaily;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewPerformanceDailyRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class ViewPerformanceDailyController {
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private ViewPerformanceDailyRepository ViewPerformanceDailyRepository;
	
	@PostMapping("performance-daily")
	public ResponseEntity<List<ViewPerformanceDaily>> 
	getAgentPerformanceDaily(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		
			List<ViewPerformanceDaily> viewPerformanceDaily = ViewPerformanceDailyRepository.findByAgentCode(
				request.getAgentCode(), request.getAgentLevel(), principal.getName());
				
			return ResponseEntity.ok().body(viewPerformanceDaily);
	}
	
	@PostMapping("performance-daily-company")
	public ResponseEntity<List<ViewPerformanceDaily>> 
	getPerformanceDailyCompany(@RequestBody RequestModel request)
		throws ResourceClosedException {
			List<ViewPerformanceDaily> viewPerformanceDaily = ViewPerformanceDailyRepository.findByCompany(
					request.getAgentLevel(), userHelper.getUserPosition());
				
			return ResponseEntity.ok().body(viewPerformanceDaily);
	}
	
	@PostMapping("performance-daily-channel")
	public ResponseEntity<List<ViewPerformanceDaily>> 
	getPerformanceDailyChannel(@RequestBody RequestModel request)
		throws ResourceClosedException {
		
			List<ViewPerformanceDaily> viewPerformanceDaily = ViewPerformanceDailyRepository.findByChannel(
					request.getChannelCode(), request.getAgentLevel(), userHelper.getUserPosition());
				
			return ResponseEntity.ok().body(viewPerformanceDaily);
	}
	
	@PostMapping("performance-daily-zone")
	public ResponseEntity<List<ViewPerformanceDaily>> 
	getPerformanceDailyZone(@RequestBody RequestModel request)
		throws ResourceClosedException {
		
			List<ViewPerformanceDaily> viewPerformanceDaily = ViewPerformanceDailyRepository.findByZone(
					request.getZoneCode(), request.getAgentLevel(), userHelper.getUserPosition());
				
			return ResponseEntity.ok().body(viewPerformanceDaily);
	}
}
