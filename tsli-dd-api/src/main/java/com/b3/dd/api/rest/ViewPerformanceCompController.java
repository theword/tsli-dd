package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewPerformanceComp;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewPerformanceCompRepository;

@RestController
@RequestMapping("/${api.prefix}/company")
public class ViewPerformanceCompController {

	@Autowired
	private ViewPerformanceCompRepository ViewPerformanceCompRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.
	@PostMapping("/performance/company")
	public ResponseEntity<List<ViewPerformanceComp>> 
	getCompanyPerformance()
	throws ResourceClosedException {
		List<ViewPerformanceComp> viewPerformanceComp = ViewPerformanceCompRepository.findByCompany(
				userHelper.getUserPosition());
		return ResponseEntity.ok().body(viewPerformanceComp);
	}
	
	// CASE II.
	@PostMapping("/performance/channel")
	public ResponseEntity<List<ViewPerformanceComp>> 
	getChannelPerformance(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewPerformanceComp> viewPerformanceComp = ViewPerformanceCompRepository.findByChannel(
				request.getChannelCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(viewPerformanceComp);
	}
		
	// CASE III.
	@PostMapping("/performance/zone")
	public ResponseEntity<List<ViewPerformanceComp>> 
	getZonePerformance(@RequestBody RequestModel request)
	throws ResourceClosedException {
		List<ViewPerformanceComp> viewPerformanceComp = ViewPerformanceCompRepository.findByZone(
				request.getZoneCode(), userHelper.getUserPosition());
		return ResponseEntity.ok().body(viewPerformanceComp);
	}
}
