package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewPerDetail;
import com.b3.dd.api.model.PaggingModel;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewPerDetailRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewPerDetailController {
	private int PAGE_SIZE = 100;
	@Autowired
	private P3ViewPerDetailRepository P3ViewPerDetailRepository;
	@Autowired
	private UserHelper userHelper;
	
	private  Predicate<P3ViewPerDetail> searchByKeywords(String keyword) {
		return t -> (t.getAvpCode() != null && t.getAvpCode().contains(keyword))
				|| (t.getAvpName() != null && t.getAvpName().contains(keyword))
				|| (t.getAlCode() != null && t.getAlCode().contains(keyword))
				|| (t.getAlName() != null && t.getAlName().contains(keyword))
				|| (t.getAgentCode() != null && t.getAgentCode().contains(keyword))
				|| (t.getAgentName() != null && t.getAgentName().contains(keyword))
				|| (t.getPolicyCode() != null && t.getPolicyCode().contains(keyword))
				|| (t.getCustomerName() != null && t.getCustomerName().contains(keyword))
				|| (t.getContYmd() != null && t.getContYmd().contains(keyword));
	}
	
	private static List<P3ViewPerDetail> filterByKeywords (List<P3ViewPerDetail> p3ViewPerDetail, Predicate<P3ViewPerDetail> predicate) {
		return p3ViewPerDetail.stream()
				.filter(predicate)
				.collect(Collectors.<P3ViewPerDetail>toList());
	}
	
	// CASE I.I - Find by AGent code
	@PostMapping("persistency/detail-agent")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getAgPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending()
					);
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending()
					);
		}
		else {
			PAGE_SIZE = 100;
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by("closYm").descending()
					);			
		}
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByAgentCode(
			request.getFilter(), request.getAgentCode()
			, principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				P3ViewPerDetail.getContent()
				, P3ViewPerDetail.getTotalPages()
				, P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE I.II - Find by AL code
	@PostMapping("persistency/detail-al")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getAlPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending()
					);
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending()
					);
		}
		else {
			PAGE_SIZE = 100;
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by("closYm").descending()
					);			
		}
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByAlCode(
			request.getFilter(), request.getAgentCode()
			, principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				P3ViewPerDetail.getContent()
				, P3ViewPerDetail.getTotalPages()
				, P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE I.III - Find by AVP code
	@PostMapping("persistency/detail-avp")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getAvpPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending()
					);
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending()
					);
		}
		else {
			PAGE_SIZE = 100;
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by("closYm").descending()
					);			
		}

		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByAvpCode(
			request.getFilter(), request.getAgentCode()
			, principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				P3ViewPerDetail.getContent()
				, P3ViewPerDetail.getTotalPages()
				, P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE I.IV - Find by GM code
	@PostMapping("persistency/detail-gm")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getGmPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		
		Pageable pagging;
		if (request.getSortOrderType().equalsIgnoreCase("desc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).descending()
					);
		}
		else if (request.getSortOrderType().equalsIgnoreCase("asc")) {
			PAGE_SIZE = 100 * (request.getPaginationPage() + 1);
			pagging = PageRequest.of(0
					, PAGE_SIZE
					, Sort.by(request.getSortColName()).ascending()
					);
		}
		else {
			PAGE_SIZE = 100;
			pagging = PageRequest.of(request.getPaginationPage()
					, PAGE_SIZE
					, Sort.by("closYm").descending()
					);			
		}
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByGmCode(
			request.getFilter(), request.getAgentCode()
			, principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				P3ViewPerDetail.getContent()
				, P3ViewPerDetail.getTotalPages()
				, P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE II.I - Find by Search on GM
	@PostMapping("persistency/detail-gm-search")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getSearchGmPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by("closYm").descending());
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByGmCode(
			request.getFilter(), request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		List<P3ViewPerDetail> res = null;
		if (!StringUtils.isEmpty(request.getSearchKeyword())) {
			final String keywords = request.getSearchKeyword().replace("%", "");
			res = filterByKeywords(P3ViewPerDetail.getContent(), searchByKeywords(keywords));
		}
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				res, P3ViewPerDetail.getTotalPages(), P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE II.II - Find by Search on AVP
	@PostMapping("persistency/detail-avp-search")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getSearchAvpPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by("closYm").descending());
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByAvpCode(
			request.getFilter(), request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		List<P3ViewPerDetail> res = null;
		if (!StringUtils.isEmpty(request.getSearchKeyword())) {
			final String keywords = request.getSearchKeyword().replace("%", "");
			res = filterByKeywords(P3ViewPerDetail.getContent(), searchByKeywords(keywords));
		}
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				res, P3ViewPerDetail.getTotalPages(), P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE II.III - Find by Search on AL
	@PostMapping("persistency/detail-al-search")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getSearchAlPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by("closYm").descending());
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByAlCode(
			request.getFilter(), request.getAgentCode(), principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		List<P3ViewPerDetail> res = null;
		if (!StringUtils.isEmpty(request.getSearchKeyword())) {
			final String keywords = request.getSearchKeyword().replace("%", "");
			res = filterByKeywords(P3ViewPerDetail.getContent(), searchByKeywords(keywords));
		}
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				res, P3ViewPerDetail.getTotalPages(), P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
	// CASE II.IV - Find by Search on AG
	@PostMapping("persistency/detail-agent-search")
	public ResponseEntity<PaggingModel<P3ViewPerDetail>> 
	getSearchAgPerDetail(@RequestBody RequestModel request, final Principal principal)
	throws ResourceClosedException {
		Pageable pagging = PageRequest.of(request.getPaginationPage(), PAGE_SIZE, Sort.by("closYm").descending());
		
		Page<P3ViewPerDetail> P3ViewPerDetail = P3ViewPerDetailRepository.findByAgentCode(
			request.getFilter(), request.getAgentCode(),  principal.getName(), userHelper.getUserPosition(), pagging
		);
		
		List<P3ViewPerDetail> res = null;
		if (!StringUtils.isEmpty(request.getSearchKeyword())) {
			final String keywords = request.getSearchKeyword().replace("%", "");
			res = filterByKeywords(P3ViewPerDetail.getContent(), searchByKeywords(keywords));
		}
		
		return ResponseEntity.ok().body( new PaggingModel<P3ViewPerDetail>(
				res, P3ViewPerDetail.getTotalPages(), P3ViewPerDetail.getTotalElements()
			)
		);
	}
	
}
