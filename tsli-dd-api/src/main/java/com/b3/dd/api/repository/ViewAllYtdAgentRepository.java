package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewAllYtdAgent;

@Repository
public interface ViewAllYtdAgentRepository extends CrudRepository<ViewAllYtdAgent, Integer> {
	
	// CASE I: Current year
	@Query(value="SELECT * FROM VIEW_ALL_YTD_AGENT WHERE AGENT_CODE = :agentCode"
			+ " AND AGENT_LEVEL = :agentLevel"
			+ " AND SUBSTR(CLOS_YM, 1, 4) = (SELECT (SUBSTR(MIN(CLOS_YM),1 ,4)) AS MIN_CLOS_YM"
			+ "				FROM DD_T_CLOS_CALENDAR"
			+ "				WHERE FLAG_END_CLOSING_YMD = 'F'"
			+ "		)"
			+ " AND ("
			+ "			(GM_CODE = :authCode OR AVP_CODE = :authCode OR AL_CODE = :authCode OR AGENT_CODE = :authCode)"
			+ "		OR "
			+ "			((SELECT AGENT_LEVEL FROM VIEW_AGENT_STRUCTURE WHERE AGENT_CODE = :authCode) = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
	, nativeQuery=true)
	public List<ViewAllYtdAgent> findByAgentCurrentYear(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
	);
	
	// CASE II: Current and last year
	@Query(value="SELECT v FROM ViewAllYtdAgent v WHERE v.agentCode = :agentCode"
			+ " AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "	(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "	OR "
			+ "	((SELECT va.agentLevel FROM ViewAgentStructure va WHERE va.agentCode = :authCode) = 'ZONE')"
			+ "	OR"
			+ "	(:authCode IN (SELECT v2.agentCode "
			+ "		FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "	)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
	, nativeQuery=true)
	public List<ViewAllYtdAgent> findByAgentCurrentAndLastYear(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode);
	
}
