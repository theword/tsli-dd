/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "OAUTH_ACCESS_TOKEN", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OauthAccessToken.findAll", query = "SELECT o FROM OauthAccessToken o")
    , @NamedQuery(name = "OauthAccessToken.findByTokenId", query = "SELECT o FROM OauthAccessToken o WHERE o.tokenId = :tokenId")
    , @NamedQuery(name = "OauthAccessToken.findByAuthenticationId", query = "SELECT o FROM OauthAccessToken o WHERE o.authenticationId = :authenticationId")
    , @NamedQuery(name = "OauthAccessToken.findByUserName", query = "SELECT o FROM OauthAccessToken o WHERE o.userName = :userName")
    , @NamedQuery(name = "OauthAccessToken.findByClientId", query = "SELECT o FROM OauthAccessToken o WHERE o.clientId = :clientId")
    , @NamedQuery(name = "OauthAccessToken.findByRefreshToken", query = "SELECT o FROM OauthAccessToken o WHERE o.refreshToken = :refreshToken")})
public class OauthAccessToken implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TOKEN_ID", nullable = false, length = 256)
    private String tokenId;
    @Lob
    @Column(name = "TOKEN")
    private Serializable token;
    @Column(name = "AUTHENTICATION_ID", length = 256)
    private String authenticationId;
    @Column(name = "USER_NAME", length = 256)
    private String userName;
    @Column(name = "CLIENT_ID", length = 256)
    private String clientId;
    @Lob
    @Column(name = "AUTHENTICATION")
    private Serializable authentication;
    @Column(name = "REFRESH_TOKEN", length = 256)
    private String refreshToken;

    public OauthAccessToken() {
    }

    public OauthAccessToken(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public Serializable getToken() {
        return token;
    }

    public void setToken(Serializable token) {
        this.token = token;
    }

    public String getAuthenticationId() {
        return authenticationId;
    }

    public void setAuthenticationId(String authenticationId) {
        this.authenticationId = authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Serializable getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Serializable authentication) {
        this.authentication = authentication;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tokenId != null ? tokenId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OauthAccessToken)) {
            return false;
        }
        OauthAccessToken other = (OauthAccessToken) object;
        if ((this.tokenId == null && other.tokenId != null) || (this.tokenId != null && !this.tokenId.equals(other.tokenId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsli.dd.entity.OauthAccessToken[ tokenId=" + tokenId + " ]";
    }
    
}
