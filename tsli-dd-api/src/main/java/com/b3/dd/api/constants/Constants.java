package com.b3.dd.api.constants;

public class Constants {

	public static class UserStatus {
		public static final Character ACTIVE = 'A';
		public static final Character INACTIVE = 'I';
		
		public static final String ACTIVE_STR = "A";
		public static final String INACTIVE_STR = "I";
		
	}
	public static class DeleteStatus{
		public static final String TRUE = "T";
		public static final String FALSE = "F";
	}
	public static class UploadStatus{
		public static final String SUCCESS = "S";
		public static final String FAIL = "F";
	}
	
}
