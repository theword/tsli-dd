/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.ColumnDefault;

import com.b3.dd.api.constants.Constants;

/**
 *
 * @author user
 */
@Entity
@Table(name = "DD_T_PROMOTION_CONFIG", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DdTPromotionConfig.findAll", query = "SELECT d FROM DdTPromotionConfig d")
    , @NamedQuery(name = "DdTPromotionConfig.findById", query = "SELECT d FROM DdTPromotionConfig d WHERE d.id = :id")
    , @NamedQuery(name = "DdTPromotionConfig.findByFileName", query = "SELECT d FROM DdTPromotionConfig d WHERE d.fileName = :fileName")
    , @NamedQuery(name = "DdTPromotionConfig.findByFilePath", query = "SELECT d FROM DdTPromotionConfig d WHERE d.filePath = :filePath")
    , @NamedQuery(name = "DdTPromotionConfig.findByStatus", query = "SELECT d FROM DdTPromotionConfig d WHERE d.status = :status")
    , @NamedQuery(name = "DdTPromotionConfig.findByCreateDate", query = "SELECT d FROM DdTPromotionConfig d WHERE d.createDate = :createDate")
    , @NamedQuery(name = "DdTPromotionConfig.findByCreateBy", query = "SELECT d FROM DdTPromotionConfig d WHERE d.createBy = :createBy")
    , @NamedQuery(name = "DdTPromotionConfig.findByUpdateDate", query = "SELECT d FROM DdTPromotionConfig d WHERE d.updateDate = :updateDate")
    , @NamedQuery(name = "DdTPromotionConfig.findByUpdateBy", query = "SELECT d FROM DdTPromotionConfig d WHERE d.updateBy = :updateBy")
    , @NamedQuery(name = "DdTPromotionConfig.findByIsDelete", query = "SELECT d FROM DdTPromotionConfig d WHERE d.isDelete = :isDelete")})
public class DdTPromotionConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 32, scale = 0, insertable = true, updatable = true)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "FILE_NAME", nullable = false, length = 1024)
    private String fileName;
    @Column(name = "FILE_PATH",  length = 1024)
    private String filePath;
    @Basic(optional = false)
    @Lob
    @Column(name = "header", nullable = false)
    private String header;
    @Column(name = "STATUS", length = 1024)
    @ColumnDefault(Constants.UploadStatus.SUCCESS)
    private String status;
    @Lob
    @Column(name = "STATUS_DETAIL")
    private String statusDetail;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY", length = 255)
    private String createBy;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY", length = 255)
    private String updateBy;
    @ColumnDefault(Constants.DeleteStatus.FALSE)
    @Column(name = "IS_DELETE", length = 1)
    private String isDelete;
    @Column(name = "TYPE", length = 1024)
    private String type;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promotionConfigId", fetch = FetchType.EAGER )
    private List<DdTPromotionDetail> ddTPromotionDetailList;

    public DdTPromotionConfig() {
    }

    public DdTPromotionConfig(BigDecimal id) {
        this.id = id;
    }

    public DdTPromotionConfig(BigDecimal id, String fileName, String filePath, String header, Date createDate) {
        this.id = id;
        this.fileName = fileName;
        this.filePath = filePath;
        this.header = header;
        this.createDate = createDate;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlTransient
    public List<DdTPromotionDetail> getDdTPromotionDetailList() {
        return ddTPromotionDetailList;
    }

    public void setDdTPromotionDetailList(List<DdTPromotionDetail> ddTPromotionDetailList) {
        this.ddTPromotionDetailList = ddTPromotionDetailList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTPromotionConfig)) {
            return false;
        }
        DdTPromotionConfig other = (DdTPromotionConfig) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.b3.dd.api.entity.DdTPromotionConfig[ id=" + id + " ]";
    }
    
}
