package com.b3.dd.api.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.b3.dd.api.model.ClosYmModel;

@Service
public class ClosYmService {
	private static final Logger log = LogManager.getLogger(ScheduleService.class);
	private static final String sql = "SELECT MIN(CLOS_YM) AS CLOS_YM\r\n" + 
			"FROM DD_T_CLOS_CALENDAR\r\n" + 
			"WHERE FLAG_END_CLOSING_YMD = 'F'";
	private ClosYmModel closYm;
	
	@PersistenceContext(unitName="ddDatasource")
	private EntityManager em;
	
//	public ClosYmService(EntityManager em) {
	public ClosYmService() {
		super();
//		this.em = em;
		closYm = new ClosYmModel();
	}
	
	@Scheduled(fixedRate = 60000L)
	public void closYmCalendarRefresh() {
		refreshClosYmCalendar();
	}
	
	public synchronized void refreshClosYmCalendar() {
		readClosYmCalendar();
	};
	
	private void readClosYmCalendar() {
		try {
			String closYmData = (String) em.createNativeQuery(sql).getSingleResult();
			closYm.setClosYm(closYmData);
		} catch (Exception e) {
			log.warn("ClosYm Refresh error.");
		}
	}
	
	public ClosYmModel getClosYm() {
		return closYm;
	}
	
	public void setClosYm(ClosYmModel closYm) {
		this.closYm = closYm;
	}
}
