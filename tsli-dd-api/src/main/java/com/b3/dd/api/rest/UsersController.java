package com.b3.dd.api.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.Users;
import com.b3.dd.api.service.UserService;

@RestController
@RequestMapping("/${api.prefix}/users")
public class UsersController {

	@Autowired
	private UserService userService;

//	@GetMapping("/devices")
//	public ResponseEntity<List<UserDevice>> getDevices(final Principal principal) {
//		List<UserDevice> devices = userService.getDevices(principal.getName());
//		return ResponseEntity.ok(devices);
//	}

	@PostMapping("/search")
	@PreAuthorize("hasRole('staff')")
	public Page<Users> getUser(@RequestBody Users user,
			@RequestParam(value = "offset", defaultValue = "0") Integer page,
			@RequestParam(value = "limit", defaultValue = "20") Integer size) {
		
		return userService.getUsers(user, page, size);
	}

}