package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewOverviewYtdAgent;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewOverviewYtdAgentRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewOverviewYtdAgentController {
	@Autowired
	private P3ViewOverviewYtdAgentRepository P3ViewOverviewYtdAgentRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - I.III : Under Producer
	@PostMapping("teamdetail-agent-ytd")
	public ResponseEntity<List<P3ViewOverviewYtdAgent>>
	getTeamDetailAgentYtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String level = request.getFilter();
		List<P3ViewOverviewYtdAgent> P3ViewOverviewYtdAgent = null;
		
		switch (level) {
		case "AL":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderAl(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm()
				, principal.getName(), userHelper.getUserPosition() 
			);
			break;
		case "AVP":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderAvp(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "GM":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderGm(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm()
				, principal.getName(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewOverviewYtdAgent);
	}
	
	// CASE II.I : Under Zone
	@PostMapping("teamdetail-zone-ytd")
	public ResponseEntity<List<P3ViewOverviewYtdAgent>>
	getTeamDetailZoneYtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<P3ViewOverviewYtdAgent> P3ViewOverviewYtdAgent = null;
		P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderZone(
			request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition() 
		);
		
		return ResponseEntity.ok().body(P3ViewOverviewYtdAgent);
	}
	
	// CASE III.I - III.IV : Under Channel
	@PostMapping("teamdetail-channel-ytd")
	public ResponseEntity<List<P3ViewOverviewYtdAgent>>
	getTeamDetailChannelYtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String filter = request.getFilter();
		List<P3ViewOverviewYtdAgent> P3ViewOverviewYtdAgent = null;
		
		switch (filter) {
		case "PC":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderChannelPc(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition() 
			);
			break;
		case "ACTIVE":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderChannelActive(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition()
			);
			break;
		case "NC":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderChannelNc(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition()
			);
			break;
		case "RYP":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderChannelRyp(
				request.getAgentCode(), request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewOverviewYtdAgent);
	}
	
	// CASE IV.I - IV.IV : Under Company
	@PostMapping("teamdetail-company-ytd")
	public ResponseEntity<List<P3ViewOverviewYtdAgent>>
	getTeamDetailCompanyYtd(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String filter = request.getFilter();
		List<P3ViewOverviewYtdAgent> P3ViewOverviewYtdAgent = null;
		
		switch (filter) {
		case "PC":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderCompanyPc(
				request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition() 
			);
			break;
		case "ACTIVE":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderCompanyActive(
				request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition()
			);
			break;
		case "NC":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderCompanyNc(
				request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition()
			);
			break;
		case "RYP":
			P3ViewOverviewYtdAgent = P3ViewOverviewYtdAgentRepository.findByUnderCompanyRyp(
				request.getAgentLevel(), request.getClosYm(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewOverviewYtdAgent);
	}
}
