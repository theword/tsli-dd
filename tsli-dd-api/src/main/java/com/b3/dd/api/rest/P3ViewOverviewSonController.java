package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewOverviewSon;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewOverviewSonRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewOverviewSonController {
	@Autowired
	private P3ViewOverviewSonRepository P3ViewOverviewSonRepository;
	@Autowired
	private UserHelper userHelper;
	
	// CASE I.I - I.IV
	@PostMapping("teamdetail-agent-by-son")
	public ResponseEntity<List<P3ViewOverviewSon>>
	getGmPromotionDetail(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String filter = request.getFilter();
		List<P3ViewOverviewSon> P3ViewOverviewSon = null;
		
		switch (filter) {
		case "PC":
			P3ViewOverviewSon = P3ViewOverviewSonRepository.findByLevelWithPc(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition() 
			);
			break;
		case "ACTIVE":
			P3ViewOverviewSon = P3ViewOverviewSonRepository.findByLevelWithActive(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "NC":
			P3ViewOverviewSon = P3ViewOverviewSonRepository.findByLevelWithNewCode(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
			);
			break;
		case "RYP":
			P3ViewOverviewSon = P3ViewOverviewSonRepository.findByLevelWithRyp(
				request.getAgentCode(), request.getAgentLevel(), principal.getName(), userHelper.getUserPosition()
			);
			break;
		default:
			break;
		}
		
		return ResponseEntity.ok().body(P3ViewOverviewSon);
	}
}
