package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.entity.dd.DdTUserRoles;


public interface UserRolesRepository extends JpaRepository<DdTUserRoles, Integer> {

	@Query(value = "SELECT d.roleId.role FROM DdTUserRoles d WHERE d.username = :username")
	public List<String> getRoles(@Param("username") String username);

}
