package com.b3.dd.api.model;

import java.util.List;

public class PaggingModel<T> {

	private List<T> content;
	private Integer totalPage;
	private Long totalElements;

	public PaggingModel() {
		super();
	}

	public PaggingModel(List<T> content, Integer totalPage, Long totalElements) {
		super();
		this.content = content;
		this.totalPage = totalPage;
		this.totalElements = totalElements;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

}
