package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DD_T_AGENT_SON_MAPPING", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "DdTAgentSonMapping.findAll", query = "SELECT v FROM DdTAgentSonMapping v")
})
public class DdTAgentSonMapping implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 6)
    private String closYm;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "CHANNEL_NAME", length = 30)
    private String channelName;
	
	@Column(name = "AGENT_LEVEL", length = 5)
    private String agentLevel;
	@Column(name = "AGENT_CODE", length = 25)
    private String agentCode;
	@Column(name = "SON_CODE", length = 25)
    private String sonCode;
	@Column(name = "SON_LEVEL", length = 25)
    private String sonLevel;
	
	@Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;
	@Column(name = "CREATE_BY", length = 10)
    private String createBy;
	@Column(name = "UPDATE_DATE", nullable = false)
    private Date updateDate;
	@Column(name = "UPDATE_BY", length = 10)
    private String updateBy;
	
	public DdTAgentSonMapping() {	
	}

	public DdTAgentSonMapping(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getSonCode() {
		return sonCode;
	}

	public void setSonCode(String sonCode) {
		this.sonCode = sonCode;
	}

	public String getSonLevel() {
		return sonLevel;
	}

	public void setSonLevel(String sonLevel) {
		this.sonLevel = sonLevel;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof DdTAgentSonMapping)) {
            return false;
        }
        DdTAgentSonMapping other = (DdTAgentSonMapping) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

}
