package com.b3.dd.api.rest;

import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.DdTAgentStructure;
import com.b3.dd.api.repository.AgentStructureRepository;

@RestController
@RequestMapping("/${api.prefix}/agentdetail")
public class AgentStructureController {
	
	@Autowired
	private AgentStructureRepository agentStructureRepository;
	
	@GetMapping("/{agentCode}")
	public ResponseEntity<List<DdTAgentStructure>> findByAgentCode(@PathVariable(value = "agentCode") String agentCode)
	throws ResourceClosedException {
		List<DdTAgentStructure> ddTAgentStructure = agentStructureRepository.findByAgentCode(agentCode);
		return ResponseEntity.ok().body(ddTAgentStructure);
	}
	
}
