package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewWholeGroupMtd;

@Repository
public interface P3ViewWholeGroupMtdRepository extends CrudRepository<P3ViewWholeGroupMtd, Integer> {

	// CASE I.I - AGENT find by AGENT_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_WHOLEGROUP_MTD"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ " ORDER BY CLOS_YM ASC", nativeQuery = true)
	public List<P3ViewWholeGroupMtd> findByAgent(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.II - AL find by AGENT_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_WHOLEGROUP_MTD"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AGENT_CODE = :authCode"
			+ "				OR AL_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY CLOS_YM ASC", nativeQuery = true)
	public List<P3ViewWholeGroupMtd> findByAl(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.III - AVP find by AGENT_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_WHOLEGROUP_MTD"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AGENT_CODE = :authCode"
			+ "				OR AVP_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ " ORDER BY CLOS_YM ASC", nativeQuery = true)
	public List<P3ViewWholeGroupMtd> findByAvp(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
	
	// CASE I.IV - GM find by AGENT_CODE
	@Query(value=""
			+ "SELECT * FROM VIEW_WHOLEGROUP_MTD"
			+ " WHERE AGENT_CODE = :agentCode"
			+ "	AND AGENT_LEVEL = :agentLevel"
			+ "	AND ("
			+ "			("
			+ "				AGENT_CODE = :authCode"
			+ "				OR GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT stb.AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING stb"
			+ "				WHERE stb.SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT stb.SON_CODE FROM DD_T_AGENT_SON_MAPPING stb WHERE stb.AGENT_CODE = :authCode)"
			+ "		)"
			+ "	)"
			+ "	ORDER BY CLOS_YM ASC", nativeQuery = true)
	public List<P3ViewWholeGroupMtd> findByGm(
			@Param("agentCode") String agentCode
			, @Param("agentLevel") String agentLevel
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel);
}
