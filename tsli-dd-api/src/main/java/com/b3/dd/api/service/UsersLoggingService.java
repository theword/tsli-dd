package com.b3.dd.api.service;


import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.b3.dd.api.entity.dd.DdTAgentStructure;
import com.b3.dd.api.entity.dd.Users;
import com.b3.dd.api.entity.dd.UsersLogging;
import com.b3.dd.api.entity.dd.Users.UserType;
import com.b3.dd.api.entity.dd.UsersLogging.ActionType;
import com.b3.dd.api.utils.ClientInfo;

@Service
public class UsersLoggingService {

	private static final Logger log = LogManager.getLogger(UsersLoggingService.class);

	@PersistenceContext
	private EntityManager em;

	
	@Transactional
	public UsersLogging saveLogging(HttpServletRequest request, String username, String status,
			ActionType actionType) {
		Users user = this.em.createQuery("FROM Users u WHERE u.username = :username", Users.class)
		.setParameter("username", username)
		.getResultList()
		.stream()
		.findFirst()
		.get();
		
		return this.saveLogging(request, user, status, actionType);
		
	}
	
	@Transactional
	public UsersLogging saveLogging(HttpServletRequest request, Users user, String status,
			ActionType actionType) {
		
		try {
//		String clientBrowser = ClientInfo.getClientBrowser(request), clientIpAddr = ClientInfo.getClientIpAddr(request),
//				clientOs = ClientInfo.getClientOS(request), clientUserAgent = ClientInfo.getUserAgent(request);
		
		log.debug("Save access log, " + user.getUsername() + " > " + actionType);
//		log.debug("Client Access browser: " + clientBrowser);
//		log.debug("Client Access ip address: " + clientIpAddr);
//		log.debug("Client Access OS: " + clientOs);


		
		DdTAgentStructure structure = this.em.createQuery("FROM DdTAgentStructure d WHERE d.agentCode = :agentCode", DdTAgentStructure.class)
		.setParameter("agentCode", user.getUsername())
		.getResultList()
	    .stream()
	    .findFirst()
	    .get();
		
		
		UsersLogging logging = new UsersLogging(user.getName() + " " + user.getSurname(), user.getUsername(), status, actionType, Calendar.getInstance().getTime());
		logging.setInfo1( UserType.PRODUCER.equals(user.getUserType())?"AGENT":"STAFF" );
		
		if(structure != null) {
			logging.setInfo2( structure.getAgentLevel());
			logging.setInfo3( structure.getGroupChannel());
			logging.setInfo4( structure.getZoneCode() );
			logging.setInfo5( structure.getZoneName());
		}
		
		return this.em.merge(logging);
		
		}catch (Exception e) {
			// TODO: handle exception
		}		
		return null;
	}

}
