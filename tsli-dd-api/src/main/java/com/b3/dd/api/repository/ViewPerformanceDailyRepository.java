package com.b3.dd.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.ViewPerformanceDaily;

@Repository
public interface ViewPerformanceDailyRepository extends CrudRepository<ViewPerformanceDaily, Integer> {

	@Query("SELECT v FROM ViewPerformanceDaily v WHERE v.agentCode = :agentCode"
			+ " AND v.agentLevel = :agentLevel"
			+ " AND ("
			+ "			(v.gmCode = :authCode OR v.avpCode = :authCode OR v.alCode = :authCode OR v.agentCode = :authCode)"
			+ "		OR "
			+ "			((SELECT va.agentLevel FROM ViewAgentStructure va WHERE va.agentCode = :authCode) = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT v2.agentCode "
			+ "			FROM DdTAgentSonMapping v2 WHERE v2.sonCode = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			v.gmCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.avpCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.alCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "			OR v.agentCode IN (SELECT v3.sonCode FROM DdTAgentSonMapping v3 WHERE v3.agentCode = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY v.dayOfMonth")
	public List<ViewPerformanceDaily> findByAgentCode(
		@Param("agentCode") String agentCode
		, @Param("agentLevel") String agentLevel
		, @Param("authCode") String authCode);
	
	@Query("SELECT v FROM ViewPerformanceDaily v WHERE v.agentLevel = :agentLevel"
			+ " AND (:authLevel = 'ZONE')"
			+ " ORDER BY v.dayOfMonth")
	public List<ViewPerformanceDaily> findByCompany(
		@Param("agentLevel") String agentLevel
		, @Param("authLevel") String authLevel);
	
	@Query("SELECT v FROM ViewPerformanceDaily v WHERE v.groupChannel = :channelCode"
			+ " AND v.agentLevel = :agentLevel"
			+ " AND (:authLevel = 'ZONE')"
			+ " ORDER BY v.dayOfMonth")
	public List<ViewPerformanceDaily> findByChannel(
		@Param("channelCode") String channelCode
		, @Param("agentLevel") String agentLevel
		, @Param("authLevel") String authLevel);
	
	@Query("SELECT v FROM ViewPerformanceDaily v WHERE v.zone = :zoneCode"
			+ " AND v.agentLevel = :agentLevel"
			+ " AND (:authLevel = 'ZONE')"
			+ " ORDER BY v.dayOfMonth")
	public List<ViewPerformanceDaily> findByZone(
		@Param("zoneCode") String zoneCode
		, @Param("agentLevel") String agentLevel
		, @Param("authLevel") String authLevel);
}
