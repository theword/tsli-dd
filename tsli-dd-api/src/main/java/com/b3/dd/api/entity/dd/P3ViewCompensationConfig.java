package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_COMPENSATION_CONFIG", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewCompensationConfig.findAll", query = "SELECT v FROM P3ViewCompensationConfig v")
})
public class P3ViewCompensationConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	@Column(name = "CLOS_YM", length = 6)
    private String closYm;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "AGENT_LEVEL", length = 5)
    private String agentLevel;
	@Column(name = "CALCULATION_PERIOD", length = 10)
    private String calculationPeriod;
	@Column(name = "COMPENSATION_CODE", length = 100)
    private String compensationCode;
	@Column(name = "COMPENSATION_DESC", length = 250)
    private String compensationDesc;
	
	@Column(name = "PC")
    private Double pc;
	@Column(name = "RATE")
    private Double rate;
	@Column(name = "ACTIVE_PRODUCER")
    private Double activeProducer;
	@Column(name = "CASE")
    private Double noCase;
	@Column(name = "VALIDATE_1")
    private Double validate1;
	@Column(name = "VALIDATE_2")
    private Double validate2;
	
	@Column(name = "CREATE_DATE")
    private Date createDate;
	@Column(name = "CREATE_BY", length = 10)
    private String createBy;
	@Column(name = "UPDATE_DATE")
    private Date upeateDate;
    @Column(name = "UPDATE_BY", length = 10)
    private String updateBy;
	
	public P3ViewCompensationConfig() {	
	}

	public P3ViewCompensationConfig(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewCompensationConfig)) {
            return false;
        }
        P3ViewCompensationConfig other = (P3ViewCompensationConfig) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getCalculationPeriod() {
		return calculationPeriod;
	}

	public void setCalculationPeriod(String calculationPeriod) {
		this.calculationPeriod = calculationPeriod;
	}

	public String getCompensationCode() {
		return compensationCode;
	}

	public void setCompensationCode(String compensationCode) {
		this.compensationCode = compensationCode;
	}

	public String getCompensationDesc() {
		return compensationDesc;
	}

	public void setCompensationDesc(String compensationDesc) {
		this.compensationDesc = compensationDesc;
	}

	public Double getPc() {
		return pc;
	}

	public void setPc(Double pc) {
		this.pc = pc;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getValidate1() {
		return validate1;
	}

	public void setValidate1(Double validate1) {
		this.validate1 = validate1;
	}

	public Double getValidate2() {
		return validate2;
	}

	public void setValidate2(Double validate2) {
		this.validate2 = validate2;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpeateDate() {
		return upeateDate;
	}

	public void setUpeateDate(Date upeateDate) {
		this.upeateDate = upeateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Double getActiveProducer() {
		return activeProducer;
	}

	public void setActiveProducer(Double activeProducer) {
		this.activeProducer = activeProducer;
	}

	public Double getNoCase() {
		return noCase;
	}

	public void setNoCase(Double noCase) {
		this.noCase = noCase;
	}

}
