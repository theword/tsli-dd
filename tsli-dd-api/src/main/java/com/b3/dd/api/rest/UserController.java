package com.b3.dd.api.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.UsersLogging.ActionType;
import com.b3.dd.api.service.UsersLoggingService;

import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("${api.prefix}/users")
public class UserController {
	
	private static final Logger log = LogManager.getLogger(UserController.class);
	@Autowired
	private DefaultTokenServices tokenServices;
	@Autowired
	private TokenStore tokenStore;
	@Autowired
	private UsersLoggingService usersLoggingService;

	@GetMapping("/me")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Principal> get(final Principal principal) {
		return ResponseEntity.ok(principal);
	}

	@DeleteMapping("/oauth/token")
	@ResponseBody
	public void revokeToken(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {

		if (authentication != null) {
			new SecurityContextLogoutHandler().logout(request, response, authentication);
			final String userToken = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
			tokenServices.revokeToken(userToken);

		}

		// String authorization = request.getHeader("Authorization");

		SecurityContextHolder.getContext().setAuthentication(null);
		// if (authorization != null && authorization.contains("Bearer")) {
		// String tokenId = authorization.substring("Bearer".length() + 1);
		// tokenServices.revokeToken(userToken);
		// }
	}

	@ResponseBody
	@DeleteMapping("/oauth/revoke")
	public void revokeToken(Authentication auth) {
		final String userToken = ((OAuth2AuthenticationDetails) auth.getDetails()).getTokenValue();
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(userToken);

		tokenStore.removeRefreshToken(accessToken.getRefreshToken());
		tokenStore.removeAccessToken(accessToken);
		// tokenStore.removeRefreshToken(token);

		// tokenServices.
	}

	@GetMapping("/signout")
	public void signout(HttpServletRequest request) {
		String authorization = request.getHeader("Authorization");
		log.info("Logout session: " + authorization);
		if (authorization != null && authorization.contains("Bearer")) {
			String tokenId = authorization.substring("Bearer".length() + 1);
			log.info("Logout token: " + tokenId);
			OAuth2AccessToken o = tokenServices.readAccessToken(tokenId);
			Map<String, Object> additionalInfo = o.getAdditionalInformation();

			String agentCode = (String) additionalInfo.get("agentCode");

			usersLoggingService.saveLogging(request, agentCode,  "SUCCESS", ActionType.LOGOUT);

		}
	}
}
