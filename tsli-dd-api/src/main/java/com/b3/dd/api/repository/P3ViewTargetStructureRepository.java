package com.b3.dd.api.repository;

import org.springframework.stereotype.Repository;

import com.b3.dd.api.entity.dd.P3ViewTargetStructure;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

@Repository
public interface P3ViewTargetStructureRepository extends PagingAndSortingRepository<P3ViewTargetStructure, Integer> {
	// CASE I.I - Find by Agent Code
	@Query(value="SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE AGENT_CODE = :agentCode"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			OR AVP_CODE = :authCode"
			+ "			OR AL_CODE = :authCode"
			+ "			OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE "
			+ "			FROM DD_T_AGENT_SON_MAPPING WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY"
			+ "		CASE WHEN AGENT_CODE = :agentCode THEN 1"
			+ "	END"
			, nativeQuery=true)
	public Page<P3ViewTargetStructure> findByAgentCode(
			@Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE I.II - Find by AL Code
	@Query(value="SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE AL_CODE = :agentCode"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			OR AVP_CODE = :authCode"
			+ "			OR AL_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE "
			+ "			FROM DD_T_AGENT_SON_MAPPING WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY"
			+ "		CASE WHEN AGENT_CODE = :agentCode THEN 1"
			+ "	END"
			, nativeQuery=true)
	public Page<P3ViewTargetStructure> findByAlCode(
			@Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE I.III - Find by AVP Code
	@Query(value="SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE AVP_CODE = :agentCode"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			OR AVP_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE "
			+ "			FROM DD_T_AGENT_SON_MAPPING WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY"
			+ "		CASE WHEN AGENT_CODE = :agentCode THEN 1"
			+ "	END"
			, nativeQuery=true)
	public Page<P3ViewTargetStructure> findByAvpCode(
			@Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE I.IV - Find by GM Code
	@Query(value="SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE GM_CODE = :agentCode"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE "
			+ "			FROM DD_T_AGENT_SON_MAPPING WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ "	ORDER BY"
			+ "		CASE WHEN AGENT_CODE = :agentCode THEN 1"
			+ "	END"
			, nativeQuery=true)
	public Page<P3ViewTargetStructure> findByGmCode(
			@Param("agentCode") String agentCode
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE I.V - Find by Zone Code
	@Query(value=""
			+ "SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE "
			+ "		(:authLevel = 'ZONE')"
			, nativeQuery = true)
	public Page<P3ViewTargetStructure> findByZoneCode(
			@Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE II.I - GM Search from keyword
	@Query(value=""
			+ "SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE GM_CODE = :agentCode"
			+ "	AND"
			+ "	("
			+ " 	(GM_CODE LIKE :searchKeyword OR"
			+ "		GM_NAME LIKE :searchKeyword OR"
			+ "		AVP_CODE LIKE :searchKeyword OR"
			+ "		AVP_NAME LIKE :searchKeyword OR"
			+ "		AL_CODE LIKE :searchKeyword OR"
			+ "		AL_NAME LIKE :searchKeyword OR"
			+ "		AGENT_CODE LIKE :searchKeyword OR"
			+ "		AGENT_NAME LIKE :searchKeyword"
			+ "		)"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING"
			+ "				WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ ")"
			, nativeQuery = true)
	public Page<P3ViewTargetStructure> findByGmSearch(
			@Param("agentCode") String agentCode
			, @Param("searchKeyword") String searchKeyword
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE II.II - AVP Search from keyword
	@Query(value=""
			+ "SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE AVP_CODE = :agentCode"
			+ "	AND"
			+ "	("
			+ " 	(AVP_CODE LIKE :searchKeyword OR"
			+ "		AVP_NAME LIKE :searchKeyword OR"
			+ "		AL_CODE LIKE :searchKeyword OR"
			+ "		AL_NAME LIKE :searchKeyword OR"
			+ "		AGENT_CODE LIKE :searchKeyword OR"
			+ "		AGENT_NAME LIKE :searchKeyword"
			+ "		)"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			OR AVP_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING"
			+ "				WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ ")"
			, nativeQuery = true)
	public Page<P3ViewTargetStructure> findByAvpSearch(
			@Param("agentCode") String agentCode
			, @Param("searchKeyword") String searchKeyword
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE II.III - AL Search from keyword
	@Query(value=""
			+ "SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE AL_CODE = :agentCode"
			+ "	AND"
			+ "	("
			+ " 	(AL_CODE LIKE :searchKeyword OR"
			+ "		AL_NAME LIKE :searchKeyword OR"
			+ "		AGENT_CODE LIKE :searchKeyword OR"
			+ "		AGENT_NAME LIKE :searchKeyword"
			+ "		)"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			OR AVP_CODE = :authCode"
			+ "			OR AL_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING"
			+ "				WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ ")"
			, nativeQuery = true)
	public Page<P3ViewTargetStructure> findByAlSearch(
			@Param("agentCode") String agentCode
			, @Param("searchKeyword") String searchKeyword
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE II.IV - AG Search from keyword
	@Query(value=""
			+ "SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE AGENT_CODE = :agentCode"
			+ "	AND"
			+ "	("
			+ " 	(AGENT_CODE LIKE :searchKeyword OR"
			+ "		AGENT_NAME LIKE :searchKeyword"
			+ "		)"
			+ " AND ("
			+ "			("
			+ "			GM_CODE = :authCode"
			+ "			OR AVP_CODE = :authCode"
			+ "			OR AL_CODE = :authCode"
			+ "			OR AGENT_CODE = :authCode"
			+ "			)"
			+ "		OR"
			+ "			(:authLevel = 'ZONE')"
			+ "		OR"
			+ "			(:authCode IN (SELECT AGENT_CODE"
			+ "				FROM DD_T_AGENT_SON_MAPPING"
			+ "				WHERE SON_CODE = :agentCode)"
			+ "		)"
			+ "		OR"
			+ "		("
			+ "			GM_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AVP_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AL_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "			OR AGENT_CODE IN (SELECT SON_CODE FROM DD_T_AGENT_SON_MAPPING WHERE AGENT_CODE = :authCode)"
			+ "		)"
			+ " )"
			+ ")"
			, nativeQuery = true)
	public Page<P3ViewTargetStructure> findByAgSearch(
			@Param("agentCode") String agentCode
			, @Param("searchKeyword") String searchKeyword
			, @Param("authCode") String authCode
			, @Param("authLevel") String authLevel
			, Pageable pageable);
	
	// CASE II.V - ZONE Search from keyword
	@Query(value=""
			+ "SELECT * FROM VIEW_TARGET_STRUCTURE"
			+ "	WHERE "
			+ "	("
			+ " 	(ZONE LIKE :searchKeyword OR"
			+ "		GM_CODE LIKE :searchKeyword OR"
			+ "		GM_NAME LIKE :searchKeyword OR"
			+ "		AVP_CODE LIKE :searchKeyword OR"
			+ "		AVP_NAME LIKE :searchKeyword OR"
			+ "		AL_CODE LIKE :searchKeyword OR"
			+ "		AL_NAME LIKE :searchKeyword OR"
			+ "		AGENT_CODE LIKE :searchKeyword OR"
			+ "		AGENT_NAME LIKE :searchKeyword"
			+ "		)"
			+ " AND ("
			+ "		(:authLevel = 'ZONE')"
			+ " )"
			+ ")"
			, nativeQuery = true)
	public Page<P3ViewTargetStructure> findByZoneSearch(
			@Param("searchKeyword") String searchKeyword
			, @Param("authLevel") String authLevel
			, Pageable pageable);
}
