package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewPromotionList;
import com.b3.dd.api.model.RequestModel;
//import com.b3.dd.api.repository.P3ViewPromotionListRepository;
import com.b3.dd.api.service.P3ViewPromotionListService;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewPromotionListController {
//	@Autowired
//	private P3ViewPromotionListRepository P3ViewPromotionListRepository;
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private P3ViewPromotionListService P3ViewPromotionListService;
	// CASE I.I - I.IV
	@PostMapping("promotion-list")
	public ResponseEntity<List>
	getGmPromotionList(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		
		List<?> P3ViewPromotionList = null;
		String agentLevel = request.getAgentLevel();
		System.out.println("Zone " + request.getChannelCode());
		System.out.println("Zone " + request.getChannelCode());
		System.out.println(request.getFilter());
		
		
		
		switch (agentLevel) {
			case "ZONE":
				P3ViewPromotionList = P3ViewPromotionListService.findByZone(
					request.getFilter(), userHelper.getUserPosition(), request.getZoneCode(), request.getChannelCode()
				);
				break;
			case "GM":
				P3ViewPromotionList = P3ViewPromotionListService.findByGmCode(
					request.getAgentCode(), request.getFilter(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			case "AVP":
				P3ViewPromotionList = P3ViewPromotionListService.findByAvpCode(
					request.getAgentCode(), request.getFilter(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			case "AL":
				P3ViewPromotionList = P3ViewPromotionListService.findByAlCode(
					request.getAgentCode(), request.getFilter(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			case "AGENT":
				P3ViewPromotionList = P3ViewPromotionListService.findByAgentCode(
					request.getAgentCode(), request.getFilter(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			default:
				break;
		}
		
		return ResponseEntity.ok().body(P3ViewPromotionList);
	}
}
