package com.b3.dd.api.rest;

import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewPerformanceWeekly;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewPerformanceWeeklyRepository;

@RestController
@RequestMapping("/${api.prefix}/performance-weekly")
public class ViewPerformanceWeeklyController {
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private ViewPerformanceWeeklyRepository ViewPerformanceWeeklyRepository;
	
	@PostMapping("company")
	public ResponseEntity<List<ViewPerformanceWeekly>> 
	getPerformanceWeeklyCompany(@RequestBody RequestModel request)
		throws ResourceClosedException {
			List<ViewPerformanceWeekly> viewPerformanceWeekly = ViewPerformanceWeeklyRepository.findByCompany(
					userHelper.getUserPosition());
				
			return ResponseEntity.ok().body(viewPerformanceWeekly);
	}
	
	@PostMapping("channel")
	public ResponseEntity<List<ViewPerformanceWeekly>> 
	getPerformanceWeeklyChannel(@RequestBody RequestModel request)
		throws ResourceClosedException {
			List<ViewPerformanceWeekly> viewPerformanceWeekly = ViewPerformanceWeeklyRepository.findByChannel(
					request.getChannelCode(), userHelper.getUserPosition());
				
			return ResponseEntity.ok().body(viewPerformanceWeekly);
	}
	
	@PostMapping("zone")
	public ResponseEntity<List<ViewPerformanceWeekly>> 
	getPerformanceWeeklyZone(@RequestBody RequestModel request)
		throws ResourceClosedException {
			List<ViewPerformanceWeekly> viewPerformanceWeekly = ViewPerformanceWeeklyRepository.findByZone(
					request.getZoneCode(), userHelper.getUserPosition());
				
			return ResponseEntity.ok().body(viewPerformanceWeekly);
	}
}
