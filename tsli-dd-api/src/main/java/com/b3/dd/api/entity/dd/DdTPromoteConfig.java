package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DD_T_PROMOTE_CONFIG", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "DdTPromoteConfig.findAll", query = "SELECT v FROM DdTPromoteConfig v")
})
public class DdTPromoteConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "EFFECTIVE_YM", length = 6)
    private String effectiveYm;
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "AGENT_LEVEL", length = 10)
    private String agentLevel;
	@Column(name = "PERIOD_START", length = 6)
    private String periodStart;
	@Column(name = "PERIOD_END", length = 6)
    private String periodEnd;
	@Column(name = "GROUP_TYPE", length = 5)
    private String groupType;
	
	@Column(name = "PERIOD_TYPE", length = 50)
    private String periodType;
	@Column(name = "POSITION_CODE", length = 25)
    private String positionCode;
	@Column(name = "PROMOTE_TYPE", length = 5)
    private String promoteType;
	@Column(name = "NEXT_AGENT_LEVEL", length = 25)
    private String nextAgentLevel;
	@Column(name = "NEXT_POSITION_CODE", length = 25)
    private String nextPositionCode;
	
	@Column(name = "VALIDATE_1", length = 50)
    private String validate1;
	@Column(name = "VALIDATE_2", length = 50)
    private String validate2;
	@Column(name = "VALIDATE_3", length = 50)
    private String validate3;
	@Column(name = "VALIDATE_4", length = 50)
    private String validate4;
	@Column(name = "VALIDATE_5", length = 50)
    private String validate5;
	@Column(name = "VALIDATE_6", length = 50)
    private String validate6;
	@Column(name = "VALIDATE_7", length = 50)
    private String validate7;
	@Column(name = "VALIDATE_8", length = 50)
    private String validate8;
	@Column(name = "VALIDATE_9", length = 50)
    private String validate9;
	@Column(name = "VALIDATE_10", length = 50)
    private String validate10;
	
	@Column(name = "VALIDATE_KPI_1", length = 50)
    private String validateKpi1;
	@Column(name = "VALIDATE_KPI_2", length = 50)
    private String validateKpi2;
	@Column(name = "VALIDATE_KPI_3", length = 50)
    private String validateKpi3;
	@Column(name = "VALIDATE_KPI_4", length = 50)
    private String validateKpi4;
	@Column(name = "VALIDATE_KPI_5", length = 50)
    private String validateKpi5;
	@Column(name = "VALIDATE_KPI_6", length = 50)
    private String validateKpi6;
	@Column(name = "VALIDATE_KPI_7", length = 50)
    private String validateKpi7;
	@Column(name = "VALIDATE_KPI_8", length = 50)
    private String validateKpi8;
	@Column(name = "VALIDATE_KPI_9", length = 50)
    private String validateKpi9;
	@Column(name = "VALIDATE_KPI_10", length = 50)
    private String validateKpi10;
	
	@Column(name = "REG_DTM")
    private Date regDtm;
	@Column(name = "PE_NO_REG", length = 10)
    private String peNoReg;
	@Column(name = "CHG_DTM")
    private Date chgDtm;
    @Column(name = "PE_NO_CHG", length = 10)
    private String peNoChg;
	
	public DdTPromoteConfig() {	
	}

	public DdTPromoteConfig(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof DdTPromoteConfig)) {
            return false;
        }
        DdTPromoteConfig other = (DdTPromoteConfig) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEffectiveYm() {
		return effectiveYm;
	}

	public void setEffectiveYm(String effectiveYm) {
		this.effectiveYm = effectiveYm;
	}

	public String getGroupChannel() {
		return groupChannel;
	}

	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public String getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getPromoteType() {
		return promoteType;
	}

	public void setPromoteType(String promoteType) {
		this.promoteType = promoteType;
	}

	public String getNextAgentLevel() {
		return nextAgentLevel;
	}

	public void setNextAgentLevel(String nextAgentLevel) {
		this.nextAgentLevel = nextAgentLevel;
	}

	public String getNextPositionCode() {
		return nextPositionCode;
	}

	public void setNextPositionCode(String nextPositionCode) {
		this.nextPositionCode = nextPositionCode;
	}

	public String getValidate1() {
		return validate1;
	}

	public void setValidate1(String validate1) {
		this.validate1 = validate1;
	}

	public String getValidate2() {
		return validate2;
	}

	public void setValidate2(String validate2) {
		this.validate2 = validate2;
	}

	public String getValidate3() {
		return validate3;
	}

	public void setValidate3(String validate3) {
		this.validate3 = validate3;
	}

	public String getValidate4() {
		return validate4;
	}

	public void setValidate4(String validate4) {
		this.validate4 = validate4;
	}

	public String getValidate5() {
		return validate5;
	}

	public void setValidate5(String validate5) {
		this.validate5 = validate5;
	}

	public String getValidate6() {
		return validate6;
	}

	public void setValidate6(String validate6) {
		this.validate6 = validate6;
	}

	public String getValidate7() {
		return validate7;
	}

	public void setValidate7(String validate7) {
		this.validate7 = validate7;
	}

	public String getValidate8() {
		return validate8;
	}

	public void setValidate8(String validate8) {
		this.validate8 = validate8;
	}

	public String getValidate9() {
		return validate9;
	}

	public void setValidate9(String validate9) {
		this.validate9 = validate9;
	}

	public String getValidate10() {
		return validate10;
	}

	public void setValidate10(String validate10) {
		this.validate10 = validate10;
	}

	public String getValidateKpi1() {
		return validateKpi1;
	}

	public void setValidateKpi1(String validateKpi1) {
		this.validateKpi1 = validateKpi1;
	}

	public String getValidateKpi2() {
		return validateKpi2;
	}

	public void setValidateKpi2(String validateKpi2) {
		this.validateKpi2 = validateKpi2;
	}

	public String getValidateKpi3() {
		return validateKpi3;
	}

	public void setValidateKpi3(String validateKpi3) {
		this.validateKpi3 = validateKpi3;
	}

	public String getValidateKpi4() {
		return validateKpi4;
	}

	public void setValidateKpi4(String validateKpi4) {
		this.validateKpi4 = validateKpi4;
	}

	public String getValidateKpi5() {
		return validateKpi5;
	}

	public void setValidateKpi5(String validateKpi5) {
		this.validateKpi5 = validateKpi5;
	}

	public String getValidateKpi6() {
		return validateKpi6;
	}

	public void setValidateKpi6(String validateKpi6) {
		this.validateKpi6 = validateKpi6;
	}

	public String getValidateKpi7() {
		return validateKpi7;
	}

	public void setValidateKpi7(String validateKpi7) {
		this.validateKpi7 = validateKpi7;
	}

	public String getValidateKpi8() {
		return validateKpi8;
	}

	public void setValidateKpi8(String validateKpi8) {
		this.validateKpi8 = validateKpi8;
	}

	public String getValidateKpi9() {
		return validateKpi9;
	}

	public void setValidateKpi9(String validateKpi9) {
		this.validateKpi9 = validateKpi9;
	}

	public String getValidateKpi10() {
		return validateKpi10;
	}

	public void setValidateKpi10(String validateKpi10) {
		this.validateKpi10 = validateKpi10;
	}

	public Date getRegDtm() {
		return regDtm;
	}

	public void setRegDtm(Date regDtm) {
		this.regDtm = regDtm;
	}

	public String getPeNoReg() {
		return peNoReg;
	}

	public void setPeNoReg(String peNoReg) {
		this.peNoReg = peNoReg;
	}

	public Date getChgDtm() {
		return chgDtm;
	}

	public void setChgDtm(Date chgDtm) {
		this.chgDtm = chgDtm;
	}

	public String getPeNoChg() {
		return peNoChg;
	}

	public void setPeNoChg(String peNoChg) {
		this.peNoChg = peNoChg;
	}

}
