package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.DdTUploadHistory;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.DdTUploadHistoryRepository;

@RestController
@RequestMapping("/${api.prefix}/upload-file-hist")
public class DdTUploadHistoryController {
	@Autowired
	private DdTUploadHistoryRepository DdTUploadHistoryRepository;
	
	@GetMapping("get-upload-file-history")
	public ResponseEntity<List<DdTUploadHistory>>
	getUploadHistoryFile(@RequestParam("type") String type)
		throws ResourceClosedException {
		return ResponseEntity.ok().body(DdTUploadHistoryRepository.findUploadHistoryByType(type));
	}

}
