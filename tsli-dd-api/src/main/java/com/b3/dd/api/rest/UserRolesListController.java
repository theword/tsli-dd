package com.b3.dd.api.rest;

import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.entity.dd.DdTUserRolesList;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.UserRolesListRepository;

@RestController
@RequestMapping("/${api.prefix}/user/roles")
public class UserRolesListController {
	@Autowired
	private UserRolesListRepository UserRolesListRepository;
	
	@PostMapping("/roles-by-username")
	public ResponseEntity<List<DdTUserRolesList>> 
	getUserRolesList(@RequestBody RequestModel request) throws ResourceClosedException {
		List<DdTUserRolesList> DdTUserRolesList = UserRolesListRepository.findByUsername(request.getUsername());
		return ResponseEntity.ok().body(DdTUserRolesList);
	}
}
