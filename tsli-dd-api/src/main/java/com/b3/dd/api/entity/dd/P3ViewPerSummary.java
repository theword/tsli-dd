package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PERSISTENCY_SUMMARY", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "P3ViewPerSummary.findAll", query = "SELECT v FROM P3ViewPerSummary v")
})

public class P3ViewPerSummary implements Serializable,  Comparable<P3ViewPerSummary> {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 10)
    private String closYm;
	@Column(name = "PERSISTENCY_TYPE", length = 25)
    private String persistencyType;
	
	@Column(name = "CHANNEL", length = 10)
    private String channel;
	@Column(name = "AGENT_CODE", length = 10)
    private String agentCode;
	@Column(name = "AGENT_LEVEL", length = 10)
    private String agentLevel;
	
	@Column(name = "TARGET_PC")
    private Double targetPc;
	@Column(name = "COLLECT_PC")
    private Double collectPc;
	@Column(name = "PLUS_PC")
    private Double plusPc;
	@Column(name = "PRST_RTE")
    private Double prstRte;
	
	public P3ViewPerSummary() {	
	}

	public P3ViewPerSummary(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof P3ViewPerSummary)) {
            return false;
        }
        P3ViewPerSummary other = (P3ViewPerSummary) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getPersistencyType() {
		return persistencyType;
	}

	public void setPersistencyType(String persistencyType) {
		this.persistencyType = persistencyType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}

	public Double getTargetPc() {
		return targetPc;
	}

	public void setTargetPc(Double targetPc) {
		this.targetPc = targetPc;
	}

	public Double getCollectPc() {
		return collectPc;
	}

	public void setCollectPc(Double collectPc) {
		this.collectPc = collectPc;
	}

	public Double getPlusPc() {
		return plusPc;
	}

	public void setPlusPc(Double plusPc) {
		this.plusPc = plusPc;
	}

	public Double getPrstRte() {
		return prstRte;
	}

	public void setPrstRte(Double prstRte) {
		this.prstRte = prstRte;
	}
	
	
	  public int compareTo(P3ViewPerSummary u) {
	    if (getClosYm() == null || u.getClosYm() == null) {
	      return 0;
	    }
	    return getClosYm().compareTo(u.getClosYm());
	  }
}
