package com.b3.dd.api.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.b3.dd.api.entity.dd.UsersLogging;

public interface UsersLoggingRepository extends JpaRepository<UsersLogging, Long> {
	
	@Query("SELECT u FROM UsersLogging u "
			+ "WHERE "
			+ " ( UPPER(u.username) like concat('%',UPPER(:username),'%') or :username = null) and "
			+ " ( UPPER( u.fullName) like concat('%',UPPER(:name),'%') or :name = null) and "
			+ " ( UPPER(u.info1) like concat('%',UPPER(:device),'%') or :device = null) and "
			+ " ( (u.createDate >= :startDate or :startDate = null ) and (u.createDate <= :endDate or :endDate = null ) ) "
			+ " order by u.createDate desc")
	Page<UsersLogging> searchUserAccessLog(
			@Param("username") String username,
			@Param("name") String name,
			@Param("startDate") Date startDate,
			@Param("endDate") Date endDate,
			@Param("device") String device,
			Pageable pageable);
}
