package com.b3.dd.api.rest;

import java.security.Principal;
import java.util.List;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.ViewYtdAgentActive;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.ViewYtdAgentActiveRepository;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class ViewYtdAgentActiveController {

	@Autowired
	private ViewYtdAgentActiveRepository ViewYtdAgentActiveRepository;
	@Autowired
	private UserHelper userHelper;
	
	@PostMapping("ytd/active") // Current and last year
	public ResponseEntity<List<ViewYtdAgentActive>>
	getAgentYtdCurrentYear(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		List<ViewYtdAgentActive> ViewYtdAgentActive = ViewYtdAgentActiveRepository.findByAgentCode(
			request.getAgentCode()
			, request.getAgentLevel()
			, principal.getName()
			, userHelper.getUserPosition());
		
		return ResponseEntity.ok().body(ViewYtdAgentActive);
	}
}
