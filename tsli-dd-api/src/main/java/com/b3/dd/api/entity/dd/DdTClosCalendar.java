package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DD_T_CLOS_CALENDAR", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "DdTClosCalendar.findAll", query = "SELECT v FROM DdTClosCalendar v")
})
public class DdTClosCalendar implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "CLOS_YM", length = 20)
    private String closYm;
	
	@Column(name = "PR_STR_YMD", length = 20)
    private String prStrYmd;
	@Column(name = "PR_END_YMD", length = 20)
    private String prEndYmd;
	@Column(name = "UW_STR_YMD", length = 20)
    private String uwStrYmd;
	@Column(name = "UW_END_YMD", length = 20)
    private String uwEndYmd;
	
	@Column(name = "CALENDAR_STR_YMD", length = 20)
    private String calendarStrYmd;
	@Column(name = "CALENDAR_END_YMD", length = 20)
    private String calendarEndYmd;
	@Column(name = "FLAG_END_CLOSING_YMD", length = 20)
    private String flagEndClosingYmd;
	@Column(name = "END_CLOSING_YMD", length = 20)
    private String endClosingYmd;
	
	public DdTClosCalendar() {	
	}

	public DdTClosCalendar(Integer id) {
		super();
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof DdTClosCalendar)) {
            return false;
        }
        DdTClosCalendar other = (DdTClosCalendar) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "P3ViewPersistency [id=" + id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClosYm() {
		return closYm;
	}

	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}

	public String getPrStrYmd() {
		return prStrYmd;
	}

	public void setPrStrYmd(String prStrYmd) {
		this.prStrYmd = prStrYmd;
	}

	public String getPrEndYmd() {
		return prEndYmd;
	}

	public void setPrEndYmd(String prEndYmd) {
		this.prEndYmd = prEndYmd;
	}

	public String getUwStrYmd() {
		return uwStrYmd;
	}

	public void setUwStrYmd(String uwStrYmd) {
		this.uwStrYmd = uwStrYmd;
	}

	public String getUwEndYmd() {
		return uwEndYmd;
	}

	public void setUwEndYmd(String uwEndYmd) {
		this.uwEndYmd = uwEndYmd;
	}

	public String getCalendarStrYmd() {
		return calendarStrYmd;
	}

	public void setCalendarStrYmd(String calendarStrYmd) {
		this.calendarStrYmd = calendarStrYmd;
	}

	public String getCalendarEndYmd() {
		return calendarEndYmd;
	}

	public void setCalendarEndYmd(String calendarEndYmd) {
		this.calendarEndYmd = calendarEndYmd;
	}

	public String getFlagEndClosingYmd() {
		return flagEndClosingYmd;
	}

	public void setFlagEndClosingYmd(String flagEndClosingYmd) {
		this.flagEndClosingYmd = flagEndClosingYmd;
	}

	public String getEndClosingYmd() {
		return endClosingYmd;
	}

	public void setEndClosingYmd(String endClosingYmd) {
		this.endClosingYmd = endClosingYmd;
	}
}
