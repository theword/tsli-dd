/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.dd.api.entity.dd;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "OAUTH_REFRESH_TOKEN", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OauthRefreshToken.findAll", query = "SELECT o FROM OauthRefreshToken o")
    , @NamedQuery(name = "OauthRefreshToken.findById", query = "SELECT o FROM OauthRefreshToken o WHERE o.id = :id")
    , @NamedQuery(name = "OauthRefreshToken.findByTokenId", query = "SELECT o FROM OauthRefreshToken o WHERE o.tokenId = :tokenId")})
public class OauthRefreshToken implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private Integer id;
    @Column(name = "TOKEN_ID", length = 256)
    private String tokenId;
    @Lob
    @Column(name = "TOKEN")
    private Serializable token;
    @Lob
    @Column(name = "AUTHENTICATION")
    private Serializable authentication;

    public OauthRefreshToken() {
    }

    public OauthRefreshToken(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public Serializable getToken() {
        return token;
    }

    public void setToken(Serializable token) {
        this.token = token;
    }

    public Serializable getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Serializable authentication) {
        this.authentication = authentication;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OauthRefreshToken)) {
            return false;
        }
        OauthRefreshToken other = (OauthRefreshToken) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsli.dd.entity.OauthRefreshToken[ id=" + id + " ]";
    }
    
}
