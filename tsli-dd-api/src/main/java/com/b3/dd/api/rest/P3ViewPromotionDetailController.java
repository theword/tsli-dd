package com.b3.dd.api.rest;

import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.dd.api.config.security.UserHelper;
import com.b3.dd.api.entity.dd.P3ViewPromotionDetail;
import com.b3.dd.api.model.FileUploadModel;
import com.b3.dd.api.model.RequestModel;
import com.b3.dd.api.repository.P3ViewPromotionDetailReposity;
import com.b3.dd.api.service.UploadPromotionExcelService;

@RestController
@RequestMapping("/${api.prefix}/agent")
public class P3ViewPromotionDetailController {
	
	private static final Logger log = LogManager.getLogger(P3ViewPromotionDetailController.class);
	@Autowired
	private P3ViewPromotionDetailReposity P3ViewPromotionDetailReposity;
	@Autowired
	private UserHelper userHelper;
	@Autowired
	private ApplicationContext ctx;
	
	// CASE I.I - I.IV
	@PostMapping("promotion-detail")
	public ResponseEntity<List<P3ViewPromotionDetail>>
	getGmPromotionDetail(@RequestBody RequestModel request, final Principal principal)
		throws ResourceClosedException {
		String agentLevel = request.getAgentLevel();
		List<P3ViewPromotionDetail> P3ViewPromotionDetail = null;
		
		String caseAgentLevel = agentLevel;
		if (agentLevel.equals("ZONE")) {
			// AS COMPANY
			if (request.getChannelCode().equals("COMPANY") && request.getZoneCode().equals("COMPANY")) {
				caseAgentLevel = "COMPANY";
			}
			// AS CHANNEL
			else if (!request.getChannelCode().equals("COMPANY") && request.getZoneCode().equals("COMPANY")) {
				caseAgentLevel = "CHANNEL";
			}
			// AS ZONE
			else if (!request.getChannelCode().equals("COMPANY") && !request.getZoneCode().equals("COMPANY")) {
				caseAgentLevel = "ZONE";
			}
		}
		
		System.out.println("------------------------------------");
		System.out.println("FilterType " + request.getFilterType());
		System.out.println("AgentLevel " + agentLevel);
		System.out.println("Filter " + request.getFilter());
		System.out.println("AgentCode " + request.getFilter());
		System.out.println("Zone" + request.getChannelCode());

		
		switch (caseAgentLevel) {
			case "COMPANY":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByCompany(
					request.getFilter(), request.getFilterType(), userHelper.getUserPosition()
				);
				break;
			case "CHANNEL":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByChannel(
					request.getFilter(), request.getChannelCode(), request.getFilterType(), userHelper.getUserPosition()
				);
				break;
			case "ZONE":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByZone(
					request.getFilter(), request.getChannelCode(), request.getZoneCode(), request.getFilterType(), userHelper.getUserPosition()
				);
				break;
			case "GM":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByGmCode(
					request.getAgentCode(), request.getFilter(), request.getFilterType(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			case "AVP":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByAvpCode(
					request.getAgentCode(), request.getFilter(), request.getFilterType(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			case "AL":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByAlCode(
					request.getAgentCode(), request.getFilter(), request.getFilterType(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			case "AGENT":
				P3ViewPromotionDetail = P3ViewPromotionDetailReposity.findByAgentCode(
					request.getAgentCode(), request.getFilter(), request.getFilterType(), principal.getName(), userHelper.getUserPosition()
				);
				break;
			default:
				break;
		}
		
		return ResponseEntity.ok().body(P3ViewPromotionDetail);
	}
	
	@PostMapping("upload-promotion")
	public ResponseEntity ExcelPromotion(@RequestBody RequestModel request) throws IOException {
		try {
			String promotionType = request.getPromotionType();
			String fileBase64 = request.getFileBased64();
			String fileType = request.getFileType();
			String fileName = request.getFileName();
			Long fileSize = request.getFileSize();
			String owner = userHelper.getUserLoggedInName() + " (" +userHelper.getName()+ ")";
			
			log.info("Upload file: " + fileName);
			log.info("Upload file type: " + fileType);
			log.info("Upload file size: " + fileSize/1024 + "kb");
			
			FileUploadModel f = new FileUploadModel();
			f.setData(fileBase64); //Base64
			f.setExtension(fileType); //Type
			f.setFilename(fileName); //FileName
			f.setSize(fileSize); //Size
			
			ctx.getBean(UploadPromotionExcelService.class, f, owner, promotionType).importFile();
			List<String> messages = Arrays.asList("success");
			return ResponseEntity.ok().body(messages);
			
		}catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getLocalizedMessage());
			List<String> messages = Arrays.asList("failed");
			return ResponseEntity.ok().body(messages);
		}
		// return ResponseEntity.ok().build();
	} 
}
