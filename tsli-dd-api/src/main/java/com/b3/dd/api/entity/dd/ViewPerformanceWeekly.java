package com.b3.dd.api.entity.dd;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "VIEW_PERFORMANCE_WEEKLY", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ViewPerformanceWeekly.findAll", query = "SELECT v FROM ViewPerformanceWeekly v")
})
public class ViewPerformanceWeekly implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
	
	@Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
	@Column(name = "ZONE", length = 30)
    private String zone;
	@Column(name = "ZONE_NAME", length = 20)
    private String zoneName;
	@Column(name = "AGENT_LEVEL", length = 10)
    private String agentLevel;
	
	@Column(name = "TARGET_1")
	private Double target1;
	@Column(name = "TARGET_2")
	private Double target2;
	@Column(name = "TARGET_3")
	private Double target3;
	@Column(name = "TARGET_4")
	private Double target4;
	
	@Column(name = "ACTUAL_1")
	private Double actual1;
	@Column(name = "ACTUAL_2")
	private Double actual2;
	@Column(name = "ACTUAL_3")
	private Double actual3;
	@Column(name = "ACTUAL_4")
	private Double actual4;
	
	@Column(name = "GAP_1")
	private Double gap1;
	@Column(name = "GAP_2")
	private Double gap2;
	@Column(name = "GAP_3")
	private Double gap3;
	@Column(name = "GAP_4")
	private Double gap4;
	
	public ViewPerformanceWeekly() {
		
	}

	public ViewPerformanceWeekly(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGroupChannel() {
		return groupChannel;
	}
	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getAgentLevel() {
		return agentLevel;
	}
	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}
	public Double getTarget1() {
		return target1;
	}
	public void setTarget1(Double target1) {
		this.target1 = target1;
	}
	public Double getTarget2() {
		return target2;
	}
	public void setTarget2(Double target2) {
		this.target2 = target2;
	}
	public Double getTarget3() {
		return target3;
	}
	public void setTarget3(Double target3) {
		this.target3 = target3;
	}
	public Double getTarget4() {
		return target4;
	}
	public void setTarget4(Double target4) {
		this.target4 = target4;
	}
	public Double getActual1() {
		return actual1;
	}
	public void setActual1(Double actual1) {
		this.actual1 = actual1;
	}
	public Double getActual2() {
		return actual2;
	}
	public void setActual2(Double actual2) {
		this.actual2 = actual2;
	}
	public Double getActual3() {
		return actual3;
	}
	public void setActual3(Double actual3) {
		this.actual3 = actual3;
	}
	public Double getActual4() {
		return actual4;
	}
	public void setActual4(Double actual4) {
		this.actual4 = actual4;
	}
	public Double getGap1() {
		return gap1;
	}
	public void setGap1(Double gap1) {
		this.gap1 = gap1;
	}
	public Double getGap2() {
		return gap2;
	}
	public void setGap2(Double gap2) {
		this.gap2 = gap2;
	}
	public Double getGap3() {
		return gap3;
	}
	public void setGap3(Double gap3) {
		this.gap3 = gap3;
	}
	public Double getGap4() {
		return gap4;
	}
	public void setGap4(Double gap4) {
		this.gap4 = gap4;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
    public boolean equals(Object obj) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof ViewPerformanceWeekly)) {
            return false;
        }
        ViewPerformanceWeekly other = (ViewPerformanceWeekly) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ViewPerformanceWeekly [id=" + id + "]";
	}
	
}
