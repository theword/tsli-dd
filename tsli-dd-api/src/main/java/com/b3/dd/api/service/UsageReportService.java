package com.b3.dd.api.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.b3.dd.api.entity.dd.UsersLogging;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

@Service
public class UsageReportService {

	private static final Logger log = LogManager.getLogger(UsageReportService.class);
	private static final String TEMPLATE = "resources/report-templates/report-usage.tpl.xlsx";
	private static final String SHEET_NAME = "Detail";
	public ByteArrayOutputStream export( List<UsersLogging> datas) {

		Workbook workbooks;
		Sheet sheet;
		ByteArrayOutputStream fileOut = null;
		
		if(datas == null )
			return fileOut;

		try {
			
			workbooks = WorkbookFactory.create(new File(TEMPLATE));
			sheet = workbooks.getSheet(SHEET_NAME);

			for(int i=0, rownum=1, cell=0; i<datas.size(); i++, rownum++ ) {

				UsersLogging data = datas.get(i);
				Row row = sheet.createRow(rownum);
				row.createCell(cell++).setCellValue(data.getInfo1());	// USER_TYPE
				row.createCell(cell++).setCellValue(data.getInfo2());	// GROUP_CHANNEL
				row.createCell(cell++).setCellValue(data.getInfo3());	// ZONE_CODE
				row.createCell(cell++).setCellValue(data.getInfo4());	// ZONE_NAME
				row.createCell(cell++).setCellValue(data.getInfo5());	// AGENT_LEVEL
				row.createCell(cell++).setCellValue(data.getFullName());	// FULL_NAME
				row.createCell(cell++).setCellValue(data.getUsername());	// USERNAME
				row.createCell(cell++).setCellValue(data.getActionType());	// ACTION_TYPE
				row.createCell(cell++).setCellValue(data.getCreateDate());	// LOGIN_TIME
				row.createCell(cell++).setCellValue(data.getCreateDate());	// DATE_TIME

			}

			// Resize all columns to fit the content size
			for(int i = sheet.getRow(0).getFirstCellNum() ; i < sheet.getRow(0).getLastCellNum(); i++) {
				sheet.autoSizeColumn(i);
			}

			  // Write the output to a file
	        fileOut = new ByteArrayOutputStream();
	        workbooks.write(fileOut);

	        // Closing the workbook
	        workbooks.close();
	        
		} catch (Exception e) {
			log.error(e);
		}

		return fileOut;
	}

	public static void main( String args[] ) throws IOException {
		UsageReportService u = new UsageReportService();
		ByteArrayOutputStream b = u.export(new ArrayList<UsersLogging>());
		
		FileOutputStream o = new FileOutputStream("D:/f.xlsx");
		o.write(b.toByteArray());
		o.flush();
		o.close();
				
	}
}
